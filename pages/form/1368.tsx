import Layout from '../../components/layout';
import {Field, FieldArray, Form, Formik} from "formik";
import React, {useState} from "react";
import FormTwo from "./1368/FormTwo";
import FormOne from "./1368/FormOne";

const formInitial = {
    from: "",
    to: "",
    net_sales_receipts: "",
    less_cost_of_goods_sold: "",
    gross_profit: "",
    officers_salaries: "",
    employee_wages: "",
    advertising: "",
    rent: "",
    utilities: "",
    interest: "",
    taxes: "",
    insurance: "",
    other_expenses: "",
    total_expenses: "",
    net_profit_loss: "",
    fiscal_year_first: "",
    fiscal_year_second: "",
    fiscal_year_third: "",
    current_year_to_date: "",
    fiscal_year_data: [
        {
            month: "",
            fiscal_year_one: "",
            estimate_one: false,
            fiscal_year_two: "",
            estimate_two: false,
            fiscal_year_three: "",
            estimate_three: false,
            current_year: "",
            estimate_current_year: false
        }, {
            month: "",
            fiscal_year_one: "",
            estimate_one: false,
            fiscal_year_two: "",
            estimate_two: false,
            fiscal_year_three: "",
            estimate_three: false,
            current_year: "",
            estimate_current_year: false
        }, {
            month: "",
            fiscal_year_one: "",
            estimate_one: false,
            fiscal_year_two: "",
            estimate_two: false,
            fiscal_year_three: "",
            estimate_three: false,
            current_year: "",
            estimate_current_year: false
        }, {
            month: "",
            fiscal_year_one: "",
            estimate_one: false,
            fiscal_year_two: "",
            estimate_two: false,
            fiscal_year_three: "",
            estimate_three: false,
            current_year: "",
            estimate_current_year: false
        }, {
            month: "",
            fiscal_year_one: "",
            estimate_one: false,
            fiscal_year_two: "",
            estimate_two: false,
            fiscal_year_three: "",
            estimate_three: false,
            current_year: "",
            estimate_current_year: false
        }, {
            month: "",
            fiscal_year_one: "",
            estimate_one: false,
            fiscal_year_two: "",
            estimate_two: false,
            fiscal_year_three: "",
            estimate_three: false,
            current_year: "",
            estimate_current_year: false
        }, {
            month: "",
            fiscal_year_one: "",
            estimate_one: false,
            fiscal_year_two: "",
            estimate_two: false,
            fiscal_year_three: "",
            estimate_three: false,
            current_year: "",
            estimate_current_year: false
        }, {
            month: "",
            fiscal_year_one: "",
            estimate_one: false,
            fiscal_year_two: "",
            estimate_two: false,
            fiscal_year_three: "",
            estimate_three: false,
            current_year: "",
            estimate_current_year: false
        }, {
            month: "",
            fiscal_year_one: "",
            estimate_one: false,
            fiscal_year_two: "",
            estimate_two: false,
            fiscal_year_three: "",
            estimate_three: false,
            current_year: "",
            estimate_current_year: false
        }, {
            month: "",
            fiscal_year_one: "",
            estimate_one: false,
            fiscal_year_two: "",
            estimate_two: false,
            fiscal_year_three: "",
            estimate_three: false,
            current_year: "",
            estimate_current_year: false
        }
    ],
    fiscal_year_one_total: "",
    fiscal_year_two_total: "",
    fiscal_year_three_total: "",
    in_total: ""
};

export default () => {

    const [tab, setTab] = useState(1);
    const [formFin, setForm] = useState(formInitial);

    const handleTab = (value) => {
        setTab(value)
    };

    const handleTabOneForm = (form) => {
        var formFinal = formFin;

        formFinal["fiscal_year_1"] = form.fiscal_year_first;
        formFinal["fiscal_year_2"] = form.fiscal_year_second;
        formFinal["fiscal_year_3"] = form.fiscal_year_third;
        formFinal["current_year_to_date"] = form.current_year_to_date;

        var fiscalYears = form.fiscal_year_data;
        var index;

        for(var i=0; i<fiscalYears.length; i++){
            index = i.toString();
            formFinal["month"+index] = fiscalYears[i].month;

            formFinal["month_"+index+"_sales_figures_fiscal_year_1"] = fiscalYears[i].fiscal_year_one;
            formFinal["month_"+index+"_sales_figures_fiscal_year_1_e_if_estimate"] = fiscalYears[i].estimate_one;

            formFinal["month_"+index+"_sales_figures_fiscal_year_2"] = fiscalYears[i].fiscal_year_two;
            formFinal["month_"+index+"_sales_figures_fiscal_year_2_e_if_estimate"] = fiscalYears[i].estimate_two;

            formFinal["month_"+index+"_sales_figures_fiscal_year_3"] = fiscalYears[i].fiscal_year_three;
            formFinal["month_"+index+"_sales_figures_fiscal_year_3_e_if_estimate"] = fiscalYears[i].estimate_three;

            formFinal["month_"+index+"_sales_figures_current_year"] = fiscalYears[i].current_year;
            formFinal["month_"+index+"_sales_figures_current_year_e_if_estimate"] = fiscalYears[i].estimate_current_year;
        }

        formFinal["total_fiscal_year_1"] = form.fiscal_year_one_total;
        formFinal["total_fiscal_year_2"] = form.fiscal_year_two_total;
        formFinal["total_fiscal_year_3"] = form.fiscal_year_three_total;
        formFinal["total_current_fiscal_year_to_date"] = form.in_total;

            console.log(formFinal)
        setForm(formFinal);
        handleTab(2);
    }

    const handleTabTwoForm = (form) => {
        var formFinal = formFin;

        formFinal["from"] = form.from;
        formFinal["to"] = form.to;
        formFinal["net_sales_receipts"] = form.net_sales_receipts;
        formFinal["less_cost_of_goods_sold"] = form.less_cost_of_goods_sold;
        formFinal["gross_profit"] = form.gross_profit;
        formFinal["officers_salaries"] = form.officers_salaries;
        formFinal["employee_wages"] = form.employee_wages;
        formFinal["advertising"] = form.advertising;
        formFinal["rent"] = form.rent;
        formFinal["utilities"] = form.utilities;
        formFinal["interest"] = form.interest;
        formFinal["taxes"] = form.taxes;
        formFinal["insurance"] = form.insurance;
        formFinal["other_expenses"] = form.other_expenses;
        formFinal["total_expenses"] = form.total_expenses;
        formFinal["net_profit_loss"] = form.net_profit_loss;

        console.log(formFinal);
        setForm(formFinal);

    }

    return (
        <Layout>
            {tab == 1 ?
                (
                    <FormOne    handleTabOne={handleTab}
                                formFin={formFin}
                                handleForm={handleTabOneForm} />
                )
                : (
                    <FormTwo    handleTabOne={handleTab}
                                handleForm={handleTabTwoForm}
                                formFin={formFin}/>
                )
            }
        </Layout>
    );
};