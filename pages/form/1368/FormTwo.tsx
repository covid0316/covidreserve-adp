import Layout from '../../../components/layout';
import {Form, Formik, Field} from "formik";
import React, {useEffect, useState} from "react";
import * as storage from "../../../lib/storage";
import _  from "lodash";

const tableCss = {
    width: "80%",
    margin: "auto",
    display: "block",
    borderColor: 'transparent',
    padding: '5px 0px 5px 0px',
    textAlign: 'right',
} as React.CSSProperties;

const initialValues = {
    from: "",
    to: "",
    net_sales_receipts: "",
    less_cost_of_goods_sold: "",
    gross_profit: "",
    officers_salaries: "",
    employee_wages: "",
    advertising: "",
    rent: "",
    utilities: "",
    interest: "",
    taxes: "",
    insurance: "",
    other_expenses: "",
    total_expenses: "",
    net_profit_loss: "",
};

const styles = {
    thInput:{
        margin: "auto",
        width: "100%",
        marginTop: 8,
        fontSize: 13,
        textAlign: 'center',
        padding: 5,
        borderColor: '#1F2933',
        // marginTop: 10,
        marginRight: 15,
        marginBottom: 10,
        marginLeft: 15,

    } as React.CSSProperties,
    trBorder:{
        border: '0.06rem solid #1F2933',
        fontSize: 15
    } as React.CSSProperties,
    tdBorder:{
        border: '0.06rem solid #1F2933',
        borderSpacing: 0 ,
        paddingLeft: 10
    } as React.CSSProperties,
}

export default ({handleTabOne,handleForm,formFin}) => {
    const [savedData, setSavedData] = useState(formFin);
    const [ loading, setLoading ] = useState(true);

    useEffect(() => {
        async function fetchData() {
            setLoading(true);
            const data = [];//await storage.FormStorageHelper.get(1368, 2);
            if (Object.keys(data).length !== 0) {
                setSavedData(data)

            }
            setLoading(false);
        }
        fetchData();
    }, []);

    if(loading) {
        return <div />;
    }

    return (
        <Formik
            initialValues={savedData}
            enableReinitialize={true}
            validate={values => {
                const errors = {};
                return errors;
            }}
            onSubmit={(values, {setSubmitting}) => {
                setTimeout(() => {
                    setSubmitting(false);
                    // storage.FormStorageHelper.set(values, 1368, 2);
                    handleForm(savedData);
                }, 1);
            }}
        >
            {({
                  values,
                  touched,
                  errors,
                  dirty,
                  isSubmitting,
                  handleChange,
                  handleBlur,
                  handleSubmit,
                  handleReset,
              }) => (
                <Form>
                    <p style={{fontSize: "20px", marginBottom: "14px"}}>Task 2</p>
                    <h1>Please submit any additional narrative or financial information you feel will help establish
                        your economic loss</h1>

                    <p style={{fontSize: "20px", marginBottom: "14px"}}>It can be helpful to provide a financial
                        forecast to illustrate what the income and expenses
                        for the business will be during the period affected by the disaster until normal operations
                        resume. This is not required. This optional forecast format is provided for your
                        convenience.</p>
                    <label style={{
                        fontSize: "20px",
                        width: '80%',
                        margin: 'auto',
                        marginTop: 40
                    }}>
                        <b>Period covered by this forcast. From</b>
                        <Field
                            onChange={(e) => {
                                setSavedData({...savedData,from: e.target.value});
                            }} value={savedData.from}
                            id="from"
                            placeholder="From"
                            type="date"
                            onBlur={handleBlur}
                            style={styles.thInput}
                        />
                            <b>to</b>
                        <Field
                            onChange={(e) => {
                                setSavedData({...savedData,to: e.target.value});
                            }} value={savedData.to}
                            id="to"
                            placeholder="From"
                            type="date"
                            onBlur={handleBlur}
                            style={styles.thInput}
                        />
                    </label>

                    <table  style={{
                                margin: 'auto',
                                width: '72%',
                                border: '0.06rem solid #1F2933',
                                borderSpacing: 0,


                            }}>
                        <tr style={styles.trBorder}>
                            <td style={{
                                    border: '0.06rem solid #1F2933',
                                    width: '60%',
                                    paddingLeft: 10
                            }}>Net Sales (receipts)</td>
                            <td style={styles.tdBorder}>
                                <Field
                                    type="text"
                                    style={tableCss}
                                    onChange={(e) => {
                                        if(!isNaN(e.target.value))
                                            setSavedData({...savedData,net_sales_receipts: e.target.value});
                                    }} value={savedData.net_sales_receipts}
                                    id="net_sales_receipts"
                                    placeholder="$xx.xxx.xx"
                                    onBlur={handleBlur}

                                />
                            </td>
                        </tr>
                        <tr style={styles.trBorder}>
                            <td style={styles.tdBorder}>Less cost of goods sold</td>
                            <td style={styles.tdBorder}>
                                <Field
                                    style={tableCss}
                                    onChange={(e) => {
                                        if(!isNaN(e.target.value))
                                            setSavedData({...savedData,less_cost_of_goods_sold: e.target.value});
                                    }} value={savedData.less_cost_of_goods_sold}
                                    id="less_cost_of_goods_sold"
                                    placeholder="$xx.xxx.xx"
                                    type="text"
                                    onBlur={handleBlur}
                                />
                            </td>
                        </tr>
                        <tr style={styles.trBorder}>
                            <td style={styles.tdBorder}>Gross profit</td>
                            <td style={styles.tdBorder}>
                                <Field
                                    style={tableCss}
                                    id="gross_profit"
                                    placeholder="$xx.xxx.xx"
                                    type="text"
                                    onChange={(e) => {
                                        if(!isNaN(e.target.value))
                                            setSavedData({...savedData,gross_profit: e.target.value});
                                    }} value={savedData.gross_profit}
                                    onBlur={handleBlur}
                                />
                            </td>
                        </tr>

                        <tr style={{

                            border: '1px solid #1F2933',
                        }}>
                            <td style={{height: '50px', fontSize: "15px",paddingLeft:10, borderTop: "1px solid rgb(31, 41, 51)", borderLeft: "1px solid rgb(31, 41, 51)", borderBottom: "1px solid rgb(31, 41, 51)"}}><b>Expenses</b></td>
                            <td style={{height: '50px', fontSize: "15px",paddingLeft:10, borderTop: "1px solid rgb(31, 41, 51)", borderRight: "1px solid rgb(31, 41, 51)", borderBottom: "1px solid rgb(31, 41, 51)"}}><b></b></td>
                        </tr>
                        <tr style={styles.trBorder}>
                            <td style={styles.tdBorder}>Officers salaries</td>
                            <td style={styles.tdBorder}>
                                <Field
                                    style={tableCss}
                                    id="officers_salaries"
                                    placeholder="$xx.xxx.xx"
                                    type="text"
                                    onChange={(e) => {
                                        if(!isNaN(e.target.value))
                                            setSavedData({...savedData,officers_salaries: e.target.value});
                                    }} value={savedData.officers_salaries}
                                    onBlur={handleBlur}
                                />
                            </td>
                        </tr>
                        <tr style={styles.trBorder}>
                            <td style={styles.tdBorder}>Employee wages</td>
                            <td style={styles.tdBorder}>
                                <Field
                                    style={tableCss}
                                    id="employee_wages"
                                    placeholder="$xx.xxx.xx"
                                    type="text"
                                    onChange={(e) => {
                                        if(!isNaN(e.target.value))
                                            setSavedData({...savedData,employee_wages: e.target.value});
                                    }} value={savedData.employee_wages}
                                    onBlur={handleBlur}
                                />
                            </td>
                        </tr>
                        <tr style={styles.trBorder}>
                            <td style={styles.tdBorder}>Advertising</td>
                            <td style={styles.tdBorder}>
                                <Field
                                    style={tableCss}
                                    id="advertising"
                                    placeholder="$xx.xxx.xx"
                                    type="text"
                                    onChange={(e) => {
                                        if(!isNaN(e.target.value))
                                            setSavedData({...savedData,advertising: e.target.value});
                                    }} value={savedData.advertising}
                                    onBlur={handleBlur}
                                />
                            </td>
                        </tr>


                        <tr style={styles.trBorder}>
                            <td style={styles.tdBorder}>Rent</td>
                            <td style={styles.tdBorder}>
                                <Field
                                    style={tableCss}
                                    id="rent"
                                    placeholder="$xx.xxx.xx"
                                    type="text"
                                    onChange={(e) => {
                                        if(!isNaN(e.target.value))
                                            setSavedData({...savedData,rent: e.target.value});
                                    }} value={savedData.rent}
                                    onBlur={handleBlur}
                                />
                            </td>
                        </tr>
                        <tr style={styles.trBorder}>
                            <td style={styles.tdBorder}>Utility</td>
                            <td style={styles.tdBorder}>
                                <Field
                                    style={tableCss}
                                    id="utilities"
                                    placeholder="$xx.xxx.xx"
                                    type="text"
                                    onChange={(e) => {
                                        if(!isNaN(e.target.value))
                                            setSavedData({...savedData,utilities: e.target.value});
                                    }} value={savedData.utilities}
                                    onBlur={handleBlur}
                                />
                            </td>
                        </tr>

                        <tr style={styles.trBorder}>
                            <td style={styles.tdBorder}>Interest</td>
                            <td style={styles.tdBorder}>
                                <Field
                                    style={tableCss}
                                    id="interest"
                                    placeholder="$xx.xxx.xx"
                                    type="text"
                                    onChange={(e) => {
                                        if(!isNaN(e.target.value))
                                            setSavedData({...savedData,interest: e.target.value});
                                    }} value={savedData.interest}
                                    onBlur={handleBlur}
                                />
                            </td>
                        </tr>
                        <tr style={styles.trBorder}>
                            <td style={styles.tdBorder}>Taxes</td>
                            <td style={styles.tdBorder}>
                                <Field
                                    style={tableCss}
                                    id="taxes"
                                    placeholder="$xx.xxx.xx"
                                    type="text"
                                    onChange={(e) => {
                                        if(!isNaN(e.target.value))
                                            setSavedData({...savedData,taxes: e.target.value});
                                    }} value={savedData.taxes}
                                    onBlur={handleBlur}
                                />
                            </td>
                        </tr>
                        <tr style={styles.trBorder}>
                            <td style={styles.tdBorder}>Insurance</td>
                            <td style={styles.tdBorder}>
                                <Field
                                    style={tableCss}
                                    id="insurance"
                                    placeholder="$xx.xxx.xx"
                                    type="text"
                                    onChange={(e) => {
                                        if(!isNaN(e.target.value))
                                            setSavedData({...savedData,insurance: e.target.value});
                                    }} value={savedData.insurance}
                                    onBlur={handleBlur}
                                />
                            </td>
                        </tr>
                        <tr style={styles.trBorder}>
                            <td style={styles.tdBorder}>Other Expenses</td>
                            <td style={styles.tdBorder}>
                                <Field
                                    style={tableCss}
                                    id="other_expenses"
                                    placeholder="$xx.xxx.xx"
                                    type="text"
                                    onChange={(e) => {
                                        if(!isNaN(e.target.value))
                                            setSavedData({...savedData,other_expenses: e.target.value});
                                    }} value={savedData.other_expenses}
                                    onBlur={handleBlur}
                                />
                            </td>
                        </tr>
                        <tr style={styles.trBorder}>
                            <td style={styles.tdBorder}>Total Expenses</td>
                            <td style={styles.tdBorder}>
                                <Field
                                    style={tableCss}
                                    id="total_expenses"
                                    type="text"
                                    onChange={(e) => {
                                        if(!isNaN(e.target.value))
                                            setSavedData({...savedData,total_expenses: e.target.value});
                                    }} value={savedData.total_expenses}
                                    onBlur={handleBlur}
                                />
                            </td>
                        </tr>
                        <tr style={styles.trBorder}>
                            <td style={styles.tdBorder}>Net profit/loss before income taxes</td>
                            <td style={styles.tdBorder}>
                                <Field
                                    style={tableCss}
                                    id="net_profit_loss"
                                    type="text"
                                    onChange={(e) => {
                                        if(!isNaN(e.target.value))
                                            setSavedData({...savedData,net_profit_loss: e.target.value});
                                    }} value={savedData.net_profit_loss}
                                    onBlur={handleBlur}
                                />
                            </td>
                        </tr>
                    </table>
                    <div
                        style={{
                            display: 'flex',
                            width: '100%',
                            paddingTop: '48px'
                        }}
                    >
                        <button className='nextbutton' type="button"
                                style={{
                                    borderColor: 'transparent',
                                    padding: '10px 15px 10px 15px',
                                    fontSize: 15
                                }}
                                onClick={() => handleTabOne(1)}>Previous</button>
                        <button style={{
                                    margin: '0px 0px 0 auto',
                                    borderColor: 'transparent',
                                    padding: '10px 15px 10px 15px',
                                    fontSize: 15
                                }} className='nextbutton' type="submit"
                                disabled={isSubmitting}>
                            Next
                        </button>
                    </div>


                </Form>
            )}
        </Formik>

    );
};
