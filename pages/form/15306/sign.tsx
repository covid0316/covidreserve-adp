import { NextPage } from 'next';
import { useRouter } from 'next/router';
import FormikWrapper from '../../../components/FormikWrapper';
import Layout from '../../../components/layout';
import * as storage from '../../../lib/storage';
import Link from 'next/link';
import { useEffect, useState } from 'react';

type Props = {};

const FormTitle = '15306';

const _413DSign: NextPage<Props> = () => {

  return (
    <Layout>
      <h2>Form 15306 completed</h2>
      <p className="alldone">You’re all set! Click the button below to sign your form.</p>
      <div className="center"><Link href={`/form/15306/signature/start?fields=q5-sig-0&fields=q6-sig-0&fields=q7-sig-0&fields=c1-sig-0&fields=c2-sig-0&fields=c3-sig-0&fields=c4-sig-0&fields=c5-sig-0&fields=c6-sig-0&fields=c7-sig-0`}><a>Initialize 15306 (Owners)</a></Link></div>
      <div className="center"><Link href={`/form/15306/signature/start?fields=signature_of_authorized_representative_of_business&fields=date&fields=print_name&fields=title`}><a>Sign 15306 (Authorized Representative)</a></Link></div>
      <div className="center"><Link href={`/form/15306/signature/start?fields=signature_of_owner_of_applicant_business&fields=date_2&fields=print_name_2&fields=title_2`}><a>Sign 15306 (Owner)</a></Link></div>
      <style jsx>{`
        .balance-sheet {
          display: flex;
          flex-wrap: wrap;
          font-size: 20px;
        }
        .balance-sheet > div {
          flex-basis: 70%;
          padding-bottom: 10px;
        }
        .balance-sheet > div.amount {
          flex-basis: 15%;
          position: relative;
        }
        .balance-sheet > div.amount > :global(.inner) {
          text-align: right;
          display: inline-block;
          position: absolute;
          right: 0;
        }
        h2 {
          font-size: 48px;
          font-weight: normal;
          text-align: center;
        }
        h3 {
          padding-top: 25px;
          font-size: 48px;
        }
        .total, .amount {
          font-weight: bold;
        }
        p.alldone {
          padding: 40px 60px 0 60px;
          font-size: 24px;
          text-align: center;
        }
        .center {
          text-align: center;
        }
        .center :global(a),
        .center :global(a:hover),
        .center :global(a:visited) {
          display: inline-block;
          background: #1382E9;
          border-radius: 5px;
          padding: 12px 16px;
          font-size: 20px;
          color: white !important;
          margin-bottom: 20px;
        }
      `}</style>
    </Layout>
  );
};

export default _413DSign;