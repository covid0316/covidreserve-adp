import { useState, useEffect } from 'react';
import Link from 'next/link';
import { Formik, Form, Field } from 'formik';
import FileUploadProgress  from 'react-fileupload-progress';
import * as Yup from 'yup';
import { v4 as uuidv4 } from 'uuid';
import { FormStorageHelper, flattenListForm } from '../../../../lib/storage';
import Layout from '../../../../components/layout';
import { Pen, X } from '../../../../components/Icons';
import ProgressBar from '../../../../components/ProgressBar';
import AddButtonFab from '../../../../components/AddButtonFab';

const _413DFormsSchema = Yup.object().shape({
  name: Yup.string().required("Name is required")
});

type FormName = {
  name: string
};

const FormTitle = '15306';

export default () => {
  const [forms, setForms] = useState([] as string[]);
  const [isAdding, setIsAdding] = useState(undefined as (string | undefined));

  const addForm = () => {
    if (!isAdding) {
      setIsAdding(uuidv4());
    }
  };

  const deleteForm = (formName: string) => async (evt: any) => {
    const newForms = forms.filter(name => formName !== name);
    await FormStorageHelper.set(newForms, FormTitle, 100);
    setForms(newForms);
  }

  const createForm = async (formName: string) => {
    if (isAdding) {
      const newForms = [ ...forms, formName ];
      await FormStorageHelper.set(newForms, FormTitle, 100);
      setForms(newForms);
      setIsAdding(undefined);
    }
  };

  useEffect(() => {
    FormStorageHelper.get(FormTitle, 100).then(data => setForms((data && data.length) ? data : []));
  }, []);

  if (!forms) {
    return <Layout>Loading...</Layout>
  }

  return (
    <Layout>{console.log(forms)}
      <div className="instructions">
        <p>Task 1</p>
        <h2 style={{fontWeight: "bold"}}>Please attach your 2019 tax returns.</h2>
        <div className="inner">
          { (forms || [])
              .sort((a, b) => (a[0] > b[0]) ? 1 : (a[0] == b[0]) ? 0 : -1)
              .map((formName, i) => {
                return (
                  <div className="row">
                    <div><label htmlFor="name">Name</label></div>
                    <div className="item">
                      <Link href={`https://0a87fe55-2e09-4589-96fa-ab80821b21c4.s3.us-east-1.amazonaws.com/${encodeURIComponent(formName)}`}>
                      <a>{Pen} Tax Forms {i}</a>
                      </Link>
                    </div>
                    <div className="details">
                      <span className="delete" onClick={deleteForm(formName)}>{X}</span>
                    </div>
                  </div>
                );
              })
          }
          { isAdding &&
            <div className="formRow">
              <div>
                <FileUploadProgress key='ex1' method="PUT" url={`https://0a87fe55-2e09-4589-96fa-ab80821b21c4.s3.us-east-1.amazonaws.com/Tax Document ${isAdding}.pdf`} onLoad={async (e, request) => {
                  await createForm(`Tax Document ${isAdding}.pdf`);
                }} />
              </div>
            </div>
          }
          <AddButtonFab className={isAdding ? 'disabled' : ''} label="Add Another Document" onClick={addForm} />
        </div>
      </div>
      <style jsx>
        {`
          .delete {
            cursor: pointer;
          }
          .formRow :global(form > div),
          .formRow :global(form input) {
            display: inline-block;
          }
          .formRow :global(form input[type=file]) {
            display: inline-block;
            width: 350px;
          }
          .formRow :global(form input[type=submit]) {
            position: relative;
            top: 2px;
            margin-left: 15px;
            display: inline-block;
            width: 200px;
          }
          .row {
            display: flex;
            justify-content: flex-start;
          }
          .row .item {
            line-height: 31px;
            padding: 0 20px;
          }
          .row :global(svg) {
            vertical-align: text-bottom;
            display: inline-block;
            position: relative;
            top: .125em;
          }
          .formRow {
            display: flex;
            justify-content: space-between;
          }
          h2 {
            font-size: 36px;
            font-weight: normal;
          }
          p {
            font-size: 20px;
            line-height: 142%;
            margin-bottom: 0;
          }
          .center {
            text-align: center;
          }
          li {
            padding-top: 20px;
            font-size: 20px;
          }
          .inner {
            padding-left: 0;
          }
          .inner p {
            font-size: 16px;
            font-weight: bold;
            margin: 16px 0;
          }
          .instructions :global(.fabbutton.disabled .fab) {
            background: #B1B9C0;
          }
          .instructions :global(.fabbutton.disabled) {
            cursor: default;
          }
          .fabbutton {
            margin-left: 0;
          }
          .formRow :global(input) {
            color: #4B6075;
            font-size: 16px;
            font-family: inherit;
            box-sizing: border-box;
            position: relative;
            width: 100%;
            margin: 0;
            border-color: black;
            padding: 5px 20px;
          }
          label {
            font-weight: bold;
            line-height: 31px;
            margin-bottom: 0;
          }
          .validate {
            line-height: 16px;
            color: #D92C23;
            font-size: 12px;
          }
          button {
            padding: 0 40px;
            background: #1382E9;
            font-size: 20px;
            height: 31px;
          }
        `}
      </style>
    </Layout>
  );
};
