import React from 'react';

import Layout from '../../components/layout';
import * as storage from '../../lib/storage';
import FormikWrapper from '../../components/FormikWrapper';
import { Form } from 'formik';
import questions, { FormSchema } from '../../lib/forms/2202';
import GrowingTable from '../../components/GrowingTable';

export default () => {
  return (
    <Layout style={{ padding: '64px 0', maxWidth: '1300px' }}>
      <div>
        <FormikWrapper
          initialValues={{ [questions[0][0].id]: [] }}
          storageKey={'2202'}
          validationSchema={FormSchema}
        >
          {({ isSubmitting, values, setFieldValue, errors }) => (
            <Form>
              <div>
                {questions[0].map(question => {
                  if (question.type === 'growingtable') {
                    return (
                      <GrowingTable
                        title={'Schedule of Liabilities'}
                        question={question}
                        values={values}
                        setFieldValue={setFieldValue}
                        className={''}
                        errors={errors}
                        num={null}
                      />
                    );
                  }

                  throw new Error('unknown question type: ' + question.type);
                })}
              </div>
              <div style={{ display: 'flex' }}>
                <div
                  className='completebutton'
                  onClick={() => {
                    storage.FormStorageHelper.set(values, 2202);
                  }}
                  style={{ margin: '0 auto' }}
                >
                      <div>
                        <a className='completebutton'>Complete</a>
                      </div>
                </div>
              </div>
              {/* <pre>{JSON.stringify(values, null, 2)}</pre>
              <pre>{JSON.stringify(errors, null, 2)}</pre> */}
            </Form>
          )}
        </FormikWrapper>
      </div>
    </Layout>
  );
};
