import { Formik, Form, Field } from 'formik';
import { NextPage } from 'next';
import { useRouter } from 'next/router';
import Link from 'next/link';
import { Question } from '../../../../components/QuestionTypes';
import Layout from '../../../../components/layout';
import * as storage from '../../../../lib/storage';
import DynamicQuestion from '../../../../lib/renderQuestion';
import FormikWrapper from '../../../../components/FormikWrapper';
import * as Yup from 'yup';
import { truncateSync } from 'fs';
import { useState, useEffect } from 'react';

type Props = {};

const FormTitle = '4506-T';

const getInitialValues = (form: object | null) => {
  if (form && Object.keys(form).length) {
    return form;
  } else {
    return questions.reduce((obj, item: any) => {
      if (item.type === 'multi') {
        return { ...obj, [item.id]: [{}] };
      }

      if (item.type === 'none') {
        return { ...obj };
      }

      if (item.type === 'date') {
        return { ...obj, [`${item.id}-month`]: '', [`${item.id}-day`]: '', [`${item.id}-year`]: '' }
      }

      if (item.other) {
        // eslint-disable-next-line no-param-reassign
        obj = { ...obj, [item.other]: false }; // @todo: re-assign is bad
      }

      if (item.fields) {
        return item.fields!.reduce(
          (obj2, item2) => ({ ...obj2, [item2.id]: false }),
          obj
        );
      }

      if (item.id) {
        return { ...obj, [item.id]: item.type === 'text' ? '' : false };
      }

      throw new Error('unexpected end')

    }, {});
  }
}


const _4506T: NextPage<Props> = ({}) => {
  const router = useRouter();
  const [formData, setFormData] = useState({});
  const [loading, setLoading] = useState(true);
  const formName = router.query.name as string;
  const submit = async (values, { setSubmitting }) => {
    await storage.FormStorageHelper.set(values, FormTitle, 0, formName);
    setSubmitting(false);
    router.push("/form/4506-T/forms");
  };

  useEffect(() => {
    if (formName) {
      setLoading(true);
      storage.FormStorageHelper.get(FormTitle, 0, formName)
        .then(data => {
          setFormData(getInitialValues(data));
          setLoading(false);
        });
    }
  }, [formName]);
  
  if (loading || !formName || !Object.keys(formData).length) {
    return <Layout>Loading...</Layout>
  }

  return <Layout>
    <h2>Form 4506-T for: {formName}</h2>
    <FormikWrapper
      stepNum={0}
      storageKey={4506}
      onSubmit={submit}
      validationSchema={yups}
      initialValues={formData}
    >
      { ({isSubmitting, values, setFieldValue}) =>
        <Form>
          { questions.map((question, index) => (
            <DynamicQuestion
              key={(question as any).id}
              question={question}
              setFieldValue={setFieldValue}
              values={values}
              num={null}
              className='subquestion'
            />
          ))}
          <div className="buttons">
            <div>
              <Link href="/form/4506-T/forms">
                <a>Back to all 4506-T forms</a>
              </Link>
            </div>
            <button type="submit">Save</button>
          </div>
        </Form>
      }
    </FormikWrapper>
    <style jsx>{`
      h2 {
        font-size: 48px;
        font-weight: normal;
      }
      .buttons {
        margin-top: 20px;
        display: flex;
        justify-content: space-between;
      }
      .buttons :global(a),
      .buttons :global(a:active),
      .buttons :global(a:visited) {
        padding: 0;
        padding: 12px 17px;
        font-size: 20px;
        cursor: pointer;
        color: #1F2933;
        background: #DCE6EF;
        display: block;
        border-radius: 5px;
      }
      button {
        padding: 0;
        background: #1382E9;
        width: 165px;
        height: 48px;
        font-size: 20px;
        cursor: pointer;
      }
    `}</style>
  </Layout>;
};


const dateValidator = (key: string): object => {
  return {
    [`${key}-day`]: Yup.number().integer(),
    [`${key}-month`]: Yup.number().integer(),
    [`${key}-year`]: Yup.number().when([`${key}-day`, `${key}-month`], {
      is: (day, month) => day || month,
      then: Yup.number()
        .typeError("Invalid date")
        .integer("Invalid date")
        .required("Complete date is required")
    }).test(
      'is-valid-date',
      'Invalid date',
      function (value) {
        const day = parseInt(this.parent[`${key}-day`], 10);
        const month = parseInt(this.parent[`${key}-month`], 10);
        const date = new Date(value, month - 1, day);
        if (date.getDate() !== day || date.getMonth() !== (month - 1) || date.getFullYear() !== value) {
          return false;
        } else {
          return true;
        }
      }
    )
  }
};

const yups = Yup.object().shape({
  'primary-name': Yup.string().required("Name is required"),
  'primary-ssn': Yup.string().required("SSN is required"),
  'phone-number-52063': Yup.string().required("Phone number is required"),
  'joint-return-yes': Yup.boolean(),
  'previous-address-different-from-current': Yup.boolean(),
  'spouse-name': Yup.string().when('joint-return-yes', {
    is: true,
    then: Yup.string().required("Name is required")
  }),
  'spouse-ssn': Yup.string().when('joint-return-yes', {
    is: true,
    then: Yup.string().required("SSN is required")
  }),
  'current-address': Yup.string().required("Address is required"),
  'previous-address': Yup.string().when('previous-address-different-from-current',
    {
      is: true,
      then: Yup.string().required("Address is required")
    }),
  ...(dateValidator('p1')),
  ...(dateValidator('p2')),
  ...(dateValidator('p3')),
});

const questions: Array<Question> = [
  {
    id: 'primary-name',
    type: 'text',
    formno: [4506],
    question: 'Name shown on tax return. If a joint return, enter the name shown first. If a company’s return, enter the Company Name as it appears on the return. '
  },
  {
    id: 'primary-ssn',
    type: 'text',
    formno: [4506],
    question: 'First social security number on the tax return, individual taxpayer ID number, or EIN.'
  },
  {
    id: 'phone-number-52063',
    type: 'text',
    formno: [4506],
    question: 'Phone number for this company or individual:'
  },
  {
    type: 'radio',
    formno: [4506],
    fields: [{
      id: 'joint-return-yes',
      displayName: 'Yes'
    }, {
      id: 'joint-return-no',
      displayName: 'No'
    }],
    question: 'Is this a joint return? If a company return, leave as no.'
  },
  {
    id: 'spouse-name',
    type: 'text',
    formno: [4506],
    question: 'Enter spouse’s name shown on tax return.',
    showCondition: (values) => values['joint-return-yes'] === true
  },
  {
    id: 'spouse-ssn',
    type: 'text',
    formno: [4506],
    question: 'Enter the spouse’s social security number or individual taxpayer identification number.',
    showCondition: (values) => values['joint-return-yes'] === true
  },
  {
    id: 'current-address',
    type: 'text',
    formno: [4506],
    question: 'Current name, address, (incl. apt., room, or studio no.), city, state and zip code.'
  },
  {
    id: '8',
    type: 'radio',
    formno: [4506],
    fields: [{
      id: 'previous-address-same-as-current',
      displayName: 'Yes'
    }, {
      id: 'previous-address-different-from-current',
      displayName: 'No'
    }],
    question: 'Is the above address the same as the address on the last filed return?'
  },
  {
    id: 'previous-address',
    type: 'text',
    formno: [4506],
    question: 'What is the previous address shown on the last return filed?',
    showCondition: (values) => values['previous-address-different-from-current'] === true
  },
  {
    id: 'form-number',
    type: 'text',
    formno: [4506],
    question: 'What is the tax form number associated with the return you’re requesting? '
  },
  {
    type: 'radio',
    formno: [4506],
    fields: [{
      id: 'nonfile-yes',
      displayName: 'Yes',
    }, {
      id: 'nonfile-no',
      displayName: 'No'
    }],
    question: 'Do you have verification from the IRS that you did not file a return at any point in the last 3 years?'
  },
  {
    id: 'p1',
    type: 'date',
    formno: [4506],
    question: 'Enter the ending date of the year requested, using the MM/DD/YYYY format.'
  },
  {
    id: 'p2',
    type: 'date',
    formno: [4506],
    question: null
  },
  {
    id: 'p3',
    type: 'date',
    formno: [4506],
    question: null
  },
];


export default _4506T;