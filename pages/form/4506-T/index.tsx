import Link from 'next/link';
import Layout from '../../../components/layout';

export default () => {
  return (
    <Layout>
      <div className="instructions">
        <h2>About Form 4506-T</h2>
        <p>Form 4606-T requests a transcript of your business' complete tax returns, including all schedules for the last 3 years for the Small Business Association. An explanation is required if not available.</p>
        <div className="center">
          <Link href="/form/4506-T/forms">
            <a className="button">Start</a>
          </Link>
        </div>
      </div>
      <style jsx>{`
        h2{
          font-size: 48px;
          text-align: center;
          font-weight: normal;
        }
        p {
          font-size: 20px;
          line-height: 142%;
        }
        .center {
          text-align: center;
        }
        .instructions :global(a),
        .instructions :global(a:visited),
        .instructions :global(a:active) {
          display: inline-block;
          color: white;
          background: #1382E9;
          border-radius: 5px;
          padding: 12px 16px;
          font-size: 20px;
          margin: 12px auto 0 auto;
        }
      `}</style>
    </Layout>
    );
};