import { useState, useEffect, Dispatch, SetStateAction } from 'react';
import Layout from '../../components/layout';
import { PdfRequest, PdfSuccess } from '../api/data/pdfTypes';
import { FormStorageHelper, flattenListForm } from '../../lib/storage';

const FormTitle = '15306';


function getPdf(forms: Array<string>) {
  return async (event: any) => {
    try {
      event.preventDefault();
      // Create a hidden anchor and click it. todo: is there a better way?
      
      let a = document.createElement("a");
      a.style.display = "none";
      document.body.appendChild(a);

      a.href = "https://0a87fe55-2e09-4589-96fa-ab80821b21c4.s3.us-east-1.amazonaws.com/"+forms[0];
      a.setAttribute("download", "Tax Forms.pdf");
      a.setAttribute("target", "_blank");
      a.click();

      document.body.removeChild(a);

      a = document.createElement("a");
      a.style.display = "none";
      document.body.appendChild(a);

      a.href = "/api/15306/download";
      a.setAttribute("download", "15306.pdf");
      a.setAttribute("target", "_blank");
      a.click();

      document.body.removeChild(a);

    } catch (e) {
      console.error("Error getting PDF", e);
    }
  };
}

export default () => {
  const [forms, setForms] = useState([] as string[]);
  useEffect(() => {
    FormStorageHelper.get(FormTitle, 100).then(data => setForms((data && data.length) ? data : []));
  }, []);

  
  return (
    <Layout>
      <h2>Summary</h2>
      <div>
        <table>
          <tr>
            <td className="c1">Form 15306:</td>
            <td className="c2">100% complete</td>
          </tr>
          <tr>
            <td className="c1">Required Documents:</td>
            <td className="c2">100% complete</td>
          </tr>
        </table>
        <div className="congrats">You’re all set! Click below to download your documents.</div>
        <div className="center"><a onClick={getPdf(forms)}>Download</a></div>
      </div>
      <style jsx>{`
        h2 {
          margin-top: 0;
          font-size: 43px;
        }
        table {
          width: 100%;
        }
        table td {
          padding-bottom: 16px;
        }
        .c1 {
          text-align: right;
          font-weight: bold;
          font-size: 20px;
        }
        .c2 {
          color: #2CA01C;
          font-size: 20px;
        }
        .congrats {
          margin-top: 48px;
          text-align: center;
          font-size: 24px;
          padding: 0 60px;
        }
        button {
          margin-top: 32px;
          padding: 0;
          background: #1382E9;
          width: 165px;
          height: 48px;
          font-size: 20px;
        }
        .center {
          margin-top: 20px;
          display: flex;
          justify-content: center;
        }
        button[disabled] {
          background: #999;
        }
        button {
          display: inline-block;
          color: white;
          background: #1382E9;
          border-radius: 5px;
          padding: 12px 16px;
          font-size: 20px;
          margin: auto;
        }
      `}</style>
    </Layout>
    );
};


const pdfRequest: PdfRequest = {
  "physical-damage": false,
  "real-property": false,
  "business-contents": false,
  "economic-injury": true,
  "partnership": false,
  "limited-partnership": true,
  "limited-liability-entity": false,
  "corporation": false,
  "nonprofit-organization": false,
  "trust": false,
  "organization-type-other": false,
  "organization-type-other-string": "",
  "business-legal-name": "Demo Biz",
  "federal-ein": "55-5555555",
  "trade-name": "Demo Trade Name",
  "business-phone": "555-555-5555",
  "mailing-business": true,
  "mailing-home": false,
  "mailing-temp": false,
  "mailing-other": false,
  "mailing-other-string": "",
  "mailing-address-street": "123 Main St",
  "mailing-address-city": "Seattle",
  "mailing-address-state": "WA",
  "mailing-address-zip": "98191",
  "mailing-address-county": "King",
  "mailing-address-is-damaged": true,

  "damaged-property-leased-0": true,
  "damaged-property-owned-0": false,

  "damaged-property-street-0": "",
  "damaged-property-city-0": "",
  "damaged-property-state-0": "",
  "damaged-property-zip-0": "",
  "damaged-property-county-0": "",
  "loss-verification-inspection-name": "John Smith",
  "loss-verification-inspection-tel": {
      "telephone_number_loss_verification": "412-555-5555"
  },
  "application-process-poc-name": "Jimmy Smith",
  "application-process-poc-tel": "555-555-8976",
  "alternate-way-to-contact-phone": false,
  "alternate-way-to-contact-email": true,
  "alternate-way-to-contact-fax": false,
  "alternate-way-to-contact-other": false,
  "alternate-way-to-contact-phone-string": "",
  "alternate-way-to-contact-email-string": "test@example.com",
  "alternate-way-to-contact-fax-string": "",
  "alternate-way-to-contact-other-string": "",
  "business-activity": "Selling",
  "employees-pre-disaster": "20",
  "established-date": "2020-03-01",
  "management-start-date": "2020-03-02",
  "estimated-loss-real-estate": true,
  "estimated-loss-inventory": true,
  "estimated-loss-m-e": true,
  "estimated-loss-leasehold": true,
  "estimated-loss-real-estate-string": "100,000",
  "estimated-loss-inventory-string": "5554",
  "estimated-loss-m-e-string": {
      "machinery_and_equipment_estimated_loss": "29999"
  },
  "estimated-loss-leasehold-string": "234234",

  "insurance-coverage-type-0": "Basic",
  "insurance-coverage-name-and-agent-0": "Jimmy Smith",
  "insurance-agent-phone-0": "555-444-7895",
  "insurance-policy-number-0": "283-44488823",

  "owner-legal-name-0": "Will Wilhelm",
  "owner-title-0": "Sr. Manager",
  "owner-percentage-0": "50",
  "owner-email-0": "test2@example.com",
  "owner-citizen-yes-0": true,
  "owner-citizen-no-0": false,
  "owner-ssn-0": "050-48-9875",
  "owner-marital-status-0": "Married",
  "owner-dob-0": "1981-03-05",
  "owner-birth-place-0": "New Rochelle",
  "owner-tel-number-0": "888-555-5468",
  "owner-street-0": "543 Main st",
  "owner-city-0": "New Rochelle",
  "owner-state-0": "New York",
  "owner-zip-0": "10801",
  "business-owner-name-0": "Fredrick's Auto Parts",
  "business-owner-ein-0": "45645",
  "business-owner-type-0": "Type A",
  "business-owner-percentage-0": "50",
  "business-owner-street-0": "500 fredrick st.",
  "business-owner-city-0": "Brooklyn",
  "business-owner-state-0": "NE",
  "business-owner-zip-0": "05987",
  "business-owner-email-0": "test3@example.com",
  "business-owner-phone-0": "555-951-8753",

  "bankruptcy-yes": false,
  "bankruptcy-no": true,
  "lawsuits-yes": false,
  "lawsuits-no": true,
  "convicted-yes": false,
  "convicted-no": true,
  "federal-loan-yes": false,
  "federal-loan-no": true,
  "delinquent-yes": false,
  "delinquent-no": true,
  "member-yes": false,
  "member-no": true,
  "suspended-yes": false,
  "suspended-no": true,
  "indictment-yes": false,
  "indictment-no": true,
  "indictment-name": "",
  "additional-funds": true,
  "owners_and_other_information": {
      "signature_of_individual": "Will Wilhelm",
      "print_individual_name": "Will Wilhelm",
      "name_of_company": "Covid Reserve",
      "phone_number_include_area_code": "555-555-5555",
      "street_address_city_state_zip": "161 E 110th. New York, NY 10029",
      "fee_charged_or_agreed_upon": "",
      "unless_the_no_box_is_checked_i_give_permission_for_sba_to_discuss_any_portion_of_this_application_with_the_representative_listed_above_no": false,
      "signature": {
        "text": {
          "name": "Will Wilhelm",
          "typeface": "Caveat"
        }
      }
  },
  "signee-title": "Owner",
  "signed-date": "2020-03-18"
};
