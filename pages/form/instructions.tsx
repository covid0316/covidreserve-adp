import Layout from '../../components/layout';
import Link from 'next/link';

export default () => {
  return (
    <Layout>
      <h2>Instructions</h2>
      <div>
        <table>
          <tr>
            <td className="c1"><div>1</div></td>
            <td className="c2">Fill out each of the required fields in all of the tabs you see above. You’ll see a next to the form that are complete. You can see your overall application progress at any time in the Summary tab.</td>
          </tr>
          <tr>
            <td className="c1"><div>2</div></td>
            <td className="c2">Submit your application via the “Submit” button on the “Summary” tab. We’ll email you a confirmation that it’s been sent.</td>
          </tr>
        </table>
      </div>
      <div className="center">
        <Link href="/form/15306/0">
          <a className="button">Start</a>
        </Link>
      </div>
      <style jsx>{`
        h2 {
          margin-top: 0;
          font-size: 43px;
          font-weight: normal;
        }
        table {
          width: 100%;
        }
        table td {
          padding-top: 0;
          padding-bottom: 16px;
        }
        .c1 {
          font-weight: bold;
          font-size: 20px;
          vertical-align: top;
        }
        .c1 div {
          text-align: center;
          display: inline-block;
          color: white;
          width: 48px;
          height: 48px;
          border-radius: 24px;
          background-color: #116DC1;
          line-height: 48px;
          margin-right: 20px;
        }
        .c2 {
          font-size: 20px;
        }
        .congrats {
          margin-top: 48px;
          text-align: center;
          font-size: 24px;
          padding: 0 60px;
        }
        button {
          margin-top: 32px;
          padding: 0;
          background: #1382E9;
          width: 165px;
          height: 48px;
          font-size: 20px;
        }
        .center {
          margin-top: 20px;
          display: flex;
          justify-content: center;
        }
        button[disabled] {
          background: #999;
        }
        a.button, a.button:visited, a.button:active {
          display: inline-block;
          color: white;
          background: #1382E9;
          border-radius: 5px;
          padding: 12px 16px;
          font-size: 20px;
          margin: auto;
        }
      `}</style>
    </Layout>
    );
};