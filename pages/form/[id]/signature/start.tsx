import Script from 'react-load-script';
import { NextPage } from 'next';
import { useRouter } from 'next/router';
import Layout from '../../../../components/layout';
import { useEffect, useState } from 'react';
import { DataRequestToken } from '../../../../lib/docspringClient';

type Props = {};

const Signature: NextPage<Props> = ({}) => {
  const router = useRouter();
  const [loaded, setLoaded] = useState(false);
  const { id, formName, testing, request, token, tokenSecret } = router.query as { [id: string]: string };
  const reqFields = router.query.fields as string[];
  const qFields = (reqFields && reqFields.length) ?
    reqFields.map(field => `fields=${encodeURIComponent(field)}`).join("&") :
    undefined;

  useEffect(() => {
    if (loaded) {
      if (request && token && tokenSecret) {
        if (window) {
          const protocolHostPort = window.location.protocol + '//' + window.location.hostname +
            (window.location.port ? ':' + window.location.port: '');
          (window as any).DocSpring.createVisualForm({
            dataRequestId: request,
            tokenId: token,
            tokenSecret: tokenSecret,
            domainVerification: false,
            redirectURL: `${protocolHostPort}/api/${encodeURIComponent(id)}/signComplete`
          });
        }
      } else {
        fetch(`/api/sign?formId=${encodeURIComponent(id)}` +
            (formName ? `&formName=${encodeURIComponent(formName)}` : '') +
            (qFields ? `&${qFields}` : ''))
          .then((data) => data.json())
          .then((data: DataRequestToken) => {
            if (window) {
              const protocolHostPort = window.location.protocol + '//' + window.location.hostname +
                (window.location.port ? ':' + window.location.port: '');
              (window as any).DocSpring.createVisualForm({
                dataRequestId: data.request_id,
                tokenId: data.id,
                tokenSecret: data.secret,
                domainVerification: false,
                redirectURL: `${protocolHostPort}/api/${encodeURIComponent(id)}/signComplete`
              });
            }
          });
      }
    }
  }, [id, formName, loaded])



  return (
    <Layout>
      <Script
        url="https://cdn.docspring.com/embed/data_request.v2.1.3.js"
        onLoad={() => setLoaded(true)}
        />
      <h2>Signature</h2>
      <p>Please review and sign the form to complete the process.</p>

    </Layout>
  );
}

export default Signature;