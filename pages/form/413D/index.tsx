import { useState, useEffect } from 'react';
import Link from 'next/link';
import { Formik, Form, Field } from 'formik';
import * as Yup from 'yup';
import { FormStorageHelper, flattenListForm } from '../../../lib/storage';
import Layout from '../../../components/layout';
import { Pen, X } from '../../../components/Icons';
import ProgressBar from '../../../components/ProgressBar';
import AddButtonFab from '../../../components/AddButtonFab';

const _413DFormsSchema = Yup.object().shape({
  name: Yup.string().required("Name is required")
});

type FormName = {
  name: string
};

const FormTitle = '413D';

export default () => {
  const [forms, setForms] = useState({});
  const [isAdding, setIsAdding] = useState(false);

  const addForm = () => {
    if (!isAdding) {
      setIsAdding(true);
    }
  };

  const deleteForm = (formName: string) => async (evt: any) => {
    await FormStorageHelper.delete(FormTitle, formName);
    const newForms = {...forms};
    delete newForms[formName];
    setForms(newForms);
  }

  const createForm = async ({ name: formName }: FormName, { resetForm, setErrors }) => {
    if (isAdding) {
      if (Object.keys(forms).indexOf(formName) === -1) {
        await FormStorageHelper.set({}, FormTitle, 0, formName);
        setForms({...forms, [formName]: {}});
        setIsAdding(false);
        resetForm({});
      } else {
        setErrors({ name: 'This form already exists' });
      }
    }
  };

  const isComplete = (value: any): boolean => {
    if (value === null || value === undefined) {
      return false;
    } else if (typeof value === 'string') {
      return value.trim() !== '';
    } else if (typeof value === 'number') {
      return true;
    }
    return false;
  };

  const percentDone = (form: any): number => {    
    const requiredFields = [
      '413-name',
      '413-address',
      '413-city-state-zip',
      '413-res-phone',
      '413-applicant-business-name',
      '413-business-phone',
      "413-cash",
      "413-savings",
      "413-ira",
      "413-a-n-r",
      "413-life-insurance",
      "413-stocks",
      "413-automobiles",
      "413-other-pp",
      "413-other-assets",
      "413-accounts-payable",
      "413-notes-payable",
      "413-install-auto",
      "413-install-other",
      "413-loan-life",
      "413-mortgage",
      "413-unpaid-tax",
      "413-other-liabilites",
    ];
    const flattenedForm = flattenListForm(form);

    const fieldsComplete = requiredFields.filter(fieldName => isComplete(flattenedForm[fieldName]));
    const totalFields = requiredFields.length;
    return fieldsComplete.length / totalFields * 100;
  };

  useEffect(() => {
    FormStorageHelper.get(FormTitle).then(data => setForms(data || {}));
  }, []);

  if (!forms) {
    return <Layout>Loading...</Layout>
  }

  return (
    <Layout>
      <div className="instructions">
        <p style={{fontWeight: "bold"}}>The SBA uses the information from 413D as one of a number of data sources in analyzing the repayment ability and creditworthiness of an application for an SBA disaster loan.</p>
        <p style={{fontWeight: "bold"}}>Complete this form for:</p>
        <ul>
          <li>Each proprietor</li>
          <li>Each general partner</li>
          <li>Managing member of an LLC</li>
          <li>Each owner with 20% or more of the equity of the Applicant business (including the assets of the owner’s spouse and any minor children)</li>
          <li>Any person providing guaranty on the loan</li>
        </ul>
        <div className="inner">
          <p>Name of proprietor, general partner, managing member of an LLC, owner with 20% or more equity, or person providing guaranty</p>
          { Object.entries(forms)
            .sort((a, b) => (a[0] > b[0]) ? 1 : (a[0] == b[0]) ? 0 : -1)
            .filter(e => e[0] !== '_id')
            .map((entry) => {
              const [formName, formContent] = entry;
              return (
                <div className="row">
                  <div><label htmlFor="name">Name</label></div>
                  <div className="item">
                    <Link href={`/form/413D/attachment/${encodeURIComponent(formName)}/0`}>
                      <a>{Pen} {formName}</a>
                    </Link>
                  </div>
                  <ProgressBar amountComplete={percentDone(formContent)} width={123} size='compact' />
                  <div className="details">
                    <span className="delete" onClick={deleteForm(formName)}>{X}</span>
                  </div>
                </div>
              );
            })
          }
          { isAdding ?
            <Formik initialValues={{
              name: ''
            }} validationSchema={_413DFormsSchema} onSubmit={createForm}
            >
              {({ errors, touched }) => (
                <Form>
                  <div className="formRow">
                    <div><label htmlFor="name">Name</label></div>
                    <div style={{width: 425}}><Field name="name"/>
                      <div className="validate">
                        {errors.name && touched.name && errors.name}&nbsp;
                      </div>
                    </div>
                    <button type="submit">Create Form</button>
                  </div>
                </Form>
              )}
            </Formik>
            : null
          }
          <AddButtonFab className={isAdding ? 'disabled' : ''} label="Add Another Form 413D" onClick={addForm} />
        </div>
      </div>
      <style jsx>
        {`
          .delete {
            cursor: pointer;
          }
          .row {
            display: flex;
            justify-content: flex-start;
          }
          .row .item {
            line-height: 31px;
            padding: 0 20px;
          }
          .row :global(svg) {
            vertical-align: text-bottom;
            display: inline-block;
            position: relative;
            top: .125em;
          }
          .formRow {
            display: flex;
            justify-content: space-between;
          }
          h2 {
            font-size: 48px;
            text-align: center;
            font-weight: normal;
          }
          p {
            font-size: 20px;
            line-height: 142%;
            margin-bottom: 0;
          }
          .center {
            text-align: center;
          }
          li {
            padding-top: 20px;
            font-size: 20px;
          }
          .inner {
            padding-left: 130px;
          }
          .inner p {
            font-size: 16px;
            font-weight: bold;
            margin: 16px 0;
          }
          .instructions :global(.fabbutton.disabled .fab) {
            background: #B1B9C0;
          }
          .instructions :global(.fabbutton.disabled) {
            cursor: default;
          }
          .formRow :global(input) {
            color: #4B6075;
            font-size: 16px;
            font-family: inherit;
            box-sizing: border-box;
            position: relative;
            width: 100%;
            margin: 0;
            border-color: black;
            padding: 5px 20px;
          }
          label {
            font-weight: bold;
            line-height: 31px;
            margin-bottom: 0;
          }
          .validate {
            line-height: 16px;
            color: #D92C23;
            font-size: 12px;
          }
          button {
            padding: 0 40px;
            background: #1382E9;
            font-size: 20px;
            height: 31px;
          }
        `}
      </style>
    </Layout>
  );
};
