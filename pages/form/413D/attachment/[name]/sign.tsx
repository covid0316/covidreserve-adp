import { NextPage } from 'next';
import { useRouter } from 'next/router';
import FormikWrapper from '../../../../../components/FormikWrapper';
import Layout from '../../../../../components/layout';
import * as storage from '../../../../../lib/storage';
import Link from 'next/link';
import { useEffect, useState } from 'react';

type Props = {};

const FormTitle = '413D';

const _413DSign: NextPage<Props> = () => {
  const router = useRouter();
  const [formData, setFormData] = useState({});
  const formName = router.query.name as string;

  useEffect(() => { storage.FormStorageHelper.get(FormTitle).then(setFormData); }, [formName]);

  if (!formName || !formData || !Object.keys(formData).length) {
    return (<Layout>Loading...</Layout>);
  }

  const form = storage.flattenListForm(formData[formName]);
  const format = (n: string | number) =>
    <div className="inner">{parseInt((n || 0).toString(), 10).toLocaleString()}</div>;
  const sum = (fields: Array<string>): number =>
    fields.map(field => form[field] || 0).reduce((acc, amt) => acc + parseFloat(amt), 0);
  form["413-total-assets"] = sum([
    "413-cash",
    "413-savings",
    "413-ira",
    "413-a-n-r",
    "413-life-insurance",
    "413-stocks",
    "413-real-estate",
    "413-automobiles",
    "413-other-pp",
    "413-other-assets",
  ]);
  form["413-total-liabilites"] = sum([
    "413-accounts-payable",
    "413-notes-payable",
    "413-install-auto",
    "413-install-other",
    "413-loan-life",
    "413-mortgage",
    "413-unpaid-tax",
    "413-other-liabilites",
  ])
  form["413-net-worth"] = (form["413-total-assets"] || 0) - (form["413-total-liabilites"] || 0);
  form["413-total-and-net"] = (form["413-total-liabilites"] || 0) + (form["413-net-worth"] || 0);
  return (
    <Layout>
      <h2>Summary</h2>
      <h3>Assets</h3>
      <div className="balance-sheet">
        <div>Cash on Hand &amp; in Banks:</div>
        <div className="amount">${format(form["413-cash"])}</div>
        <div>Savings Accounts:</div>
        <div className="amount">${format(form["413-savings"])}</div>
        <div>IRA or Other Retirement Account:</div>
        <div className="amount">${format(form["413-ira"])}</div>
        <div>Accounts &amp; Notes Receivable:</div>
        <div className="amount">${format(form["413-a-n-r"])}</div>
        <div>Life Insurance - Cash Surrender Value Only:</div>
        <div className="amount">${format(form["413-life-insurance"])}</div>
        <div>Stocks &amp; Bonds:</div>
        <div className="amount">${format(form["413-stocks"])}</div>
        <div>Real Estate:</div>
        <div className="amount">${format(form["413-real-estate"])}</div>
        <div>Automobiles - Total Present Value:</div>
        <div className="amount">${format(form["413-automobiles"])}</div>
        <div>Other Personal Property:</div>
        <div className="amount">${format(form["413-other-pp"])}</div>
        <div>Other Assets:</div>
        <div className="amount">${format(form["413-other-assets"])}</div>
        <div className="total">TOTAL ASSETS:</div>
        <div className="amount">${format(form["413-total-assets"])}</div>
      </div>
      <h3>Liabilities</h3>
      <div className="balance-sheet">
        <div>Accounts Payable:</div>
        <div className="amount">${format(form["413-accounts-payable"])}</div>
        <div>Notes Payable to Banks and Others:</div>
        <div className="amount">${format(form["413-notes-payable"])}</div>
        <div>Installment Account (Auto):</div>
        <div className="amount">${format(form["413-install-auto"])}</div>
        <div>Installment Account (Other):</div>
        <div className="amount">${format(form["413-install-other"])}</div>
        <div>Loan on Life Insurance:</div>
        <div className="amount">${format(form["413-loan-life"])}</div>
        <div>Mortgages on Real Estate:</div>
        <div className="amount">${format(form["413-mortgage"])}</div>
        <div>Unpaid Taxes:</div>
        <div className="amount">${format(form["413-unpaid-tax"])}</div>
        <div>Other Liabilities:</div>
        <div className="amount">${format(form["413-other-liabilites"])}</div>
        <div className="total">TOTAL LIABILITIES:</div>
        <div className="amount">${format(form["413-total-liabilites"])}</div>
        <div className="total">NET WORTH:</div>
        <div className="amount">${format(form["413-net-worth"])}</div>
        <div className="total">TOTAL LIABILITIES &amp; NET WORTH:</div>
        <div className="amount">${format(form["413-total-and-net"])}</div>
      </div>
      <p className="alldone">You’re all set! Click the button below to submit your disaster relief application to the SBA.</p>
      <div className="center"><Link href={`/form/413D/attachment/${encodeURIComponent(formName)}/sign`}><a>Sign &amp; Complete 413D</a></Link></div>
      <style jsx>{`
        .balance-sheet {
          display: flex;
          flex-wrap: wrap;
          font-size: 20px;
        }
        .balance-sheet > div {
          flex-basis: 70%;
          padding-bottom: 10px;
        }
        .balance-sheet > div.amount {
          flex-basis: 15%;
          position: relative;
        }
        .balance-sheet > div.amount > :global(.inner) {
          text-align: right;
          display: inline-block;
          position: absolute;
          right: 0;
        }
        h2 {
          font-size: 48px;
          font-weight: normal;
          text-align: center;
        }
        h3 {
          padding-top: 25px;
          font-size: 48px;
        }
        .total, .amount {
          font-weight: bold;
        }
        p.alldone {
          padding: 40px 60px 0 60px;
          font-size: 24px;
          text-align: center;
        }
        .center {
          text-align: center;
        }
        .center :global(a),
        .center :global(a:hover),
        .center :global(a:visited) {
          display: inline;
          background: #1382E9;
          border-radius: 5px;
          padding: 12px 16px;
          font-size: 20px;
          color: white !important;
        }
      `}</style>
    </Layout>
  );
};

export default _413DSign;