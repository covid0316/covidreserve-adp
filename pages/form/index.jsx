import React, { useContext } from 'react';
import Head from 'next/head';
import Link from 'next/link';
import Layout from '../../components/layout';
import { UserContext } from '../../components/UserContext';
import fetchSwal from '../../lib/fetchSwal';

const FormHomePage = () => {
  const {
    state: {
      isLoggedIn,
      user: {
        name, email, emailVerified,
      },
    },
  } = useContext(UserContext);

  function sendVerificationEmail() {
    fetchSwal.post('/api/user/email/verify');
  }

  // Loading state
  if (isLoggedIn === undefined) {
    // @todo: loading screen goes here
    return <Layout />;
  }

  if (isLoggedIn === false) {
    return (
      <Layout>
        <p>Please sign in</p>
      </Layout>
    );
  }


  return (
    <Layout>
      <style jsx>
        {`
          h2 {
            text-align: left;
            margin-right: 0.5rem;
          }
          button {
            margin: 0 0.25rem;
          }
          img {
            width: 10rem;
            height: auto;
            border-radius: 50%;
            box-shadow: rgba(0, 0, 0, 0.05) 0 10px 20px 1px;
            margin-right: 1.5rem;
          }
          div {
            color: #777;
            display: flex;
            align-items: center;
          }
          p {
            font-family: monospace;
            color: #444;
            margin: 0.25rem 0 0.75rem;
          }
          a {
            margin-left: 0.25rem;
          }
        `}
      </style>
      <Head />
      <div>
        <section>
          <div>
            <h2>{name}</h2>
            <Link href="/profile/settings">
              <button type="button">Edit</button>
            </Link>
          </div>
          Email
          <p>
            {email}
            {!emailVerified ? (
              <>
                {' '}
                unverified
                {' '}
                {/* eslint-disable-next-line */}
                <a role="button" onClick={sendVerificationEmail}>
                  Send verification email
                </a>
              </>
            ) : null}
          </p>
        </section>
      </div>
    </Layout>
  );
};

export default FormHomePage;
