import React, { useState } from 'react';
import Head from 'next/head';
import { Formik, Form, Field } from 'formik';
import * as Yup from 'yup';
import fetchSwal from '../../lib/fetchSwal';
import Layout from '../../components/homeLayout';
import { Logo } from '../../components/Icons';
import redirectTo from '../../lib/redirectTo';

const ForgotPasswordSchema = Yup.object().shape({
  email: Yup.string().email("Email is invalid").required("Email is required")
});

const ForgetPasswordPage = () => {
  const [pwResetError, setPwResetError] = useState('');
  const [completed, setCompleted] = useState(false);
  function handleSubmit({ email }) {
    fetchSwal.post('/api/user/password/reset', { email })
      .then(resp => {
        setCompleted(true);
        if (resp.ok) {
          setCompleted(true)
        } else if (resp.message) {
          setPwResetError(resp.message);
        } else {
          setPwResetError("Sorry, something went wrong. Please try again.");
        }
      })
      .catch(err => {
        if (err.message) {
          setPwResetError(err.message);
        } else {
          setPwResetError("Sorry, something went wrong. Please try again.");
        }
      })
  }

  return (
    <Layout>
      <Head>
        <title>Forget password</title>
      </Head>
      <div className="signup">
        <div className="head">
          <h2>{Logo}</h2>
        </div>
        { ! completed ? 
            <div className="body">
              <h2>Reset my password</h2>
              <h3>Enter the email you used to create your CovidReserve account</h3>
              <div className="inner">
                <Formik initialValues={{
                    email: ''
                  }}
                  validationSchema={ForgotPasswordSchema}
                  onSubmit={handleSubmit}
                >
                  {({ errors, touched }) => (
                    <Form>
                      <label htmlFor="email">
                        <div>Email address*</div>
                      </label>
                      <div className="form-control-wrapper">
                      <Field name="email" />
                      </div>
                      <div className="validate">
                        {errors.email && touched.email && errors.email}&nbsp;
                      </div>
                      <div className="validate">
                        {pwResetError}
                      </div>
                      <button type="submit">Reset my password</button>
                    </Form>
                  )}
                </Formik>
              </div>
            </div>
          :
            <div className="body">
              <h2 style={{padding: "0 70px"}}>A password reset email has been sent!</h2>
              <h3>If you don’t see it in your inbox, please check yours spam folder.</h3>
            </div>
        }
      </div>
      <style jsx>{`
        .head {
          background: #1382E9;
          border-radius: 5px 5px 0 0;
        }
        .head h2 {
          margin: 0;
          padding: 20px 0;
          color: white;
          font-weight: bold;
          font-size: 28px;
        }
        .head h2 :global(svg) {
          vertical-align: text-bottom;
          display: inline-block;
          position: relative;
        }
        .body { 
          box-sizing: border-box;
          padding: 20px;
          border-radius: 0 0 5px 5px;
          border-bottom: 1px solid #4B6075;
          border-left: 1px solid #4B6075;
          border-right: 1px solid #4B6075;
        }
        .body h2 {
          font-size: 34px;
          font-weight: normal;
          margin-bottom: 0;
        }
        .body h3 {
          font-size: 16px;
          font-weight: normal;
          margin-top: 5px;
          margin-bottom: 20px;
          text-align: center;
          color: #4B6075;
        }
        .signup {
          width: 566px;
          margin: 36px auto 0 auto;
          background: white;
        }
        .inner {
          padding: 0 99px;
        }
        .center {
          margin-top: 10px;
          text-align: center;
          font-size: 20px;
        }
        label > div {
          font-size: 18px;
        }
        .validate {
          line-height: 16px;
          color: #D92C23;
          font-size: 12px;
        }
        .form-control-wrapper {
          box-sizing: border-box;
          position: relative;
          width: 100%;
        }
        .form-control-wrapper :global(input) {
          color: #4B6075;
          font-size: 18px;
          font-family: inherit;
          box-sizing: border-box;
          position: relative;
          width: 100%;
          margin: 0;
          border-color: black;
          padding: 10px 20px;
        }
        button {
          margin: 20px auto 0 auto;
          padding: 0;
          background: #1382E9;
          width: 231px;
          height: 48px;
          font-size: 20px;
        }
      `}</style>
    </Layout>
  );
};

export default ForgetPasswordPage;
