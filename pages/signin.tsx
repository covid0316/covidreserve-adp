import React, { useEffect, useContext, useState } from 'react';
import Head from 'next/head';
import Link from 'next/link';
import { useRouter } from 'next/router';
import { Formik, Form, Field } from 'formik';
import fetchSwal from '../lib/fetchSwal';
import { UserContext } from '../components/UserContext';
import Layout from '../components/homeLayout';
import { Logo } from '../components/Icons';
import * as Yup from 'yup';

type User = {
  email: string,
  password: string
}

const SignupSchema = Yup.object().shape({
  email: Yup.string().email("Email is invalid").required("Email is required"),
  password: Yup.string().required("Password is required")
});

const LoginPage = () => {
  const { dispatch } = useContext(UserContext);
  const [loginError, setLoginError] = useState('');
  const {
    state: {
      isLoggedIn,
      user: { name },
    },
  } = useContext(UserContext);
  const router = useRouter();
  useEffect(() => {
    if (isLoggedIn) {
      router.replace("/form/instructions");
    }
  });

  const handleSubmit = (user: User) => {
    setLoginError('');
    fetchSwal
      .post('/api/authenticate', user)
      .then(resp => {
        if (resp.ok) {
          // we should swap this out with a friendly spa redirect
          // if redux is added. For now if we don't do this,
          // the menu is wrong on the next page since it was
          // rendered when we were logged out
          window.location.href = "/form/instructions";
        } else if (resp.message) {
          setLoginError(resp.message);
        } else {
          setLoginError('Sorry, something went wrong. Please try again.');
        }
      })
      .catch(err => {
        if (err.message) {
          setLoginError(err.message);
        } else {
          setLoginError('Sorry, something went wrong. Please try again.');
        }
      })
  };

  return (
    <Layout>
      <Head>
        <title>Sign in</title>
      </Head>
      <div className="signup signup-width">
        <div className="head">
          <h2>{Logo}</h2>
        </div>
        <div className="body">
          <h2 className="text-center">Sign in</h2>
          <h3>to continue to your CovidReserve account</h3>
          <div className="inner">
            <Formik initialValues={{
                email: '',
                password: ''
              }}
              validationSchema={SignupSchema}
              onSubmit={handleSubmit}
            >
              {({ errors, touched }) => (
              <Form>
                <label htmlFor="email">
                  <div>Email address*</div>
                </label>
                <div className="form-control-wrapper">
                < Field name="email" />
                </div>
                <div className="validate">
                  {errors.email && touched.email && errors.email}&nbsp;
                </div>
                <label htmlFor="password">
                  <div>Password*</div>
                </label>
                <div className="form-control-wrapper">
                  <Field name="password" type="password" />
                </div>
                <div className="validate">
                  {errors.password && touched.password && errors.password}&nbsp;
                </div>
                <Link href="/forgotpassword">
                  <a>Forgot password?</a>
                </Link>
                <div className="validate">
                  {loginError}
                </div>
                <div className="text-center">
                <button type="submit">Sign in</button>
                </div>
                <div className="center">
                  <Link href="/signup">
                    <a>Sign up for an account</a>
                  </Link>
                </div>
              </Form>
            )}
            </Formik>
          </div>
        </div>
      </div>
      <style jsx>{`
        .head {
          background: #1382E9;
          border-radius: 5px 5px 0 0;
          text-align: center;
        }
        .form-group input {
  margin-left: 0 !important;
}
.contact_frm {
    padding: 1rem 3rem;
}
.container-custom {
    width: 80%;
    margin: auto;
}
.mt-8 {
  padding-top: 6rem;
}
.row {
                display: flex;
                flex-wrap: wrap;
                margin-right: -15px;
                margin-left: -15px;
            }
.form-control {
    display: block;
    width: 100%;
    height: calc(1.5em + 0.75rem + 2px);
    padding: 0.375rem 0.75rem;
    font-size: 1rem;
    font-weight: 400;
    line-height: 1.5;
    color: #495057;
    background-color: #fff;
    background-clip: padding-box;
    border: 1px solid #ced4da;
    border-radius: 0.25rem;
    transition: border-color 0.15s ease-in-out, box-shadow 0.15s ease-in-out;
}
.form-group {
    margin-bottom: 1rem;
}
label {
    display: inline-block;
    margin-bottom: 0.5rem;
}
body {
    margin: 0;
    /*font-family: -apple-system, BlinkMacSystemFont, "Segoe UI", Roboto, "Helvetica Neue", Arial, "Noto Sans", sans-serif, "Apple Color Emoji", "Segoe UI Emoji", "Segoe UI Symbol", "Noto Color Emoji";*/
    font-size: 1rem;
    font-weight: 400;
    line-height: 1.5;
    color: #212529;
    text-align: left;
    background-color: #fff;
}
textarea {
    overflow: auto;
    resize: vertical;
}
input, button, select, optgroup, textarea {
    margin: 0;
    font-family: inherit;
    font-size: inherit;
    line-height: inherit;
}
.text-center {
    text-align: center;
}
.blockquote {
    margin-bottom: 1rem;
    font-size: 1.25rem;
}
.img-fluid {
    max-width: 100%;
    height: auto;
}
.card-body {
    flex: 1 1 auto;
    min-height: 1px;
    padding: 1.25rem;
}
button {
    border: none;
}
.justify-content-center {
    justify-content: center !important;
}
 *, *::before, *::after {
                    box-sizing: border-box;
                }

            .col-1, .col-2, .col-3, .col-4, .col-5, .col-6, .col-7, .col-8, .col-9, .col-10, .col-11, .col-12, .col, .col-auto, .col-sm-1, .col-sm-2, .col-sm-3, .col-sm-4, .col-sm-5, .col-sm-6, .col-sm-7, .col-sm-8, .col-sm-9, .col-sm-10, .col-sm-11, .col-sm-12, .col-sm, .col-sm-auto, .col-md-1, .col-md-2, .col-md-3, .col-md-4, .col-md-5, .col-md-6, .col-md-7, .col-md-8, .col-md-9, .col-md-10, .col-md-11, .col-md-12, .col-md, .col-md-auto, .col-lg-1, .col-lg-2, .col-lg-3, .col-lg-4, .col-lg-5, .col-lg-6, .col-lg-7, .col-lg-8, .col-lg-9, .col-lg-10, .col-lg-11, .col-lg-12, .col-lg, .col-lg-auto, .col-xl-1, .col-xl-2, .col-xl-3, .col-xl-4, .col-xl-5, .col-xl-6, .col-xl-7, .col-xl-8, .col-xl-9, .col-xl-10, .col-xl-11, .col-xl-12, .col-xl, .col-xl-auto {
                position: relative;
                width: 100%;
                padding-right: 15px;
                padding-left: 15px;
            }
             .signup-width{
                width: 80%;
              }


             svg {
                overflow: hidden;
                vertical-align: middle;
            }
            @media (min-width: 992px) {
            .col-lg-4 {
                flex: 0 0 33.333333%;
                max-width: 33.333333%;
            }
             .col-lg-6 {
                flex: 0 0 50%;
                max-width: 50%;
             }
            .col-lg-2 {
                flex: 0 0 16.666667%;
                max-width: 16.666667%;
            }
          .col-lg-3 {
    flex: 0 0 25%;
    max-width: 25%;
}}
@media (max-width: 567px) {
  .signup-width{
      width: 95%;
  }
  .col-sm-12 {
    flex: 0 0 100%;
    max-width: 100%;
  }
}
@media (max-width: 700px) {
  .mobile-form-deny{
    background-color: white;
    height: 100%;
    width: 100%;
    position: fixed;
    bottom: 0px;
    text-align: center;
  }
  .mobile-form-deny-text{
    position: relative;
    top: 50%;
    transform: translateY(-50%);
  }
}
@media (min-width: 701px) {
  .mobile-form-allow-text{
    color: transparent;
  }
}



@media (min-width: 576px) {
.col-sm-6 {
    flex: 0 0 50%;
    max-width: 50%;
}}

@media screen and (min-width: 1074px) {
  .signup-width{
    width: 38%;
  }
}


article, aside, figcaption, figure, footer, header, hgroup, main, nav, section {
    display: block;
}

        .head h2 {
          margin: 0;
          padding: 20px 0;
          color: white;
          font-weight: bold;
          font-size: 28px;
        }
        .head h2 :global(svg) {
          vertical-align: text-bottom;
          display: inline-block;
          position: relative;
        }
        .body {
          box-sizing: border-box;
          padding: 1rem 3rem;
          border-radius: 0 0 5px 5px;
          border-bottom: 1px solid #4B6075;
          border-left: 1px solid #4B6075;
          border-right: 1px solid #4B6075;
          // text-align: center;
          margin: 0;
          font-family: -apple-system, BlinkMacSystemFont, "Segoe UI", Roboto, "Helvetica Neue", Arial, "Noto Sans", sans-serif, "Apple Color Emoji", "Segoe UI Emoji", "Segoe UI Symbol", "Noto Color Emoji";
          font-size: 1rem;
          font-weight: 400;
          line-height: 1.5;
          color: #212529;
          text-align: left;
          background-color: #fff;
        }
        .body h2 {
          font-size: 34px;
          font-weight: normal;
          margin-bottom: 0;
        }
        .body h3 {
          font-size: 16px;
          font-weight: normal;
          margin-top: 5px;
          margin-bottom: 20px;
          text-align: center;
          color: #4B6075;
        }
        .signup {

          margin: 36px auto 0 auto;
          background: white;
        }
        .inner {
          padding: 0 0px;
        }
        .center {
          margin-top: 10px;
          text-align: center;
          font-size: 20px;
        }
        label > div {
          font-size: 18px;
        }
        .validate {
          line-height: 16px;
          color: #D92C23;
          font-size: 12px;
        }
        .form-control-wrapper {
          box-sizing: border-box;
          position: relative;
          width: 100%;
        }
        .form-control-wrapper :global(input) {
          color: #4B6075;
          font-size: 18px;
          font-family: inherit;
          box-sizing: border-box;
          position: relative;
          width: 100%;
          margin: 0;
          border-color: black;
          padding: 10px 20px;
        }
        button {
          margin: 20px auto 0 auto;
          padding: 0;
          background: #1382E9;
          width: 165px;
          height: 48px;
          font-size: 20px;
          border: none;
          border-radius: 5px;
          color: #fff;
        }
        @media (max-width: 567px) {
          .body {
            padding: 1rem 1rem;
          }
        }
      `}</style>
    </Layout>
  );
};

export default LoginPage;
