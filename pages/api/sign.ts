import { pipe } from 'fp-ts/lib/pipeable';
import { FpConnect, authenticate } from '../../lib/tsNextConnect';
import { DocspringClient, FpDocspringClient, DataRequestToken } from '../../lib/docspringClient';
import * as TE from 'fp-ts/lib/TaskEither';
import * as E from 'fp-ts/lib/Either';
import middleware from '../../middlewares/middleware';
import { fromPromise } from '../../lib/util';
import { flattenListForm } from '../../lib/storage';
import { PdfRequest, Form413D } from './data/pdfTypes';

type SignatureRequest = {
  formId: string
};

type Error = {
  message: string,
  code: number
}

const template = {
  '413D': process.env.FORM_TEMPLATE_413D,
  '5': process.env.FORM_TEMPLATE_5,
  '2202': process.env.FORM_TEMPLATE_2202,
  '15306': process.env.FORM_TEMPLATE_15306
}

const client = new FpDocspringClient(new DocspringClient(
  process.env.DOCSPRING_API_TOKEN as string,
  process.env.DOCSPRING_API_TOKEN_SECRET as string
));

const handler = new FpConnect();

handler.useMiddleware(middleware);
handler.get<void, DataRequestToken | Error>((req, res) => {
  const formId = req.query.formId;
  const formName = req.query.formName;
  const fields = req.query.fields as string[];
  const db = req.db!;
  
  return pipe(
    TE.fromEither<any, undefined>(authenticate(req)),
    TE.chain(_ => fromPromise(
      db.collection(`form-${formId}`)
        .findOne({ _id: req.user._id }))),
    TE.chain(formData =>
      pipe(
        TE.fromEither(toPdfRequest(formName ? formData[formName] : formData, formId)),
        TE.chain(data => client.generateDataRequest(
          template[formId],
          req.user.email,
          data,
          fields)),
        TE.chain(dataRequestId => client.createDataRequestToken(dataRequestId.id)))
      ),
    TE.map(result => res.json(result)),
    TE.mapLeft(err => {
      if (err.notAuthorized) {
        res.status(401).end();
      } else {
        console.error("Failed to get signing token for PDF", err);
        res.status(500).json({
          message: "Failed to sign PDF",
          code: 500
        });
      }
    })
  );
});

function toPdfRequest(data: any, formId: string): E.Either<any, PdfRequest> {
  switch (formId) {
    case '15306':
      const excludedFields = [
        "trade-name-no", "trade-name-yes", "business-address-state",
        "business-address-city", "business-address-zip", "business-address-county",
        "owners", "suspended-yes", "default-yes", "indictment-yes", "felony-yes",
        "all-citizens-no", "all-citizens-yes", "affiliates-string", "sba-2020-string",
        "sba-2020-no", "_bsontype", "id", "forms"];
      const form = flattenListForm(data);

      form["owners"].forEach((owner, i) => {
        if (owner) {
          form[`owner-legal-name-${i}`] = owner["owner-legal-name"];
          form[`owner-title-${i}`] = owner["owner-title"];
          form[`owner-percentage-${i}`] = owner["owner-percentage"];
          form[`owner-tin-ein-ssn-${i}`] = owner["owner-tin-ein-ssn"];
          form[`owner-mailing-address-${i}`] = owner["owner-mailing-address-street"] + " " + owner["owner-city"] +", "+ owner["owner-state"] +" "+ owner["owner-zip"];
        }
      });
      form["owner-legal-name-1"] = '';
      form["owner-title-1"] = '';
      form["owner-percentage-1"] = '';
      form["owner-tin-ein-ssn-1"] = '';
      form["owner-mailing-address-1"] = '';
      form["business-address-city-state-zip"] = form["business-address-city"] +", "+ form["business-address-state"] +" "+ form["business-address-zip"];
      form["loan-amount"] = (parseInt((parseFloat(form['average-monthly-payroll'] || 0) * 2.5 * 100).toString(), 10) / 100).toString();
      const request = Object.entries(form)
        .filter(entry => !excludedFields.includes(entry[0]) && !/^[0-9]+$/.test(entry[0]))
        .reduce((obj, entry) => {
          obj[entry[0]] = entry[1];
          return obj;
        }, {});
        
      return E.right(request as PdfRequest);
  }
  return E.left("Invalid form type");
}

export default handler.apply();