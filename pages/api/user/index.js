import nextConnect from 'next-connect';
import multer from 'multer';
import middleware from '../../../middlewares/middleware';

const upload = multer({ dest: '/tmp' });
const handler = nextConnect();

handler.use(middleware);

handler.patch(upload.single('profilePicture'), async (req, res) => {
  try {
    if (!req.user) throw { userFacingMessage: 'You need to be logged in.' };
    const { name, bio } = req.body;
    res.json({
      ok: true,
      message: 'Profile updated successfully',
      data: { name, bio },
    });
  } catch (error) {
    console.log('Error occurred when updating profile.', )
    res.json({
      ok: false,
      message: error.userFacingMessage || 'Sorry, something went wrong. Please try again.',
    });
  }
});

export const config = {
  api: {
    bodyParser: false,
  },
};

export default handler.apply.bind(handler);
