import nextConnect from 'next-connect';
import bcrypt from 'bcryptjs';
import middleware from '../../../../middlewares/middleware';

const handler = nextConnect();
handler.use(middleware);

handler.put(async (req, res) => {
  try {
    if (!req.user) throw { userFacingMessage: 'You need to be logged in.' };
    const { oldPassword, newPassword } = req.body;
    if (!(await bcrypt.compare(oldPassword, req.user.password))) {
      throw { userFacingMessage: 'The password you has entered is incorrect.' };
    }
    const password = await bcrypt.hash(newPassword, 10);
    await req.db
      .collection('users')
      .updateOne({ _id: req.user._id }, { $set: { password } });
    res.json({ message: 'Your password has been updated.' });
  } catch (error) {
    console.error('Error occurred updating password.', error);
    res.json({
      ok: false,
      message: error.userFacingMessage || 'Sorry, something went wrong. Please try again.',
    });
  }
});

export default handler;
