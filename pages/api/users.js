import nextConnect from 'next-connect';
import isEmail from 'validator/lib/isEmail';
import normalizeEmail from 'validator/lib/normalizeEmail';
import bcrypt from 'bcryptjs';
import middleware from '../../middlewares/middleware';

const handler = nextConnect();

handler.use(middleware);

handler.post(async (req, res) => {
  const { firstname, lastname, email, password, phone } = req.body;
  try {
    const normalizedEmail = email; // mrw: allow dupes for now -- normalizeEmail(email);
    if (!isEmail(normalizedEmail)) throw { userFacingMessage: 'The email you entered is invalid.' };
    // if (!password || !lastname || !firstname || !phone) throw { userFacingMessage: 'Missing field(s)' };
    if ((await req.db.collection('users').countDocuments({ email: normalizedEmail })) > 0) throw { userFacingMessage: 'The email has already been used.' };
    const hashedPassword = await bcrypt.hash(password, 10);
    const user = await req.db
      .collection('users')
      .insertOne({ email: normalizedEmail, password: hashedPassword, firstname, lastname, phone })
      .then(({ ops }) => ops[0]);
    req.logIn(user, (err) => {
      if (err) throw err;
      res.status(201).json({
        ok: true,
        message: 'Success!',
      });
    });
  } catch (err) {
    res.json({
      ok: false,
      message: err.userFacingMessage || 'Sorry, something went wrong. Please try again.',
    });
  }
});

export default handler.apply.bind(handler);
