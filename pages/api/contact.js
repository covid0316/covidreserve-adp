import crypto from 'crypto';
import sgMail from '@sendgrid/mail';
import nextConnect from 'next-connect';
import middleware from '../../middlewares/middleware';

sgMail.setApiKey(process.env.SENDGRID_API_KEY);

const handler = nextConnect();

handler.use(middleware);

handler.post(async (req, res) => {
  console.log('as')
  try {

    const msg = {
      to: 'covidreserve@gmail.com',
      subject: 'Covid Reserve: Contact Us',
      from: "info@covidreserve.com",
      html: `
      <div>
        <p>Hey CovidReserve,</p>
        <br />
        <p>You have been contacted by :</p>
          <p>Name: `+req.body.firstName + " " + req.body.lastName+`</p>
          <p>Email: `+req.body.email+`</p>
          <p>Phone: `+req.body.phone+`</p>
          <p>Message: `+req.body.message+`</p>
      </div>
      `,
    };
    await sgMail.send(msg);
    console.log('Sent')
    res.json({message:"" });
  } catch (error) {
    console.error('Error occurred sending contact email', error);
    res.json({
      ok: false,
      message: 'Sorry, something went wrong.',
    });
  }
});

export default handler.apply.bind(handler);
