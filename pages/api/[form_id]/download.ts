import { pipe } from 'fp-ts/lib/pipeable';
import { FpConnect, authenticate } from '../../../lib/tsNextConnect';
import { DocspringClient, FpDocspringClient, DataRequestToken } from '../../../lib/docspringClient';
import * as TE from 'fp-ts/lib/TaskEither';
import * as E from 'fp-ts/lib/Either';
import middleware from '../../../middlewares/middleware';
import { fromPromise } from '../../../lib/util';
import { flattenListForm } from '../../../lib/storage';
import { PdfRequest, Form413D } from '../data/pdfTypes';

const handler = new FpConnect();

const client = new FpDocspringClient(new DocspringClient(
  process.env.DOCSPRING_API_TOKEN as string,
  process.env.DOCSPRING_API_TOKEN_SECRET as string
));

const NotFound = {};

handler.useMiddleware(middleware);
handler.get<void, void>((req, res) => {
  const db = req.db!;
  const user = req.user;
  const { form_id } = req.query;

  if (user && form_id) {
    const { _id: userId } = user;
    return pipe(
      fromPromise(db.collection("pdfs").findOne(
        { _id: userId })),
      TE.chain(record => (record && record['15306']) ? client.getSubmission(record["15306"]) : TE.left(NotFound)),
      TE.map(submission => res.redirect(submission.url)),
      TE.mapLeft(err => {
        if (err === NotFound) {
          res.status(404).end();
        } else {
          console.error("Failed to get signing token for PDF", err);
          res.redirect(`/summary`);
        }
      })
    );
  } else {
    return TE.left(res.status(403).end());
  }
});

export default handler.apply();