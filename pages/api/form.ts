import middleware from '../../middlewares/middleware';
import nextConnect from 'next-connect';
import { Db, ObjectID } from 'mongodb';

const handler = nextConnect();

handler.use(middleware);

handler.post(async (req, res) => {
  if (req.db && req.body && req.user) {
    const client: Db = req.db
    const { form, step, values, formName } = req.body;
    try {
      const user = req.user as { _id: ObjectID };
      if (values) {
        // set
        const updateStatement = formName !== undefined ?
          { [`${formName}.${step || 0}`]: values } :
          { [step || 0]: values };
        const response = await client.collection(`form-${form}`).updateOne(
          { _id: user._id },
          { $set: { _id: user._id, ...updateStatement } },
          { upsert: true });
        res.status(200).json(response.result);
      } else {
        // get
        const data = await client.collection(`form-${form}`).findOne({ _id: user._id });
        const formData = formName === undefined ? data : data && data[formName];
        const response = step === undefined ? formData : formData && formData[step];
        if (response) {
          delete response._id;
          res.status(200).json(response);
        } else {
          res.status(200).json({});
        }
      }
    } catch (e) {
      console.error("Error occurred fetching form from the database", e);
      res.status(500).end();
    }
  } else {
    res.status(403).json({});
  }
});

handler.get(async (req, res) => {
  if (req.db && req.user && req.query.form) {
    const user = req.user as { _id: ObjectID };
    const client: Db = req.db;
    const form = req.query.form;
    const formName = req.query.formName;
    const step = req.query.step;
    try {
      const data = await client.collection(`form-${form}`).findOne({ _id: user._id });
      const formData = formName === undefined ? data : data && data[formName];
      const response = step === undefined ? formData : formData && formData[step];
      if (response) {
        res.status(200).json(response);
      } else {
        res.status(200).json({});
      }
    } catch (e) {
      console.error("Error occurred fetching form from the database", e);
      res.status(500).end();
    }
  } else {
    res.status(403).end();
  }
});

handler.delete(async (req, res) => {
  if (req.db && req.user && req.body.form && req.body.formName) {
    const user = req.user as { _id: ObjectID };
    const client: Db = req.db;
    const form = req.body.form;
    const formName = req.body.formName;
    try {
      const response = await client.collection(`form-${form}`).updateOne(
        { _id: user._id },
        { $unset: { [formName]: "" } });
      res.status(200).json(response.result);
    } catch (e) {
      console.error("Error occurred deleting form from the database", e);
      res.status(500).end();
    }
  } else {
    res.status(403).end();
  }
});

export default handler.apply.bind(handler);
