import nextConnect from 'next-connect';
import middleware from '../../middlewares/middleware';
import passport from '../../lib/passport';

const handler = nextConnect();

handler.use(middleware);

handler.post((req, res) =>
  passport.authenticate('local', (err, user) => {
    if (err) {
      console.error("Error occurred during authentication", err);
      res.status(500).json({
        ok: false,
        message: 'Sorry, something went wrong. Please try again.'
      });
    } else if (!user) {
      res.status(403).json({
        ok: false,
        message: 'Invalid username or password.'
      });
    } else {
    req.logIn(user, (err) => {
        if (err) {
          console.error("Error occurred during authentication", err);
          res.status(500).json({
            ok: false,
            message: 'Sorry, something went wrong. Please try again.'
          });
        } else {
          res.json({
            ok: true
          });
        }
      });
    }
  })(req, res));

export default handler.apply.bind(handler);
