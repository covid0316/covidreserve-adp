import React, {useContext, useState} from 'react';
import {UserContext} from '../components/UserContext';
import Layout from '../components/homeLayout';
import Footer from "./Footer";

type User = {
    email: string,
    password: string
}


const LoginPage = () => {
    const {dispatch} = useContext(UserContext);
    const [showAdditional, setShowAdditional] = useState(true);

    const handleAdditionalFaq = () => {
        setShowAdditional(!showAdditional)
    }

    return (
        <Layout>
            <div className="container-fluid p-0 faq">
                <div className="hero-img-wrapper">
                    <div className="hero-gradient">
                        <div className="hero-tag">Frequently Asked<br/> Questions</div>


                    </div>
                    <img src="/img/FAQ.jpeg" className="img-fluid faq_img" alt="Responsive image"/>

                </div>
                <h1 className="callout p-4 text-center-h1" >Browse common topics below.</h1>

                <div className="container">
                    <div className="row section1">
                        <div className="accordion" id="accordionExample">

                            <div className="card card-custom">
                                <div className="card-header card-header-custom card-flex-header" id="headingOne">
                                    <button className="btn btn-link bt-link-custom arrow_btn_rot" type="button"
                                                    data-toggle="collapse"
                                                    data-target="#collapseOne" aria-expanded="true"
                                                    aria-controls="collapseOne">


                                    <h2 className="mb-0">
                                         Who can apply for this government loan?
                                    </h2>
                                        <img    src="/img/downarrow.png"

                                                className={"arrow-img-up"}
                                                alt="Responsive image"/>
                                              </button>
                                </div>

                                <div id="collapseOne" className="collapse show" aria-labelledby="headingOne"
                                     data-parent="#accordionExample">
                                    <div className="card-body card-body-custom">
                                        If your business has been impacted by Covid-19 coronavirus, and you qualify as a
                                        small business, you can apply. Depending on your industry, SBA defines a small
                                        business based on the number of employees and/or the income of the business. To
                                        determine if your business qualifies as a small business, see here. As a rule of
                                        thumb, most businesses that have fewer than 500 employees count as a small
                                        business. Visit here to find more information about SBA guidances on what
                                        constitutes a small business.
                                    </div>
                                </div>
                            </div>

                            <div className="card card-custom">
                                <div className="card-header card-header-custom card-flex-header"  id="headingTwo">
                                     <button className="btn btn-link collapsed bt-link-custom arrow_btn_rot" type="button"
                                                    data-toggle="collapse"
                                                    data-target="#collapseTwo" aria-expanded="false"
                                                    aria-controls="collapseTwo">
                                        <h2 className="mb-0">

                                                How much does CovidReserve charge for services?

                                        </h2>
                                        <img    src="/img/downarrow.png"

                                                className={"arrow-img-up"}
                                                alt="Responsive image"/>
                                    </button>
                                </div>
                                <div id="collapseTwo" className="collapse" aria-labelledby="headingTwo"
                                     data-parent="#accordionExample">
                                    <div className="card-body card-body-custom">
                                        Pricing for CovidReserve is $999 for services for SBA Disaster Loans, but will
                                        issue a refund if your loan is not approved by the SBA. In comparison, the SBA
                                        believes reasonable pricing for consulting and agent services for application
                                        preparation is up $2500. We are able to offer these lower prices because we have
                                        built software that automatically completes most of the paperwork for these
                                        filings.
                                    </div>
                                </div>
                            </div>

                            <div className="card card-custom">
                                <div className="card-header card-header-custom card-flex-header" id="headingThree">
                                    <button className="btn btn-link collapsed bt-link-custom arrow_btn_rot" type="button"
                                                    data-toggle="collapse"
                                                    data-target="#collapseThree" aria-expanded="false"
                                                    aria-controls="collapseThree">
                                        <h2 className="mb-0">


                                                How long will the form take to fill out?


                                        </h2>
                                        <img    src="/img/downarrow.png"
                                                className={"arrow-img-up"}
                                                alt="Responsive image"/>
                                    </button>
                                </div>
                                <div id="collapseThree" className="collapse" aria-labelledby="headingThree"
                                     data-parent="#accordionExample">
                                    <div className="card-body card-body-custom">
                                        Unsurprisingly these government documents are complex. Even with CovidReserve’s
                                        custom software, it will take 1-3 hours of dedicated paperwork to fill out. You
                                        will need to submit multiple documents ranging from the names of the owners to
                                        your company, to permission for the Small Business Administration to access your
                                        tax returns from the Internal Revenue Service. Once you submit your application
                                        it will need to be processed by the SBA, on a first-come-first-serve basis. In
                                        previous disasters, such as Hurricane Katrina, the SBA was able to respond
                                        within several weeks to most applicants. However, Covid-19 coronavirus is a
                                        national pandemic and the application queue is growing quickly. We recommend
                                        applying immediately.
                                    </div>
                                </div>
                            </div>

                            <div className="card card-custom">
                                <div className="card-header card-header-custom card-flex-header" id="headingFour">
                                     <button className="btn btn-link collapsed bt-link-custom arrow_btn_rot" type="button"
                                                    data-toggle="collapse"
                                                    data-target="#collapseFour" aria-expanded="false"
                                                    aria-controls="collapseFour">
                                        <h2 className="mb-0">

                                                What are the terms of the loan?

                                        </h2>
                                        <img    src="/img/downarrow.png"
                                                className={"arrow-img-up"}
                                                alt="Responsive image"/>
                                    </button>
                                </div>
                                <div id="collapseFour" className="collapse" aria-labelledby="headingFour"
                                     data-parent="#accordionExample">
                                    <div className="card-body card-body-custom">
                                        These loans may be used to pay fixed debts, payroll, accounts payable and other
                                        bills that can’t be paid because of the disaster’s impact. The interest rate is
                                        3.75% for small businesses without credit available elsewhere; businesses with
                                        credit available elsewhere are not eligible. The interest rate for non-profits
                                        is 2.75%. SBA offers loans with long-term repayments in order to keep payments
                                        affordable, up to a maximum of 30 years. Terms are determined on a case-by-case
                                        basis, based upon each borrower’s ability to repay.
                                    </div>
                                </div>
                            </div>

                            <div className="card card-custom">
                                <div className="card-header card-header-custom bt-link-custom card-flex-header" id="headingFive">
                                    <button className="btn btn-link collapsed bt-link-custom arrow_btn_rot" type="button"
                                                    data-toggle="collapse"
                                                    data-target="#collapseFive" aria-expanded="false"
                                                    aria-controls="collapseFive">
                                        <h2 className="mb-0">


                                                How much can I borrow?


                                        </h2>
                                        <img    src="/img/downarrow.png"
                                                className={"arrow-img-up"}
                                                alt="Responsive image"/>
                                    </button>
                                </div>
                                <div id="collapseFive" className="collapse" aria-labelledby="headingFive"
                                     data-parent="#accordionExample">
                                    <div className="card-body card-body-custom">
                                        You may request an EIDL for the amount of economic injury and operating needs,
                                        but not in excess of what your business could have paid had the disaster not
                                        occurred. In determining your eligible amount, the SBA will look at: (a) the
                                        total of your debt obligations; (b) operating expenses that mature during the
                                        period affected by the disaster, plus the amount you need to maintain a
                                        reasonable working capital position during that period; and (c) expenses you
                                        could have met and a working capital position you could have maintained had the
                                        disaster not occurred. The amount of your economic injury does not automatically
                                        represent the dollar amount of your loan eligibility; the SBA will evaluate the
                                        information you provide and determine the reasonableness of your loan request.
                                        These loans could not exceed $2 million to repair or replace damaged property or
                                        economic injury. SBA can also lend additional funds up to 20 percent of the
                                        verified losses to help make improvement to the property (both real and
                                        contents) that protect, prevent or minimize the same type of disaster damage
                                        from occurring in the future.

                                    </div>
                                </div>
                            </div>

                            <div className="card card-custom">
                                <div className="card-header card-header-custom bt-link-custom card-flex-header" id="headingSix">
                                    <button className="btn btn-link collapsed bt-link-custom arrow_btn_rot" type="button"
                                                    data-toggle="collapse"
                                                    data-target="#collapseSix" aria-expanded="false"
                                                    aria-controls="collapseSix">
                                        <h2 className="mb-0">

                                                Do I have to accept the loan if I apply?

                                        </h2>
                                        <img    src="/img/downarrow.png"
                                                className={"arrow-img-up"}
                                                alt="Responsive image"/>
                                    </button>
                                </div>
                                <div id="collapseSix" className="collapse" aria-labelledby="headingSix"
                                     data-parent="#accordionExample">
                                    <div className="card-body card-body-custom">
                                        No, the risk for application is very low for a business owner. If you apply and
                                        are offered a loan, you have no obligation to accept it. The SBA encourages
                                        impacted business owners to apply early, as a preparatory measure.

                                    </div>
                                </div>
                            </div>

                            <div className="card card-custom">
                                <div className="card-header card-header-custom bt-link-custom card-flex-header" id="heading7">
                                     <button className="btn btn-link collapsed bt-link-custom arrow_btn_rot" type="button"
                                                    data-toggle="collapse"
                                                    data-target="#collapse7" aria-expanded="false"
                                                    aria-controls="collapse7">
                                        <h2 className="mb-0">

                                                Can I do this alone without CovidReserve?

                                        </h2>
                                        <img    src="/img/downarrow.png"
                                                className={"arrow-img-up"}
                                                alt="Responsive image"/>
                                     </button>
                                </div>
                                <div id="collapse7" className="collapse" aria-labelledby="heading7"
                                     data-parent="#accordionExample">
                                    <div className="card-body card-body-custom">
                                        Yes, all the information to self-service is available online, without
                                        CovidReserve’s assistance; but we encourage our customers to consider working
                                        with an expert because the paperwork and research is complex. If you want to do
                                        the paperwork yourself, we recommend a starting point of reading 13 CFR § 120.2
                                        - Descriptions of the business loan programs here. Additionally there is
                                        information available on the SBA website. While CovidReserve’s goal is to help
                                        you navigate though this complex process, it is definitely possible to
                                        self-serve.

                                    </div>
                                </div>
                            </div>


                            { /* <button type="button" className="btn btn-primary btn-block btn-lg additional p-4"
                                    onClick={handleAdditionalFaq}>  {"Additional FAQs"}
                            </button> */}


                            {showAdditional ? (

                                <>
                                    <div className="card card-custom">
                                        <div className="card-header card-header-custom bt-link-custom card-flex-header" id="heading8">
                                            <button className="btn btn-link collapsed bt-link-custom arrow_btn_rot" type="button"
                                                            data-toggle="collapse"
                                                            data-target="#collapse8" aria-expanded="false"
                                                            aria-controls="collapse8">
                                                <h2 className="mb-0">


                                                        When do you collect payment?


                                                </h2>
                                                <img    src="/img/downarrow.png"
                                                        className={"arrow-img-up"}
                                                        alt="Responsive image"/>
                                            </button>
                                        </div>
                                        <div id="collapse8" className="collapse" aria-labelledby="heading8"
                                             data-parent="#accordionExample">
                                            <div className="card-body card-body-custom">
                                                After your documents are finalized and ready for submission, you will be
                                                able to
                                                enter your credit card information and we will submit documents on your
                                                behalf.
                                                We will not collect payments until you are ready to submit, so there is
                                                no risk
                                                of getting started immediately.

                                            </div>
                                        </div>
                                    </div>

                                    <div className="card card-custom">
                                        <div className="card-header card-header-custom bt-link-custom card-flex-header" id="heading9">
                                         <button className="btn btn-link collapsed bt-link-custom arrow_btn_rot" type="button"
                                                        data-toggle="collapse"
                                                        data-target="#collapse9" aria-expanded="false"
                                                        aria-controls="collapse9">
                                                <h2 className="mb-0">


                                                        Why do customers have to pay up-front for services from
                                                        CovidReserve,
                                                        rather than pay when the loan is approved?


                                                </h2>
                                            <img    src="/img/downarrow.png"
                                                    className={"arrow-img-up"}
                                                    alt="Responsive image"/>
                                         </button>
                                        </div>
                                        <div id="collapse9" className="collapse" aria-labelledby="heading9"
                                             data-parent="#accordionExample">
                                            <div className="card-body card-body-custom">
                                                By law, CovidReserve is prohibited from collecting fees that are
                                                contingent on
                                                SBA loan approval.
                                            </div>
                                        </div>
                                    </div>

                                    <div className="card card-custom">
                                        <div className="card-header card-header-custom bt-link-custom card-flex-header" id="heading10">
                                          <button className="btn btn-link collapsed bt-link-custom arrow_btn_rot" type="button"
                                                        data-toggle="collapse"
                                                        data-target="#collapse10" aria-expanded="false"
                                                        aria-controls="collapse10">
                                                <h2 className="mb-0">


                                                        Will I need to personally guarantee the loan?

                                                </h2>
                                                <img    src="/img/downarrow.png"
                                                        className={"arrow-img-up"}
                                                        alt="Responsive image"/>
                                            </button>
                                        </div>
                                        <div id="collapse10" className="collapse" aria-labelledby="heading10"
                                             data-parent="#accordionExample">
                                            <div className="card-body card-body-custom">
                                                Yes. The SBA requires the principals of the business to personally
                                                guarantee
                                                repayment of the loan and, in some instances, to secure the loan by
                                                pledging
                                                additional collateral.
                                            </div>
                                        </div>
                                    </div>

                                    <div className="card card-custom">
                                        <div className="card-header card-header-custom bt-link-custom card-flex-header" id="heading11">
                                             <button className="btn btn-link collapsed bt-link-custom arrow_btn_rot" type="button"
                                                            data-toggle="collapse"
                                                            data-target="#collapse11" aria-expanded="false"
                                                            aria-controls="collapse11">
                                                <h2 className="mb-0">


                                                        Is collateral required for an EIDL?

                                                </h2>
                                                <img    src="/img/downarrow.png"
                                                        className={"arrow-img-up"}
                                                        alt="Responsive image"/>
                                             </button>
                                        </div>
                                        <div id="collapse11" className="collapse" aria-labelledby="heading11"
                                             data-parent="#accordionExample">
                                            <div className="card-body card-body-custom">
                                                Yes. The SBA requires the principals of the business to personally
                                                guarantee
                                                repayment of the loan and, in some instances, to secure the loan by
                                                pledging
                                                additional collateral.
                                            </div>
                                        </div>
                                    </div>

                                    <div className="card card-custom">
                                        <div className="card-header card-header-custom bt-link-custom card-flex-header" id="heading12">
                                             <button className="btn btn-link collapsed bt-link-custom arrow_btn_rot" type="button"
                                                            data-toggle="collapse"
                                                            data-target="#collapse12" aria-expanded="false"
                                                            aria-controls="collapse12">
                                                <h2 className="mb-0">


                                                        Can I use the disaster loan to expand my business?

                                                </h2>
                                                <img    src="/img/downarrow.png"
                                                        className={"arrow-img-up"}
                                                        alt="Responsive image"/>
                                             </button>
                                        </div>
                                        <div id="collapse12" className="collapse" aria-labelledby="heading12"
                                             data-parent="#accordionExample">
                                            <div className="card-body card-body-custom">
                                                The disaster loan helps restore the business to pre-disaster condition,
                                                and,
                                                under certain circumstances, protects the business from future
                                                disasters. It
                                                cannot upgrade or expand a business unless required by local laws (such
                                                as a
                                                physical building improvement that is necessary due to the disaster).
                                            </div>
                                        </div>
                                    </div>

                                    <div className="card card-custom">
                                        <div className="card-header card-header-custom bt-link-custom card-flex-header card-flex-header" id="heading13">
                                            <button className="btn btn-link collapsed bt-link-custom arrow_btn_rot" type="button"
                                                            data-toggle="collapse"
                                                            data-target="#collapse13" aria-expanded="false"
                                                            aria-controls="collapse13">
                                                <h2 className="mb-0">


                                                        If I can borrow from a bank, am I still eligible for SBA
                                                        assistance?

                                                </h2>
                                                <img    src="/img/downarrow.png"
                                                        className={"arrow-img-up"}
                                                        alt="Responsive image"/>
                                            </button>
                                        </div>
                                        <div id="collapse13" className="collapse" aria-labelledby="heading13"
                                             data-parent="#accordionExample">
                                            <div className="card-body card-body-custom">
                                                Private credit sources must be used as much as possible to overcome the
                                                economic
                                                injury. The SBA can provide EIDL assistance only to the extent the
                                                business (and
                                                its principals) cannot recover by using its own resources and normal
                                                lending
                                                channels.
                                            </div>
                                        </div>
                                    </div>

                                    <div className="card card-custom">
                                        <div className="card-header card-header-custom bt-link-custom card-flex-header" id="heading14">
                                             <button className="btn btn-link collapsed bt-link-custom arrow_btn_rot" type="button"
                                                            data-toggle="collapse"
                                                            data-target="#collapse14" aria-expanded="false"
                                                            aria-controls="collapse14">
                                            <h2 className="mb-0">


                                                    What are some prohibited uses of an EIDL?

                                            </h2>
                                            <img    src="/img/downarrow.png"
                                                    className={"arrow-img-up"}
                                                    alt="Responsive image"/>
                                        </button>
                                        </div>
                                        <div id="collapse14" className="collapse" aria-labelledby="heading14"
                                             data-parent="#accordionExample">
                                            <div className="card-body card-body-custom">
                                                You may not use funds to pay cash dividends or bonuses, or for
                                                disbursements to
                                                owners, partners, officers or stockholders not directly related to the
                                                performance of services for the business. The SBA will not refinance
                                                long-term
                                                debts or provide working capital that was needed by the business prior
                                                to the
                                                disaster.
                                            </div>
                                        </div>
                                    </div>

                                    <div className="card card-custom">
                                        <div className="card-header card-header-custom bt-link-custom card-flex-header" id="heading15">
                                             <button className="btn btn-link collapsed bt-link-custom arrow_btn_rot" type="button"
                                                            data-toggle="collapse"
                                                            data-target="#collapse15" aria-expanded="false"
                                                            aria-controls="collapse15">
                                                <h2 className="mb-0">

                                                        What are some prohibited uses of an EIDL?

                                                </h2>
                                                <img    src="/img/downarrow.png"
                                                        className={"arrow-img-up"}
                                                        alt="Responsive image"/>
                                              </button>
                                        </div>
                                        <div id="collapse15" className="collapse" aria-labelledby="heading15"
                                             data-parent="#accordionExample">
                                            <div className="card-body card-body-custom">
                                                You may not use funds to pay cash dividends or bonuses, or for
                                                disbursements to
                                                owners, partners, officers or stockholders not directly related to the
                                                performance of services for the business. The SBA will not refinance
                                                long-term
                                                debts or provide working capital that was needed by the business prior
                                                to the
                                                disaster.
                                            </div>
                                        </div>
                                    </div>

                                    <div className="card card-custom">
                                        <div className="card-header card-header-custom bt-link-custom card-flex-header" id="heading16">
                                            <button className="btn btn-link collapsed bt-link-custom arrow_btn_rot" type="button"
                                                            data-toggle="collapse"
                                                            data-target="#collapse16" aria-expanded="false"
                                                            aria-controls="collapse16">
                                                <h2 className="mb-0">

                                                        I already have a mortgage on my business. Can the SBA refinance
                                                        my
                                                        mortgage?

                                                </h2>
                                                <img    src="/img/downarrow.png"
                                                        className={"arrow-img-up"}
                                                        alt="Responsive image"/>
                                            </button>
                                        </div>
                                        <div id="collapse16" className="collapse" aria-labelledby="heading16"
                                             data-parent="#accordionExample">
                                            <div className="card-body card-body-custom">
                                                The SBA can refinance all or part of a previous mortgage in some cases
                                                when the
                                                applicant does not have credit available elsewhere, has suffered
                                                uninsured
                                                damage (40 percent or more of the value), and intends to repair the
                                                damage. SBA
                                                disaster loan officers can provide additional details.
                                            </div>
                                        </div>
                                    </div>

                                    <div className="card card-custom">
                                        <div className="card-header card-header-custom bt-link-custom card-flex-header" id="heading17">
                                            <button className="btn btn-link collapsed bt-link-custom arrow_btn_rot" type="button"
                                                            data-toggle="collapse"
                                                            data-target="#collapse17" aria-expanded="false"
                                                            aria-controls="collapse17">
                                                <h2 className="mb-0">


                                                        How long will it take me to hear back from CovidReserve?


                                                </h2>
                                                <img    src="/img/downarrow.png"
                                                        className={"arrow-img-up"}
                                                        alt="Responsive image"/>
                                            </button>
                                        </div>
                                        <div id="collapse17" className="collapse" aria-labelledby="heading17"
                                             data-parent="#accordionExample">
                                            <div className="card-body card-body-custom">
                                                Once you finish your application to CovidReserve, our software will
                                                automatically generate an application to SBA. You will hear back from us
                                                within
                                                1 hour.
                                            </div>
                                        </div>
                                    </div>

                                    <div className="card card-custom">
                                        <div className="card-header card-header-custom bt-link-custom card-flex-header" id="heading18">
                                         <button className="btn btn-link collapsed bt-link-custom arrow_btn_rot" type="button"
                                                        data-toggle="collapse"
                                                        data-target="#collapse18" aria-expanded="false"
                                                        aria-controls="collapse18">
                                                    <h2 className="mb-0">

                                                            How soon before I know I’ve been approved for a loan?

                                                    </h2>
                                                    <img    src="/img/downarrow.png"
                                                            className={"arrow-img-up"}
                                                            alt="Responsive image"/>
                                        </button>
                                        </div>
                                        <div id="collapse18" className="collapse" aria-labelledby="heading18"
                                             data-parent="#accordionExample">
                                            <div className="card-body card-body-custom">
                                                The sooner you return the completed loan application, the sooner the SBA
                                                can
                                                process it. The SBA tries to make a decision within two to three weeks,
                                                but with
                                                the unprecedented number of applications related to Covid-19 coronavirus
                                                this
                                                will likely take longer. Make sure the application is complete. Missing
                                                information is a major cause of delays.
                                            </div>
                                        </div>
                                    </div>

                                    <div className="card card-custom">
                                        <div className="card-header card-header-custom bt-link-custom card-flex-header" id="heading19">
                                        <button className="btn btn-link collapsed bt-link-custom arrow_btn_rot" type="button"
                                                        data-toggle="collapse"
                                                        data-target="#collapse19" aria-expanded="false"
                                                        aria-controls="collapse19">
                                                <h2 className="mb-0">

                                                        If I receive an EIDL, may I spend the loan money any way I
                                                        want?

                                                </h2>
                                                <img    src="/img/downarrow.png"
                                                        className={"arrow-img-up"}
                                                        alt="Responsive image"/>
                                        </button>
                                        </div>
                                        <div id="collapse19" className="collapse" aria-labelledby="heading19"
                                             data-parent="#accordionExample">
                                            <div className="card-body card-body-custom">
                                                No. An EIDL is intended to help you maintain a secure financial
                                                condition until
                                                your business is back to normal, not for purposes like expanding
                                                business
                                                facilities or purchasing a new line of inventory. Your loan will be made
                                                for
                                                specific and designated purposes. Remember that the penalty for misusing
                                                disaster funds is immediate repayment of one and a half times the
                                                original
                                                amount of the loan. The SBA requires that you keep receipts and good
                                                records of
                                                all loan expenditures for three years following receipt of your SBA
                                                loan.
                                            </div>
                                        </div>
                                    </div>

                                    <div className="card card-custom">
                                        <div className="card-header card-header-custom bt-link-custom card-flex-header" id="heading20">
                                            <button className="btn btn-link collapsed bt-link-custom arrow_btn_rot" type="button"
                                                            data-toggle="collapse"
                                                            data-target="#collapse20" aria-expanded="false"
                                                            aria-controls="collapse20">
                                                <h2 className="mb-0">

                                                        How long will I have to pay off the SBA loan?

                                                </h2>
                                                <img    src="/img/downarrow.png"
                                                        className={"arrow-img-up"}
                                                        alt="Responsive image"/>
                                             </button>
                                        </div>
                                        <div id="collapse20" className="collapse" aria-labelledby="heading20"
                                             data-parent="#accordionExample">
                                            <div className="card-body card-body-custom">
                                                The SBA will assess your financial situation and will set loan terms
                                                based on
                                                your needs and repayment ability. The maximum maturity for disaster
                                                loans is 30
                                                years.
                                            </div>
                                        </div>
                                    </div>

                                    <div className="card card-custom">
                                        <div className="card-header card-header-custom bt-link-custom card-flex-header" id="heading21">
                                            <button className="btn btn-link collapsed bt-link-custom arrow_btn_rot" type="button"
                                                            data-toggle="collapse"
                                                            data-target="#collapse21" aria-expanded="false"
                                                            aria-controls="collapse21">
                                                <h2 className="mb-0">

                                                        How soon will I know if I get the loan?

                                                </h2>
                                                <img    src="/img/downarrow.png"
                                                        className={"arrow-img-up"}
                                                        alt="Responsive image"/>
                                            </button>
                                        </div>
                                        <div id="collapse21" className="collapse" aria-labelledby="heading21"
                                             data-parent="#accordionExample">
                                            <div className="card-body card-body-custom">
                                                That depends on how soon you file a complete SBA loan application. SBA
                                                needs to
                                                be able to calculate the amount of economic injury and the working
                                                capital and
                                                other needs of your business. They have to be satisfied that you can
                                                repay the
                                                loan out of business operations, and they must take reasonable
                                                safeguards to
                                                help ensure the loan is repaid. The SBA loan application asks for the
                                                information we need. Since the SBA processes applications in the order
                                                received,
                                                the faster you can return it to us with all the needed information, the
                                                faster
                                                they can work on it. They try to make a decision on each application
                                                within 21
                                                days. Be sure the information in your application is complete; missing
                                                information is the biggest cause of delay.
                                            </div>
                                        </div>
                                    </div>

                                    <div className="card card-custom">
                                        <div className="card-header card-header-custom bt-link-custom card-flex-header" id="heading22">
                                             <button className="btn btn-link collapsed bt-link-custom arrow_btn_rot" type="button"
                                                            data-toggle="collapse"
                                                            data-target="#collapse22" aria-expanded="false"
                                                            aria-controls="collapse22">

                                                <h2 className="mb-0">

                                                    How soon can I expect the money?
                                                </h2>
                                                <img    src="/img/downarrow.png"
                                                        className={"arrow-img-up"}
                                                        alt="Responsive image"/>
                                            </button>
                                        </div>
                                        <div id="collapse22" className="collapse" aria-labelledby="heading22"
                                             data-parent="#accordionExample">
                                            <div className="card-body card-body-custom">
                                                Loans over $5,000 have to be secured. After SBA approves a request, they
                                                will
                                                tell you what documents are needed to close the loan. When they receive
                                                these
                                                documents, they can order checks. You will receive the money in
                                                installments as
                                                it is needed.
                                            </div>
                                        </div>
                                    </div>

                                    <div className="card card-custom">
                                        <div className="card-header card-header-custom bt-link-custom card-flex-header" id="heading23">
                                            <button className="btn btn-link collapsed bt-link-custom arrow_btn_rot" type="button"
                                                            data-toggle="collapse"
                                                            data-target="#collapse23" aria-expanded="false"
                                                            aria-controls="collapse23">
                                                    <h2 className="mb-0">

                                                            How may I use an Economic Injury Disaster Loan?

                                                    </h2>
                                                    <img    src="/img/downarrow.png"
                                                        className={"arrow-img-up"}
                                                        alt="Responsive image"/>
                                            </button>
                                        </div>
                                        <div id="collapse23" className="collapse" aria-labelledby="heading23"
                                             data-parent="#accordionExample">
                                            <div className="card-body card-body-custom">
                                                The loan provides working capital for disaster-related needs until your
                                                business
                                                or private, non-profit organization recovers. You may request an EIDL
                                                for the
                                                amount of economic injury but not in excess of what your business or
                                                private,
                                                non-profit organization could have paid if the disaster had not
                                                occurred. EIDL
                                                loans do not replace sales or lost profits.
                                            </div>
                                        </div>
                                    </div>

                                    <div className="card card-custom">
                                        <div className="card-header card-header-custom bt-link-custom card-flex-header" id="heading24">
                                            <button className="btn btn-link collapsed bt-link-custom arrow_btn_rot" type="button"
                                                            data-toggle="collapse"
                                                            data-target="#collapse24" aria-expanded="false"
                                                            aria-controls="collapse24">
                                                <h2 className="mb-0">

                                                        What documents do I need to submit to CovidReserve in order to
                                                        apply?

                                                </h2>
                                                <img    src="/img/downarrow.png"
                                                        className={"arrow-img-up"}
                                                        alt="Responsive image"/>
                                             </button>
                                        </div>
                                        <div id="collapse24" className="collapse" aria-labelledby="heading24"
                                             data-parent="#accordionExample">
                                            <div className="card-body card-body-custom">
                                                We have an easy-to-use website where you will need to provide critical
                                                details
                                                about your business. We will use this information to populate a minimum
                                                of 5 SBA
                                                forms and 1 IRS form (potentially more forms depending on ownership
                                                structure of
                                                your business, and other needs).
                                            </div>
                                        </div>
                                    </div>

                                    <div className="card card-custom">
                                        <div className="card-header card-header-custom bt-link-custom card-flex-header" id="heading25">
                                        <button className="btn btn-link collapsed bt-link-custom arrow_btn_rot" type="button"
                                                        data-toggle="collapse"
                                                        data-target="#collapse25" aria-expanded="false"
                                                        aria-controls="collapse25">
                                            <h2 className="mb-0">

                                                    Must I sell assets that are not used in my regular business
                                                    operations
                                                    before I am eligible for an EIDL?

                                            </h2>
                                            <img    src="/img/downarrow.png"
                                                    className={"arrow-img-up"}
                                                    alt="Responsive image"/>
                                       </button>
                                        </div>
                                        <div id="collapse25" className="collapse" aria-labelledby="heading25"
                                             data-parent="#accordionExample">
                                            <div className="card-body card-body-custom">
                                                The SBA will review the availability of such assets to determine if part
                                                or all
                                                of your economic injury might be remedied by using such assets. The
                                                business and
                                                its principal owners must use their own resources to overcome the
                                                economic
                                                injury to the greatest extent possible without causing undue hardship.
                                            </div>
                                        </div>
                                    </div>

                                    <div className="card card-custom">
                                        <div className="card-header card-header-custom bt-link-custom card-flex-header" id="heading26">
                                            <button className="btn btn-link collapsed bt-link-custom arrow_btn_rot" type="button"
                                                            data-toggle="collapse"
                                                            data-target="#collapse26" aria-expanded="false"
                                                            aria-controls="collapse26">
                                                <h2 className="mb-0">

                                                        If I show the SBA that I am not making a profit, is that enough
                                                        to
                                                        qualify me for an EIDL?
                                                        before I am eligible for an EIDL?

                                                </h2>
                                                <img    src="/img/downarrow.png"
                                                        className={"arrow-img-up"}
                                                        alt="Responsive image"/>
                                             </button>
                                        </div>
                                        <div id="collapse26" className="collapse" aria-labelledby="heading26"
                                             data-parent="#accordionExample">
                                            <div className="card-body card-body-custom">
                                                No. Neither lack of profit or loss of anticipated sales alone is
                                                sufficient to
                                                establish substantial economic injury. Substantial economic injury
                                                occurs only
                                                when you cannot meet current obligations because of the disaster.
                                                Indicators of
                                                economic injury might be a larger than normal volume of receivables, a
                                                lower
                                                sales volume, slow inventory turnover, and the development of
                                                delinquencies in
                                                trade payables, current accruals and debt payments.
                                            </div>
                                        </div>
                                    </div>

                                    <div className="card card-custom">
                                        <div className="card-header card-header-custom bt-link-custom card-flex-header" id="heading27">
                                        <button className="btn btn-link collapsed bt-link-custom arrow_btn_rot" type="button"
                                                        data-toggle="collapse"
                                                        data-target="#collapse27" aria-expanded="false"
                                                        aria-controls="collapse27">
                                            <h2 className="mb-0">

                                                    How does this impact my taxes?
                                                    before I am eligible for an EIDL?

                                            </h2>
                                            <img    src="/img/downarrow.png"
                                                    className={"arrow-img-up"}
                                                    alt="Responsive image"/>
                                        </button>
                                        </div>
                                        <div id="collapse27" className="collapse" aria-labelledby="heading27"
                                             data-parent="#accordionExample">
                                            <div className="card-body card-body-custom">
                                                It depends on the specifics of your situation so you will need to
                                                consult your
                                                accountant, but in some cases, owners are able to claim losses in
                                                federal tax
                                                returns based on the year a disaster occurs. Any interest or fees paid
                                                during
                                                the year often present the opportunity to deduct the taxes or provide
                                                benefits
                                                to the owner. Then, he or she may owe less for other income and revenue
                                                based on
                                                the monetary tax bracket the person or company has entered.
                                            </div>
                                        </div>
                                    </div>

                                    <div className="card card-custom">
                                        <div className="card-header card-header-custom bt-link-custom card-flex-header" id="heading28">
                                        <button className="btn btn-link collapsed bt-link-custom arrow_btn_rot" type="button"
                                                        data-toggle="collapse"
                                                        data-target="#collapse28" aria-expanded="false"
                                                        aria-controls="collapse28">
                                            <h2 className="mb-0">

                                                    What if I don’t have the necessary information to fill out the
                                                    form on
                                                    your website?

                                            </h2>
                                            <img    src="/img/downarrow.png"
                                                    className={"arrow-img-up"}
                                                    alt="Responsive image"/>
                                             </button>
                                        </div>
                                        <div id="collapse28" className="collapse" aria-labelledby="heading28"
                                             data-parent="#accordionExample">
                                            <div className="card-body card-body-custom">
                                                The information required on the website is similar to what you’d
                                                typically need
                                                to fill out your taxes. If you don’t have this information, then
                                                CovidReserve
                                                cannot help you apply for the SBA loan. We would recommend contacting
                                                your
                                                accountant or bookkeeper to work with CovidReserve to fill out on your
                                                behalf.
                                            </div>
                                        </div>
                                    </div>

                                    <div className="card card-custom">
                                        <div className="card-header card-header-custom bt-link-custom card-flex-header" id="heading29">
                                        <button className="btn btn-link collapsed bt-link-custom arrow_btn_rot" type="button"
                                                        data-toggle="collapse"
                                                        data-target="#collapse29" aria-expanded="false"
                                                        aria-controls="collapse29">
                                            <h2 className="mb-0">

                                                    Is an SBA Economic Injury Disaster Loan the right thing for me?

                                            </h2>
                                            <img    src="/img/downarrow.png"
                                                    className={"arrow-img-up"}
                                                    alt="Responsive image"/>
                                        </button>
                                        </div>
                                        <div id="collapse29" className="collapse" aria-labelledby="heading29"
                                             data-parent="#accordionExample">
                                            <div className="card-body card-body-custom">
                                                You should consult your financial team or accountant. We do not make
                                                recommendations about what financial products are right for your
                                                specific case.
                                                However, we do make it easier to apply to SBA Economic Injury Disaster
                                                Loan if
                                                it is right for you.
                                            </div>
                                        </div>
                                    </div>

                                    <div className="card card-custom">

                                        <div className="card-header card-header-custom bt-link-custom card-flex-header" id="heading30">
                                        <button className="btn btn-link collapsed bt-link-custom arrow_btn_rot" type="button"
                                                        data-toggle="collapse"
                                                        data-target="#collapse30" aria-expanded="false"
                                                        aria-controls="collapse30">
                                            <h2 className="mb-0">

                                                    Are religious nonprofit organizations eligible for an EIDL?

                                            </h2>
                                            <img    src="/img/downarrow.png"
                                                    className={"arrow-img-up"}
                                                    alt="Responsive image"/>
                                         </button>
                                        </div>
                                        <div id="collapse30" className="collapse" aria-labelledby="heading30"
                                             data-parent="#accordionExample">
                                            <div className="card-body card-body-custom">
                                                No. Only profit-oriented operating small businesses, small agricultural
                                                cooperatives, and some private nonprofit organizations may apply.
                                            </div>
                                        </div>
                                    </div>

                                    <div className="card card-custom">

                                        <div className="card-header card-header-custom bt-link-custom card-flex-header" id="heading31">
                                            <button className="btn btn-link collapsed bt-link-custom arrow_btn_rot" type="button"
                                                            data-toggle="collapse"
                                                            data-target="#collapse31" aria-expanded="false"
                                                            aria-controls="collapse31">
                                                <h2 className="mb-0">

                                                        When I apply, does this loan become public record

                                                </h2>
                                                <img    src="/img/downarrow.png"
                                                        className={"arrow-img-up"}
                                                        alt="Responsive image"/>
                                             </button>
                                        </div>
                                        <div id="collapse31" className="collapse" aria-labelledby="heading31"
                                             data-parent="#accordionExample">
                                            <div className="card-body card-body-custom">
                                                Please refer to additional guidance published by SBA.gov here.
                                            </div>
                                        </div>
                                    </div>

                                    <div className="card card-custom">

                                        <div className="card-header card-header-custom bt-link-custom card-flex-header" id="heading32">
                                            <button className="btn btn-link collapsed bt-link-custom arrow_btn_rot" type="button"
                                                            data-toggle="collapse"
                                                            data-target="#collapse32" aria-expanded="false"
                                                            aria-controls="collapse32">
                                                <h2 className="mb-0">

                                                        Will I need to personally guarantee the loan?

                                                </h2>
                                                <img    src="/img/downarrow.png"
                                                        className={"arrow-img-up"}
                                                        alt="Responsive image"/>
                                          </button>
                                        </div>
                                        <div id="collapse32" className="collapse" aria-labelledby="heading32"
                                             data-parent="#accordionExample">
                                            <div className="card-body card-body-custom">
                                                Yes. The SBA requires the principals of the business to personally
                                                guarantee
                                                repayment of the loan and, in some instances, to secure the loan by
                                                pledging
                                                additional collateral.
                                            </div>
                                        </div>
                                    </div>

                                    <div className="card card-custom">

                                        <div className="card-header card-header-custom bt-link-custom card-flex-header" id="heading33">
                                             <button className="btn btn-link collapsed bt-link-custom arrow_btn_rot" type="button"
                                                            data-toggle="collapse"
                                                            data-target="#collapse33" aria-expanded="false"
                                                            aria-controls="collapse33">
                                                <h2 className="mb-0">

                                                        Is collateral required for an EIDL?

                                                </h2>
                                                <img    src="/img/downarrow.png"
                                                        className={"arrow-img-up"}
                                                        alt="Responsive image"/>
                                            </button>
                                        </div>
                                        <div id="collapse33" className="collapse" aria-labelledby="heading33"
                                             data-parent="#accordionExample">
                                            <div className="card-body card-body-custom">
                                                Generally a business must pledge collateral to the extent available.
                                                Occasionally, the SBA will make very small EIDLs on an unsecured basis.
                                                The SBA
                                                will not decline an EIDL request solely because available collateral
                                                will not
                                                adequately secure the loan, and a business will not be required to
                                                pledge more
                                                collateral than is necessary. The SBA may decline a loan if a business
                                                has
                                                collateral available but refuses to pledge it.
                                            </div>
                                        </div>


                                    </div>
                                </>
                            ) : ""}
                        </div>
                    </div>
                </div>


                <Footer/>


            </div>
            <style jsx>
                {`
                @media only screen and (max-width: 960px) {
                    .hero-tag {
                        font-size: 24px !important;
                        font-weight: 700 !important;
                    }
                }






                .arrow-img{
                      max-width: 3%;
                      height: auto;
                    }
                    .arrow-img-up {
                        max-width: 3%;
                        height: auto;
                        transform: rotate(180deg);
                        transition: 0.3s;
                        position: absolute;
                        right: 19px;
                    }
                    .card-flex-header{
                      display: flex;
                      flex-direction: row;
                      justify-content: space-between;
                      align-items: center;
                      padding-right: 5%;
                    }
                    /*Navbar css*/
@media (min-width: 992px) {
.navbar-expand-lg {
    flex-flow: row nowrap;
    justify-content: flex-start;
}
.navbar-expand-lg .navbar-toggler {
    display: none;
}
.navbar-expand-lg .navbar-collapse {
    display: flex !important;
    flex-basis: auto;
}
.navbar-expand-lg .navbar-nav {
    flex-direction: row;
}
.navbar-expand-lg .navbar-nav .nav-link {
    padding-right: 0.5rem;
    padding-left: 0.5rem;
}
}

.navbar {
    position: relative;
    display: flex;
    flex-wrap: wrap;
    align-items: center;
    justify-content: space-between;
    padding: 0.5rem 1rem;
}
.navbar-nav {
    display: flex;
    flex-direction: column;
    padding-left: 0;
    margin-bottom: 0;
    list-style: none;
}
.ml-auto, .mx-auto {
    margin-left: auto !important;
}
.navbar-light .navbar-toggler {
    color: rgba(0, 0, 0, 0.5);
    border-color: rgba(0, 0, 0, 0.1);
}
.navbar-toggler {
    padding: 0.25rem 0.75rem;
    font-size: 1.25rem;
    line-height: 1;
    background-color: transparent;
    border: 1px solid transparent;
    border-radius: 0.25rem;
}
.navbar-light .navbar-toggler-icon {
    background-image: url("data:image/svg+xml;utf8, <svg xmlns='http://www.w3.org/2000/svg' width='30' hei…miterlimit='10' stroke-width='2' d='M4 7h22M4 15h22M4 23h22'/></svg>");
}
.navbar-toggler-icon {
    display: inline-block;
    width: 1.5em;
    height: 1.5em;
    vertical-align: middle;
    content: "";
    background: no-repeat center center;
    background-size: 100% 100%;
}
.navbar-collapse {
    flex-basis: 100%;
    flex-grow: 1;
    align-items: center;
}
.navbar-light .navbar-nav .show > .nav-link, .navbar-light .navbar-nav .active > .nav-link, .navbar-light .navbar-nav .nav-link.show, .navbar-light .navbar-nav .nav-link.active {
    color: rgba(0, 0, 0, 0.9);
}
.navbar-light .navbar-nav .nav-link {
    color: rgba(0, 0, 0, 0.5);
}
.nav-link {
    display: block;
    padding: 0.5rem 1rem;
}
.collapse:not(.show) {
    display: none;
}
.arrow-img{
  max-width: 3%;
  height: auto;
}
.arrow-img-up {
    max-width: 3%;
    height: auto;
    transform: rotate(180deg);
    transition: 0.3s;
    position: absolute;
    right: 19px;
}
.card-flex-header{
  display: flex;
  flex-direction: row;
  justify-content: space-between;
  align-items: center;
  padding-right: 5%;
}
                .faq_img {
                    vertical-align: middle;
                    border-style: none;
                    height: 601px;
                    width: 100%;
                    object-fit: cover;
                }
                .accordion{
                    width:100%;
                    margin-bottom: 48px;
                }
                .callouts{
                    width:100%;
                    text-align:center

                }
                .additional{
                    height:110px;
                    width:100%;
                    font-size: 40px;
                    font-weight: 600;
                    margin-top: 50px;
                    margin-bottom: 80px;
                }

                .bt-link-custom{
                    font-weight: 600;
                    font-size: 24px;
                    text-align: left;
                    text-decoration: none
                }
                .bt-link-custom h2{
                    font-weight: 600;
                    font-size: 24px;
                    text-align: left;
                    text-decoration: none;
                    color: #007bff;
                }
                .bt-link-custom:hover h2 {
                    color: #0056b3;
                }
                 .card-body-custom{
                    padding-left: 2rem;
                    font-size: 20px;
                }

                .card-header-custom{
                    padding: 0.75rem 1.25rem;
                    margin-bottom: 0;
                    background-color: #ffff;
                    border-bottom: 0;
                }

                 .card-custom{
                    position: relative;
                    display: flex;
                    flex-direction: column;
                    min-width: 0;
                    word-wrap: break-word;
                    background-color: #fff;
                    background-clip: border-box;
                    border: 0;
                    border-radius: 0.25rem;
                }


                  .hero-tag {
                    color: white;
                    position: absolute;
                    left: 10%;
                    font-style: normal;
                    font-weight: bold;
                    font-size: 48px;
                    padding-top: 200px;

                  }

                  .hero-img {
                    width: 100%;
                    min-width: 1824px;
                  }

                  .text-center-h1 {
                    text-align: center;
                    font-size: 44px;
                    padding-top: 39px;
                  }
                  .arrow_btn_rot {
                    display: flex;
                    justify-content: space-between;
                    width: 100%;
                    align-items: center;
                    border: none;
                    background: #0000;
                    }
                     .arrow_btn_rot:focus {
                        outline: none;
                    }
                    .arrow_btn_rot h2 {
                    padding-right: 28px;
                    }
                    .arrow_btn_rot.collapsed img {
                        transform: rotate(360deg);
                        transition: 0.3s ease-in-out;
                    }
                    .accordion > .card > .card-header {
                        border-radius: 0;
                        margin-bottom: -1px;
                    }
                    .accordion > .card:not(:last-of-type) {
                        border-bottom: 0;
                        border-bottom-right-radius: 0;
                        border-bottom-left-radius: 0;
                    }
                    .mb-0, .my-0 {
                        margin-bottom: 0 !important;
                    }
                    img {
                        vertical-align: middle;
                        border-style: none;
                    }




                  @media only screen and (max-width: 767px) {
                .bt-link-custom h2{
                font-size: 18px;
                }
                .text-center-h1 {
                    font-size: 32px;
                  }}
                   @media only screen and (max-width: 567px) {

                    .text-center-h1 {
                        font-size: 22px;
                      }}

                 @media (min-width: 576px){
                .container, .container-sm {
                    max-width: 540px;
                }
               }
                @media (min-width: 768px){
                .container, .container-sm, .container-md {
                    max-width: 720px;
                }}
                 @media (min-width: 992px){
                .container, .container-sm, .container-md, .container-lg {
                    max-width: 960px;
                }}
                @media (min-width: 1200px){
                .container, .container-sm, .container-md, .container-lg, .container-xl {
                    max-width: 1140px;
                }}

               @media (min-width: 576px){
                .container {
                    max-width: 540px;
                }}
                @media (min-width: 768px){
                .container {
                    max-width: 720px;
                }}
                @media (min-width: 992px){
                .container {
                    max-width: 960px;
                }}
                @media (min-width: 1200px){
                .container {
                    max-width: 1140px;
                }}



                .container {
                    width: 100%;
                    padding-right: 15px;
                    padding-left: 15px;
                    margin-right: auto;
                    margin-left: auto;
                }


        `}
            </style>
        </Layout>
    );
};


const quoteSvg = (
    <svg width="62" height="42" viewBox="0 0 62 42" fill="none" xmlns="http://www.w3.org/2000/svg">
        <path
            d="M22.0459 2.87256C22.0459 4.14209 20.8252 6.48584 18.3838 9.90381C15.9912 13.2729 14.7949 15.0552 14.7949 15.2505C14.7949 15.7388 15.2344 16.1782 16.1133 16.5688C16.9922 16.9595 18.0664 17.3989 19.3359 17.8872C20.6055 18.3755 21.875 19.0103 23.1445 19.7915C24.4629 20.5239 25.5615 21.6714 26.4404 23.2339C27.3193 24.7964 27.7588 26.6519 27.7588 28.8003C27.7588 32.5112 26.4648 35.5386 23.877 37.8823C21.3379 40.2261 18.042 41.3979 13.9893 41.3979C10.083 41.3979 6.7627 40.1284 4.02832 37.5894C1.34277 35.0015 0 31.8521 0 28.1411C0 23.8931 0.854492 19.645 2.56348 15.397C4.32129 11.1001 6.64062 7.56006 9.52148 4.77686C12.4512 1.99365 15.4541 0.602051 18.5303 0.602051C20.874 0.602051 22.0459 1.35889 22.0459 2.87256ZM55.8105 2.87256C55.8105 4.14209 54.5898 6.48584 52.1484 9.90381C49.7559 13.2729 48.5596 15.0552 48.5596 15.2505C48.5596 15.6899 48.877 16.0806 49.5117 16.4224C50.1465 16.7642 50.9521 17.0815 51.9287 17.3745C52.9053 17.6675 53.9307 18.1069 55.0049 18.6929C56.1279 19.2788 57.1777 19.9624 58.1543 20.7437C59.1309 21.4761 59.9365 22.5503 60.5713 23.9663C61.2061 25.3823 61.5234 26.9937 61.5234 28.8003C61.5234 32.5112 60.2295 35.5386 57.6416 37.8823C55.1025 40.2261 51.8066 41.3979 47.7539 41.3979C43.8477 41.3979 40.5273 40.1284 37.793 37.5894C35.1074 35.0015 33.7646 31.8521 33.7646 28.1411C33.7646 23.8931 34.6191 19.645 36.3281 15.397C38.0859 11.1001 40.4053 7.56006 43.2861 4.77686C46.2158 1.99365 49.2188 0.602051 52.2949 0.602051C54.6387 0.602051 55.8105 1.35889 55.8105 2.87256Z"
            fill="#1382E9"/>
    </svg>
);

const circleIcon = (
    <svg width="60" height="60" viewBox="0 0 60 60" fill="none" xmlns="http://www.w3.org/2000/svg">
        <path
            d="M52.4999 30.175V32.5C52.4936 43.5717 45.2056 53.3206 34.5883 56.4598C23.9709 59.599 12.5534 55.3808 6.52728 46.0927C0.501188 36.8046 1.30206 24.6591 8.49558 16.2427C15.6891 7.82629 27.5617 5.14384 37.6749 9.65002"
            stroke="#1382E9" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"/>
        <path d="M55 10L27.5 37.5L20 30" stroke="#1382E9" stroke-width="2" stroke-linecap="round"
              stroke-linejoin="round"/>
    </svg>
);

const clockIcon = (
    <svg width="60" height="60" viewBox="0 0 60 60" fill="none" xmlns="http://www.w3.org/2000/svg">
        <path fill-rule="evenodd" clip-rule="evenodd"
              d="M30 55C43.8071 55 55 43.8071 55 30C55 16.1929 43.8071 5 30 5C16.1929 5 5 16.1929 5 30C5 43.8071 16.1929 55 30 55Z"
              stroke="#1382E9" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"/>
        <path d="M30 15V30L37.5 37.5" stroke="#1382E9" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"/>
    </svg>
);

const textIcon = (
    <svg width="60" height="60" viewBox="0 0 60 60" fill="none" xmlns="http://www.w3.org/2000/svg">
        <path fill-rule="evenodd" clip-rule="evenodd"
              d="M35 5H15C12.2386 5 10 7.23858 10 10V50C10 52.7614 12.2386 55 15 55H45C47.7614 55 50 52.7614 50 50V20L35 5Z"
              stroke="#1382E9" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"/>
        <path d="M35 5V20H50" stroke="#1382E9" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"/>
        <path d="M40 32.5H20" stroke="#1382E9" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"/>
        <path d="M40 42.5H20" stroke="#1382E9" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"/>
        <path d="M25 22.5H22.5H20" stroke="#1382E9" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"/>
    </svg>
);

export default LoginPage;
