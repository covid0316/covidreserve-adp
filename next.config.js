require('dotenv').config();

const withSourceMaps = require('@zeit/next-source-maps');

module.exports = withSourceMaps({
  env: {
    MONGODB_URI: process.env.MONGODB_URI,
    DB_NAME: process.env.DB_NAME,
    SERVER_URL: process.env.SERVER_URL,
    SENDGRID_API_KEY: process.env.SENDGRID_API_KEY,
    EMAIL_FROM: process.env.EMAIL_FROM,
    DOCSPRING_API_TOKEN: process.env.DOCSPRING_API_TOKEN,
    DOCSPRING_API_TOKEN_SECRET: process.env.DOCSPRING_API_TOKEN_SECRET,

    FORM_TEMPLATE_5: 'tpl_HX4f3EkGT9bqPkZYzF',
    FORM_TEMPLATE_2202: 'tpl_b9b6gN6Rez4GX3JE3N',
    FORM_TEMPLATE_413D: 'tpl_QAre6sAJ5rdbsfXtxL',
    FORM_TEMPLATE_15306: 'tpl_tbAPZMZsZXDjEGgjAb'
  },
  webpack(config, options) {
    return config;
  },
});
