import { Field, ErrorMessage } from 'formik';
import { addressFieldsNoLease } from '../lib/addressFields';
import { QuestionProps, AddressQuestion } from './QuestionTypes';
import CloseSvg from './CloseSvg';
import { useState } from 'react';

const AddressCluster: QuestionProps<AddressQuestion> = ({
  question,
  setFieldValue,
  values,
  num,
  className,
  addressFields = addressFieldsNoLease,
  onClose,
  prefix = ''
}) => {
  const [address, city, state, zip, county] = addressFields(
    question.id!
  ) as AddressQuestion[];
  const prefixStr =
    typeof num === 'number' && typeof prefix === 'string'
      ? `${prefix}.${num}.`
      : '';

  return (
    <div className='questionsection'>
      <div
        style={{ display: 'flex' }}
        className={className + ' subquestion tooltippadding'}
      >
        <div style={{ marginRight: 'auto' }}>{question.question}</div>
        {onClose && (
          <Hoverable
            index={num}
            onClick={onClose}
            style={{ marginLeft: 'auto' }}
          >
            <CloseSvg />
          </Hoverable>
        )}
      </div>
      <div className='address'>
        <div className='subfieldheader'>{address.question}</div>
        <div style={{ display: 'flex', flexDirection: 'column' }}>
          <Field
            type='textbox'
            name={prefixStr + address.id}
            // value={
            //   prefixStr
            //     ? values[prefix!][num][address.id!]
            //     : values[address.id!]
            // }
          />
          <ErrorMessage component="div" className="formik-error" name={prefixStr + address.id} />
        </div>
        <div style={{ display: 'flex', padding: '8px 0' }}>
          <div className='addressclustersection'>
            <div className='subfieldheader'>{city.question}</div>
            <div>
              <Field
                type='textbox'
                flag='twothirds'
                name={prefixStr + city.id}
                // value={prefixStr ? values[prefix!][num][city.id!] : values[city.id]}
              />
              <ErrorMessage component="div" className="formik-error" name={prefixStr + city.id} />
            </div>
          </div>
          <div className='addressclustersection'>
            <div className='subfieldheader'>{state.question}</div>
            <div>
              <Field
                type='textbox'
                flag='twothirds'
                name={prefixStr + state.id}
                // value={
                //   prefixStr
                //     ? values[prefix!][num][state.id!]
                //     : values[state.id!]
                // }
              />
              <ErrorMessage component="div" className="formik-error" name={prefixStr + state.id} />
            </div>
          </div>
          <div className='addressclustersection'>
            <div className='subfieldheader'>{zip.question}</div>
            <div>
              <Field
                flag='twothirds'
                type='textbox'
                name={prefixStr + zip.id}
                // value={
                //   prefixStr ? values[prefix!][num][zip.id!] : values[zip.id!]
                // }
              />
              <ErrorMessage component="div" className="formik-error" name={prefixStr + zip.id} />
            </div>
          </div>
          <div className='addressclustersection'>
            <div className='subfieldheader'>{county.question}</div>
            <div>
              <Field
                type='textbox'
                flag='twothirds'
                name={prefixStr + county.id}
                // value={
                //   prefixStr
                //     ? values[prefix!][num][county.id!]
                //     : values[county.id!]
                // }
              />
              <ErrorMessage component="div" className="formik-error" name={prefixStr + county.id} />
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

const Hoverable = ({ children, ...props }) => {
  const [hovered, setHovered] = useState(false);
  const toggleHover = (e: React.MouseEvent<HTMLDivElement, MouseEvent>) => {
    const element = document.getElementById(
      'subquestionsectionhoverable-' + props.index
    );
    if (!hovered) {
      element?.classList.add('subquestionsectionhoverable');
    } else {
      element?.classList.remove('subquestionsectionhoverable');
    }
    setHovered(!hovered);
  };
  return (
    <div {...props} onMouseEnter={toggleHover} onMouseLeave={toggleHover}>
      {children}
    </div>
  );
};

export default AddressCluster;
