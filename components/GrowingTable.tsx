import React from 'react';
import { GrowingTableQuestion, QuestionProps } from './QuestionTypes';
import { FieldArray, Field, ErrorMessage, FieldArrayRenderProps } from 'formik';
import AddButtonFab from './AddButtonFab';
import { useState } from 'react';

const GrowingTable: QuestionProps<GrowingTableQuestion> = ({
  question, errors, touched, values, setFieldValue
}) => {
  const onAdd = (length: number) => {
    const data = (question.onAdd || ((__) => ({})))(length);
    for (const field in data) {
      setFieldValue(`${question.id}.${length - 1}.${question.columnIds[0]}`, data[field]);
    }
    return data;
  };
  const currentValues = (values[question.id] && values[question.id].length > 0) ? values[question.id] : [onAdd(1)];
  const [length, setLength] = useState(currentValues.length);
  const direction = question.direction || 'vertical';
  return (
    <div>
      <FieldArray
        name={question.id}
        render={arrayHelpers => {
          return (
            <div>
              <div className="growingtable-wrapper">
                <div className="growingtable-wrapper-inner">
                  <div style={{ margin: '0 auto', paddingTop: '0' }} className='question'>
                    {question.question}
                  </div>
                </div>
                { direction === 'vertical' ?
                  VerticalTable(question, arrayHelpers, length, setLength)
                :
                  HorizontalTable(question, arrayHelpers, length, setLength)
                }
              </div>
              <div style={{ display: 'flex', padding: '16px 32px'}}>
                <div
                  className='fabsection'
                  onClick={() => {
                    arrayHelpers.push(onAdd(length + 1));
                    setLength(length + 1);
                  }}
                >
                  <AddButtonFab
                    style={{ margin: '0', padding: '16px 0' }}
                    label={`Add another ${question.rowTitle}`}
                  />
                </div>
              </div>
            </div>);
        }}
      />
      <style jsx>{`
        .growingtable-wrapper {
          padding: 0 32px;
        }
        .growingtable-wrapper-inner {
          display: flex;
          padding: 16px 16px;
        }
      `}</style>
    </div>
  );
};

const HorizontalTable = (question: GrowingTableQuestion, arrayHelpers: FieldArrayRenderProps, length: number, setLength: (n: number) => void) => {
  return (
    <div className="growingtable horizontal">
      {question.columns.map((col, colIdx) =>
        <div className='flex-grid'>
          <div className='flex-grid-cell' style={{flex: 1}}>{col}</div>
          {new Array(length).fill(0).map((_, rowIdx) => {
            const name = `${question.id}.${rowIdx}.${question.columnIds[colIdx]}`;
            return (<div className='flex-grid-cell' style={{flex: 1}}>
              <Field flag='flex' name={name}></Field>
              <div>
                <ErrorMessage name={name} render={msg => <div className='formik-error'>{msg}</div>} />
              </div>
            </div>);
          })}
        </div>
      )}
    </div>
  );
};

const VerticalTable = (question: GrowingTableQuestion, arrayHelpers: FieldArrayRenderProps, length: number, setLength: (n: number) => void) => {
  const cancelGridSize = 1;
  const totalGrid = question.gridSize.reduce(
    (num, sum) => num + sum,
    cancelGridSize
  );
  return (
    <div className="growingtable vertical">
      <div className='flex-grid'>
        {question.columns
          .map((col, idx) => {
            return (
              <div
                style={{
                  flex: `${(question.gridSize[idx] / totalGrid) *
                    100}%`
                }}
                className='flex-grid-cell'
              >
                {col}
              </div>
            );
          })
          .concat(
            <div
              style={{
                flex: `${(1 / totalGrid) * 100}%`,
                border: 'none'
              }}
              className='flex-grid-cell'
            />
          )}
      </div>
      {new Array(length).fill(0).map((_, rowIdx) => {
        return (
          <div className='flex-grid'>
            {question.columnIds
              .map((__, colIdx) => {
                const name = `${question.id}.${rowIdx}.${question.columnIds[colIdx]}`;
                return (
                  <div
                    className={`rowhoverable-${rowIdx} flex-grid-cell`}
                    style={{
                      flex: `${(question.gridSize[colIdx] /
                        totalGrid) *
                        100}%`
                    }}
                  >
                    <Field
                      flag='flex'
                      name={name}
                    />
                    <div>
                    <ErrorMessage name={name} render={msg => <div className='formik-error'>{msg}</div>} />
                    </div>
                  </div>
                );
              })
              .concat(
                <div
                  style={{
                    flex: `${(1 / totalGrid) * 100}%`,
                    display: 'flex',
                    border: 'none'
                  }}
                  className='flex-grid-cell'
                >
                  <Hoverable index={rowIdx}>
                    <svg
                      onClick={() => {
                        arrayHelpers.remove(rowIdx);
                        setLength(length - 1);
                      }}
                      style={{ margin: 'auto', cursor: 'pointer' }}
                      width='16'
                      height='10'
                      viewBox='0 0 16 10'
                      fill='blue'
                      xmlns='http://www.w3.org/2000/svg'
                    >
                      <path
                        className='close-svg-path'
                        d='M4 1L12 9M12 1L4 9'
                        stroke='#1F2933'
                        stroke-width='1.5'
                        stroke-linecap='round'
                        stroke-linejoin='round'
                      />
                    </svg>
                  </Hoverable>
                </div>
              )}
          </div>
        );
      })}
    </div>
  );
};

// Component is highlighted when corresponding <Hoverable> is hovered
const Highlightable = ({ children, ...props }) => {
  return <div id={`rowhoverable-${props.index}`}>{children}</div>;
};

// Highlights the component wrapped in <Hoverable> onHover
const Hoverable = ({ children, ...props }) => {
  const [hovered, setHovered] = useState(false);
  const toggleHover = (e: React.MouseEvent<HTMLDivElement, MouseEvent>) => {
    const elements = document.getElementsByClassName(
      'rowhoverable-' + props.index
    );

    for (let x = 0; x < elements.length; x++) {
      const element = elements.item(x);
      if (!hovered) {
        element?.classList.add('rowhoverableactive');
      } else {
        element?.classList.remove('rowhoverableactive');
      }
    }
    setHovered(!hovered);
  };
  return (
    <div {...props} onMouseEnter={toggleHover} onMouseLeave={toggleHover}>
      {children}
    </div>
  );
};

export default GrowingTable;
