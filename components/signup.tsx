import { NextPage } from 'next';
import React, { useState } from 'react';
import { Formik, Form, Field } from 'formik';
import libphonenumber from 'google-libphonenumber';
import * as Yup from 'yup';

const phoneUtil = libphonenumber.PhoneNumberUtil.getInstance();

type Props = {
  compact?: boolean,
  successLabel?: string;
  onSubmit: (user: User) => void
}

export type User = {
  email: string,
  // password: string,
  firstname: string,
  lastname: string,
  phone: string
};

const SignupSchema = Yup.object().shape({
  email: Yup.string().email("Email is invalid").required("Email is required"),
  // password: Yup.string().required("Password is required"),
  firstname: Yup.string().required("First name is required"),
  lastname: Yup.string().required("Last name is required"),
  phone: Yup.string()
  .required("Phone number is required")
  .test(
    'is-phone',
    'Phone is invalid. Please use a US number.',
    function (number) {
      let isPhoneNumer = false;
      try {
        isPhoneNumer = phoneUtil.isValidNumberForRegion(phoneUtil.parse(number, 'US'), 'US');
      } catch {}
      return !number || isPhoneNumer;
    }
  )
});

const SignUp: NextPage<Props> = ({ compact = false, onSubmit, successLabel }) => {
  return (
    <div>
      <Formik initialValues={{
          email: '',
          password: '',
          firstname: '',
          lastname: '',
          phone: ''
        }}
        validationSchema={SignupSchema}
        onSubmit={onSubmit}
      >
        {({ errors, touched }) => (
        <Form>
          <label htmlFor="email">
            <div>Email address*</div>
          </label>
          <div className="form-control-wrapper">
          < Field name="email" />
          </div>
          <div className="validate">
            {errors.email && touched.email && errors.email}&nbsp;
          </div>
          {/* <label htmlFor="password">
            <div>Password*</div>
          </label>
          <div className="form-control-wrapper">
            <Field name="password" type="password" />
          </div>
          <div className="validate">
            {errors.password && touched.password && errors.password}&nbsp;
          </div> */}
          <div className={`names ${compact ? "compact" : ''}`}>
            <div>
              <label htmlFor="firstname">
                <div>First Name*</div>
              </label>
              <div className="form-control-wrapper">
                <Field name="firstname" />
              </div>
              <div className="validate">
                {errors.firstname && touched.firstname && errors.firstname}&nbsp;
              </div>
            </div>
            <div>
              <label htmlFor="lastname">
                <div>Last Name*</div>
              </label>
              <div className="form-control-wrapper">
                <Field name="lastname" />
              </div>
              <div className="validate">
                {errors.lastname && touched.lastname && errors.lastname}&nbsp;
              </div>
            </div>
            </div>
          <label htmlFor="phone">
            <div>Phone Number*</div>
          </label>
          <div className="form-control-wrapper">
            <Field name="phone" />
          </div>
          <div className="validate">
            {errors.phone && touched.phone && errors.phone}&nbsp;
          </div>
          <div className="disclaimer">By clicking Submit, you agree to the CovidReserve Terms of Service, Terms of Use, and have read and acknowledge our US Privacy Statement</div>
        {successLabel && <div style={{ textAlign: 'center', padding: '28px 0 8px' }}>{successLabel}</div>}
          <div className="text-center center">
          <button type="submit" >Submit</button>
          </div>
        </Form>
      )}
      </Formik>
      <style jsx>{`
        label > div {
          font-size: 18px;
        }
        .names.compact {
          display: flex;
          justify-content: space-between;
          width: 100%;
        }
        .names.compact > div {
          width: 48%;
        }
        .validate {
          line-height: 16px;
          color: #D92C23;
          font-size: 12px;
        }
        .disclaimer {
          box-sizing: border-box;
          padding: 0 24px;
          margin-top: 8px;
          font-style: oblique;
          text-align: center;
          font-size: 14px;
          color: #4B6075;
        }
        .form-control-wrapper {
          box-sizing: border-box;
          position: relative;
          width: 100%;
        }
        .form-control-wrapper :global(input) {
          color: #4B6075;
          font-size: 18px;
          font-family: inherit;
          box-sizing: border-box;
          position: relative;
          width: 100%;
          margin: 0;
          border-color: black;
          padding: 10px 20px;
        }
        button {
          margin: 20px auto 0 auto;
          padding: 0;
          background: #1382E9;
          width: 165px;
          height: 48px;
          font-size: 20px;
          color: white;
          border-radius: 5px;
          border: none;
        }
        .center {
                  margin-top: 10px;
                  text-align: center;
                  font-size: 20px;
                }
      `}</style>
    </div>
  );
};

export { SignUp };
