import { useState } from 'react';
import DynamicQuestion from '../lib/renderQuestion';
import { QuestionProps, MultiQuestion } from './QuestionTypes';
import { FieldArray } from 'formik';
import AddButtonFab from './AddButtonFab';
import CloseSvg from './CloseSvg';
import { getInitialValuesForQuestion } from '../lib/forms/helper';

const Multi: QuestionProps<MultiQuestion> = ({
  question,
  setFieldValue,
  values,
  className,
  offsetNum = 0,
  initialCount = 1,
  addLabel = 'Add another',
  errors
}) => {
  const initialObject = question.fieldFunction(offsetNum).reduce((obj, q) => getInitialValuesForQuestion(q, obj), {})

  return (
    <div
      className='multiquestionsection'
      style={{
        display:
          question.showCondition && question.showCondition(values) === false
            ? 'none'
            : 'block'
      }}
    >
      {question.question && (
        <div className={className + ' questionsection'}>{question.question}</div>
      )}
      {question.tooltip && <div className='formtooltip'>{question.tooltip}</div>}
      <FieldArray
        name={question.id!}
        render={arrayHelpers => (
          <div>
            {values[question.id].map((_, index) => {
              const templates = question.fieldFunction!(index + offsetNum);
              const components = templates.map((template, subindex) => {
              const component = (
                <div key={`multi-${question.id}-${index}-${subindex}`}>
                  <DynamicQuestion
                    errors={errors}
                    question={template}
                    setFieldValue={setFieldValue!}
                    values={values}
                    num={index + offsetNum}
                    prefix={question.id}
                    className={'subquestionsection subquestion'}

                  />
                </div>
                );
                return component;
              });
              return (
                <Closable prefix={question.id} key={`closableTag-${question.id}-${index}`} index={index + offsetNum} onClose={
                    () => {
                        arrayHelpers.remove(index + offsetNum);
                      }
                }>
                  <div className={'subquestionsection'}>{components}</div>
                </Closable>
              );
            })}

            <div
              className='fabsection'
              onClick={() => {
                arrayHelpers.push(initialObject);
              }}
            >
              {errors && typeof errors[question.id] === 'string' && (
                <div className='formik-error'>{errors[question.id]}</div>
              )}
              <AddButtonFab label={addLabel!} />
            </div>
          </div>
        )}
      />
    </div>
  );
};

const Hoverable = ({ children, prefix, ...props }) => {
  const [hovered, setHovered] = useState(false);
  const toggleHover = (e: React.MouseEvent<HTMLDivElement, MouseEvent>) => {
    const element = document.getElementById(
      prefix+'subquestionsectionhoverable-' + props.index
    );
    if (!hovered) {
      element?.classList.add('subquestionsectionhoverable');
    } else {
      element?.classList.remove('subquestionsectionhoverable');
    }
    setHovered(!hovered);
  };
  return (
    <div {...props} onMouseEnter={toggleHover} onMouseLeave={toggleHover}>
      {children}
    </div>
  );
};

const Closable = ({ children, index, onClose, prefix }) => {
  return <div id={prefix+'subquestionsectionhoverable-' + index}>
    <Hoverable prefix={prefix} style={{display: 'flex'}} onClick={onClose} index={index}><div style={{margin: '16px 16px -48px auto'}}><CloseSvg /></div></Hoverable>
    {children}
    </div>;
};

export default Multi;
