import { Formik, FormikHelpers } from 'formik';
import * as storage from '../lib/storage';
import * as Yup from 'yup';

type FormikWrapperParams = {
  initialValues: object,
  stepNum?: number,
  children: any,
  storageKey: string | number,
  validationSchema?: Yup.Schema<any>,
  onSubmit?: (values: any, formikHelpers: FormikHelpers<any>) => void | Promise<any>;
};



/**
 * @todo: Implement Yup.Schema validation here
 *
 * Take in entire form question schema as a prop and do dynamic validation
**/

const FormikWrapper  = (
  { initialValues, stepNum = 0, children, storageKey, validationSchema, onSubmit }: FormikWrapperParams
) => {
  const defaultSubmit =  (values, { setSubmitting }) => {
    setSubmitting(false);
    storage.FormStorageHelper.set(values, storageKey, stepNum);
  };
  return <Formik
    initialValues={{ ...initialValues }}
    validationSchema={validationSchema}
    validate={values => {
      const errors = {};
      return errors;
    }}
    onSubmit={onSubmit || defaultSubmit}
  >
    {children}
  </Formik>
}

export default FormikWrapper;
