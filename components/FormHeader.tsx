export type FormHeaderType = ({ step, total, form }: { step: number, total: number, form: number }) => JSX.Element;
import questions from '../lib/formFields';
import React, { useState } from 'react';
import JumpQuestionDropdown from './selectOption';
import Link from 'next/link';
import * as storage from '../lib/storage';
import ProgressBar from './ProgressBar';

const getQuestions = async (form: number) => {
    let renderArr: {value: string, label: any}[] = [];
    const item = await storage.FormStorageHelper.get(form) || {};
    const len = Object.keys(item).length;
    for(let key=0; key<len; key++){
        let ques = questions[key][0].question;
        let link = key;
        renderArr.push({
            value: String(ques),
            label: <Link href={String(link)}><a>{String(ques)}</a></Link>,
         })
    }
    return renderArr;
};

const FormHeader: FormHeaderType = ({ step, total, form }) => {
    const [questions, setQuestions] = useState(undefined);
    const amountComplete = (step / (total+1) * 100);

    // @todo: refactor to useEffect
    if (questions === undefined) { // only trigger on undefined -- not null
        setQuestions(null as any); // set to null while loading to only trigger 1 promise
        getQuestions(form).then(value => setQuestions(value as any));
    }

    return (<div style={{ margin: '0 64px' }}>
        <div className="formprogress"><ProgressBar amountComplete={amountComplete}/></div>
        <div className='questionheader' >
            Question {step} {false && <JumpQuestionDropdown val={questions}></JumpQuestionDropdown>}
        </div>
        <style jsx>{`
            .formprogress {
                float: right;
                background-color: white;
                height: 24px;
            }
        `}</style>
    </div>);
}

export default FormHeader;
