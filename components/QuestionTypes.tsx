import * as Yup from 'yup';
import { PdfRequest } from "../pages/api/data/pdfTypes";

export type SetFieldValueType = (field: string, value: any, shouldValidate?: boolean | undefined) => void;

export interface Field {
  id: string;
  showCondition?: (values: object) => boolean;
  displayName: string;
  onChangeHandler?: (setFieldValue: SetFieldValueType, e: any) => void;
}

export type QuestionProps<T> = ({}: {
  question: T;
  values: object;
  setFieldValue: SetFieldValueType;
  className: string;
  num: number | null;
  prefix?: string;
  id?: string;
  errors?: object;
  title?: string;
  touched?: object;
  onClose?: (event: React.MouseEvent<HTMLDivElement, MouseEvent>) => void;
  onCloseHover?: Function;
  addressFields?: (prefix: string, num?: number | undefined) => Question[];
  completeForm?: any;
  /**
   * Start indexing at {offsetNum} instead of at 0
   * damaged-property-leased-{offsetNum}
   */
  offsetNum?: number; //multi
  initialCount?: number; //multi
  addLabel?: string; //multi
}) => JSX.Element;

export type Question = MultiQuestion | RadioQuestion | CheckboxQuestion | AddressQuestion | TextboxQuestion | NoneQuestion | DateQuestion | GrowingTableQuestion;

interface BaseQuestion {
  type: string;
  formno?: Array<string | number>;
  showCondition?: (...args: any[]) => boolean;
  question: string | null;
  tooltip?: JSX.Element | string;
  renderData?: any; // dumping ground for metadata to assit rendering
  validation?: Yup.Schema<any>;
  readonly?: boolean;
  computedValue?: (form: any) => any
}

export interface GrowingTableQuestion extends BaseQuestion {
  type: 'growingtable';
  id: string;
  columns: string[];
  columnIds: string[];
  columnTypes?: Array<'string' | 'number' | 'date'>;
  gridSize: number[];
  rowTitle: string;
  direction?: 'vertical' | 'horizontal';
  onAdd?: (length: number) => object
}

export interface MultiQuestion extends BaseQuestion {
  type: 'multi';
  id: string;
  initialCount: number;
  addLabel: string;
  fieldFunction: (num: number) => Question[];
}
export interface RadioQuestion extends BaseQuestion {
  type: 'radio';
  fields: Field[];
  id?: string;
  other?: string;
  onChangeHandler?: (setFieldValue: SetFieldValueType, e: any) => any;
}
export interface CheckboxQuestion extends BaseQuestion {
  type: 'checkbox';
  fields: Field[];
  other?: string;
  id?: string;
  minimumSelected?: number; // At least this many should be selected for this question (default 0)
}

export interface AddressQuestion extends BaseQuestion {
  type: 'address';
  id: string | keyof PdfRequest;
}

export interface TextboxQuestion extends BaseQuestion{
  type: 'text' | 'currency' | 'textarea';
  id: string | keyof PdfRequest;
}

export interface DateQuestion extends BaseQuestion{
  type: 'date',
  id: string
}

export interface NoneQuestion extends BaseQuestion {
  type: 'none';
}
