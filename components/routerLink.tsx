import React from 'react';
import { NextPage } from 'next';
import Link from 'next/link';
import { useRouter } from 'next/router';

type Props = {
  href: string
  style?: any
}

/**
 * Same as next/link/Link except it adds a "selected"
 * class when the current route is the link's href.
 */

const RouterLink: NextPage<Props> = ({ href, children, style }) => {
  if (React.isValidElement(children)) {
    const router = useRouter();
    // look at first 2 levels only
    const first2Href = href.split("/").slice(1, 3).join("/");
    const first2Path = router.pathname.split("/").slice(1, 3).join("/");
    const className =
      ((first2Path === first2Href && href !== '/form/15306/documents/upload' && router.pathname !== '/form/15306/documents/upload') || (href === '/form/15306/documents/upload' && router.pathname === '/form/15306/documents/upload')) ?
        `${children.props.className || ''} selected` :
        children.props.className;

    return <Link href={href}>{React.cloneElement(children, { className })}</Link>
  } else {
    return null;
  }
}

export { RouterLink };
