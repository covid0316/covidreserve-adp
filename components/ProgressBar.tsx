import { NextPage } from 'next';

type Props = {
  amountComplete: number,
  width?: number,
  size?: 'normal' | 'compact'
};

const ProgressBar: NextPage<Props> = ({amountComplete, width = 322, size = 'normal'}) => {
  return (
    <div className={`formprogress ${size}`}>
      <div className='progressbar'><div className='inner' style={{ width: amountComplete + '%' }}></div></div>
      <div className='progresstext'>
        { size === 'normal' ?
          <> Percent complete ({Math.round(amountComplete)}%)  </>
        :
          <> ({Math.round(amountComplete)}% complete) </>
        }
      </div>

      <style jsx>{`
        .formprogress.compact {
          display: flex;
          align-items: center;
        }
        .progressbar {
          height: 4px;
          background: #B1B9C0;
          width: ${width}px;
          position: relative;
        }
        .progresstext {
          font-size: 14px;
          text-align: right;
        }
        .compact .progresstext {
          padding: 0 15px 0 5px;
          font-size: 10px;
        }
        .progressbar .inner {
          position: absolute;
          left: 0;
          top: 0;
          bottom: 0;
          background: #2CA01C;
        }
      `}</style>
    </div>
  );
};

export default ProgressBar;
