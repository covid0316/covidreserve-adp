/* eslint-disable react/jsx-no-bind */
import { Field, ErrorMessage } from 'formik';
import { QuestionProps, DateQuestion } from './QuestionTypes';

const Datebox: QuestionProps<DateQuestion> = ({ question, values, className, prefix = '', num }) => {
  const prefixStr =
    typeof num === 'number' && typeof prefix === 'string'
      ? `${prefix}.${num}.`
      : '';

  return (
  <div
    className='questionsection'
    style={{ display: question.showCondition && question.showCondition(values) === false ? 'none' : 'block' }}
  >
    <div>
      <div className={className}>{question.question}</div>
      {question.tooltip && <h3 className='tooltip'>{question.tooltip}</h3>}
    </div>
    <div className="control" style={{display: 'flex'}}>
      <div>
        <div><label htmlFor={prefixStr + question.id + '-month'}>MM</label></div>
        <Field
          maxlength={2}
          size={2}
          type="textbox"
          key={prefixStr + question.id + '-month'}
          flag="half"
          name={prefixStr + question.id + '-month'}
          value={prefixStr ? values[prefix!][num][question.id+'-month'] : values[question.id!+'-month']}
          />
      </div>
      <div>
        <div><label htmlFor={prefixStr + question.id + '-day'}>DD</label></div>
        <Field
          maxlength={2}
          size={2}
          type="textbox"
          key={prefixStr + question.id + '-day'}
          flag="half"
          name={prefixStr + question.id + '-day'}
          value={prefixStr ? values[prefix!][num][question.id+'-day'] : values[question.id!+'-day']}
          />
      </div>
      <div>
        <div><label htmlFor={prefixStr + question.id + '-year'}>YYYY</label></div>
        <Field
          maxlength={4}
          size={5}
          type="textbox"
          key={prefixStr + question.id + '-year'}
          flag="half"
          name={prefixStr + question.id + '-year'}
          value={prefixStr ? values[prefix!][num][question.id+'-year'] : values[question.id!+'-year']}
          />
      </div>
    </div>
    <div className="formik-error"><ErrorMessage name={`${question.id}-year`}/></div>
    <style jsx>{`
      label {
        display: block;
        text-align: center;
      }
      .control :global(input) {
        padding: 8px 12px;
        margin: 0 4px;
      }
    `}</style>
  </div>
)};

export default Datebox;
