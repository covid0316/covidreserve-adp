/* eslint-disable react/jsx-no-bind */
import { Field, ErrorMessage } from 'formik';
import { QuestionProps, TextboxQuestion } from './QuestionTypes';

const Textbox: QuestionProps<TextboxQuestion> = ({ question, values, className, completeForm, prefix = '', num }) => {
  const prefixStr =
    typeof num === 'number' && typeof prefix === 'string'
      ? `${prefix}.${num}.`
      : '';

    const isCurrency = question.type === 'currency';

    const valueFor = (val: any) => {
        return (question.computedValue && completeForm) ? question.computedValue(completeForm) : val;
    };

    return (
        <div
            className='questionsection'
            style={{display: question.showCondition && question.showCondition(values) === false ? 'none' : 'block'}}
        >


            <div>
                <div className={className}>{question.question}</div>
            </div>
            <div className={`form-group ${ isCurrency ? "currency" : ''}`}>
                { isCurrency ?
                    <div className="currency-symbol">$</div>
                : null }
                {question.readonly ? 
                    <div className="readonly">{prefixStr ? valueFor(values[prefix!][num][question.id]) : valueFor(values[question.id!])}</div>
                :
                    <Field
                        className="form-control"
                        type={'text'}
                        component={question.type === 'textarea' ? 'textarea' : null}
                        flag="half"
                        name={prefixStr + question.id}
                        value={prefixStr ? valueFor(values[prefix!][num][question.id]) : valueFor(values[question.id!])}
                    />
                }

                {question.tooltip &&
                <small id="emailHelp" className="form-text text-muted">{question.tooltip}</small>}
                <div className="formik-error"><ErrorMessage name={question.id!}/></div>
            </div>

            <style jsx>{`
                div.readonly {
                    line-height: 32px;
                    padding-left: 31px;
                }
                .form-group :global(input[flag=half]) {
                    width: 320px;
                }
                small {
                    margin-top: 20px;
                    display: block;
                }
                .form-group.currency {
                    position: relative;
                }
                .currency-symbol {
                    position: absolute;
                    top: 6px;
                    left: 12px;
                }
                .currency :global(input[type=text]) {
                    padding-left: 30px;
                }
                .form-group :global(textarea) {
                    border: 1px solid #1f2933;
                    border-radius: 0.25rem;
                    width: 75% !important;
                    min-height: 225px;
                    margin-left: 0;
                }
            `}</style>
        </div>
    )
};

export default Textbox;
