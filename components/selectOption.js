import React from 'react';
import Select from 'react-select';

class jumpQuestionDropdown extends React.Component {

  constructor(props){
      super(props);

      this.state = {
        selectedOption: null,
      };
  }  

  handleChange = selectedOption => {
    this.setState(
      { selectedOption },
      () => console.log(`Option selected:`, this.state.selectedOption)
    );
  };

  render() {
    const { selectedOption } = this.state;
    //TODO - Styling the drop-down
    return (
      <Select
        value={selectedOption}
        onChange={this.handleChange}
        options={this.props.val}
        placeholder='Jump to'
      />
    );
  }
}

export default jumpQuestionDropdown;