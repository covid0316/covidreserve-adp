import Checkbox from '../components/Checkbox';
import Textbox from '../components/Textbox';
import Radio from '../components/Radio';
import Multi from '../components/Multi';
import AddressCluster from '../components/AddressCluster';
import Datebox from '../components/Datebox';
import GrowingTable from '../components/GrowingTable';
import { QuestionProps, Question } from '../components/QuestionTypes';

const DynamicQuestion: QuestionProps<Question> = ({
  question,
  setFieldValue,
  values,
  num,
  className,
  id,
  onClose,
  onCloseHover,
  prefix,
  errors,
  completeForm
}) => {
  if (question.type === 'checkbox') {
    return (
      <Checkbox
        key={id || question.question! + num}
        question={question}
        setFieldValue={setFieldValue}
        values={values}
        num={num}
        errors={errors}
        className={className}
        onClose={onClose}
        onCloseHover={onCloseHover}
        prefix={prefix}
      />
    );
  }

  if (question.type === 'radio') {
    return (
      <Radio
        num={num}
        errors={errors}
        key={id || question.question! + num}
        question={question}
        setFieldValue={setFieldValue}
        values={values}
        className={className}
        onClose={onClose}
        onCloseHover={onCloseHover}
        prefix={prefix}
      />
    );
  }

  if (question.type === 'text' || question.type === 'currency' || question.type === 'textarea') {
    return (
      <Textbox
        errors={errors}
        num={num}
        setFieldValue={setFieldValue}
        key={id || question.id}
        question={question}
        values={values}
        className={className}
        onClose={onClose}
        onCloseHover={onCloseHover}
        prefix={prefix}
        completeForm={completeForm}
      />
    );
  }

  if (question.type === 'date') {
    return (
      <Datebox
        errors={errors}
        num={num}
        setFieldValue={setFieldValue}
        key={id || question.id}
        question={question}
        values={values}
        className={className}
        onClose={onClose}
        onCloseHover={onCloseHover}
        prefix={prefix}
      />
    );
  }

  if (question.type === 'none') {
    return (
      <div
        className={'questionsection ' + className}
        key={id || question.question! + num}
        style={{
          display:
            question.showCondition && question.showCondition(values) === false
              ? 'none'
              : 'block'
        }}
      >
        {question.question}
        {!!question.tooltip && (
          <div className='formtooltip'>{question.tooltip}</div>
        )}
      </div>
    );
  }

  if (question.type === 'multi') {
    return (
      <Multi
        errors={errors}
        num={num}
        key={id || question.question! + num}
        question={question}
        setFieldValue={setFieldValue}
        values={values}
        className={className}
        onClose={onClose}
        onCloseHover={onCloseHover}
        initialCount={question.initialCount}
        addLabel={question.addLabel}
      />
    );
  }

  if (question.type === 'growingtable') {
    return (
      <GrowingTable
        question={question}
        values={values}
        setFieldValue={setFieldValue}
        className={''}
        errors={errors}
        num={null}
      />
    );
  }

  if (question.type === 'address') {
    return (
      <AddressCluster
        className={className}
        key={id || question.id}
        question={question}
        values={values}
        num={num}
        setFieldValue={setFieldValue}
        onClose={onClose}
        onCloseHover={onCloseHover}
        prefix={prefix}
      />
    );
  }

  console.error('Unsupported question type', question);
  throw new Error(`Unsupport question type ${(question as any).type}`);
};

export default DynamicQuestion;
