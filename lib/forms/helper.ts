import { Question } from "../../components/QuestionTypes";

export const getInitialValuesForQuestion = (item: Question, obj = {}) => {
  if (item.type === 'multi') {
    if (item.initialCount) {
      const initialObject = item.fieldFunction(0).reduce((obj, q) => getInitialValuesForQuestion(q, obj), {})
      return { [item.id]: new Array(item.initialCount).fill(initialObject), ...obj };
    }
    return { [item.id]: new Array(item.initialCount).fill({}), ...obj };
  }

  if (item.type === 'growingtable') {
    return { [item.id]: [], ...obj };
  }

  if (item.type === 'none') {
    return { ...obj };
  }

  if ((item.type === 'radio' || item.type === 'checkbox') && item.other) {
    // eslint-disable-next-line no-param-reassign
    obj = { [item.other]: false, ...obj }; // @todo: re-assign is bad
  }

  if ((item.type === 'radio' || item.type === 'checkbox') && item.fields) {
    return item.fields.reduce(
      (obj2, item2) => ({ [item2.id]: false, ...obj2 }),
      obj
    );
  }

  if (!(item.type === 'radio' || item.type === 'checkbox') && item.id) {
    if (item.type === 'address') {
      return {
        [item.id + '-street']: '',
        [item.id + '-state']: '',
        [item.id + '-city']: '',
        [item.id + '-zip']: '',
        [item.id + '-county']: '',
        ...obj,
      };
    }
    return { [item.id]: (item.type === 'text' || item.type === 'currency' || item.type === 'textarea') ? '' : false, ...obj };
  }

  console.error(item);
  throw new Error('unexpected end ' + item.question);
}
