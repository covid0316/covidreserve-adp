import { PdfRequest } from '../../pages/api/data/pdfTypes';

type ArrayTypes =
  | 'damaged-property'
  | 'insurance-coverage'
  | 'owner'
  | 'business-owner';

type _Form5Middleware = (values: object) => [PdfRequest, object];
export const middleware: _Form5Middleware = (values: {[key:string]: any}) => {
  // ignore
  let initial = {};
  let mailingAddressIsDamagedOffset = 0;
  if (values['mailing-address-is-damaged'] === true) {
    initial['damaged-property-street-0'] = values['mailing-address-street'];
    initial['damaged-property-city-0'] = values['mailing-address-city'];
    initial['damaged-property-state-0'] = values['mailing-address-state'];
    initial['damaged-property-zip-0'] = values['mailing-address-zip'];
    initial['damaged-property-county-0'] = values['mailing-address-county'];
    initial['damaged-property-leased-0'] = values['mailing-address-leased'];
    initial['damaged-property-owned-0'] = values['mailing-address-owned'];
    mailingAddressIsDamagedOffset = 1;
  }

  if (values['insurance-coverage-no'] === true) {
    delete values['insurance'];
  }

  const extra = {};
  const filtered = Object.keys(values)
    .filter(key => !ignoreList[key])
    .reduce((obj, key) => {
      if (extraList[key]) {
        if (!!values[key]) {
          extra[key] = values[key];
        }
      } else if (Object.keys(arrayValues).includes(key)) {
        (values[key] as Array<any>).forEach((value, index) => {
          if ((index + (key === 'damaged-property' ? mailingAddressIsDamagedOffset : 0)) >= arrayValues[key]) {
            // move to extra
            extra[key] = [ ...(extra[key] || []), value ];
          } else {
            // move to obj
            Object.keys(value).forEach((subval) => {
              if (!arraySubvalueIgnore[subval]) {
                obj[`${subval}-${index}`] = value[subval];
              }
            });
          }
        });
      } else if (typeof values[key] === 'object') {
        obj = { ...obj,  ...values[key] };
      } else {
        obj[key] = values[key];
      }

      return obj;
    }, initial as PdfRequest);

  return [filtered, extra];
};

// These values are saved as prefix-0, prefix-1 up to max number
// The rest are moved to the "extra" list
// All multi belong here
const arrayValues: { [key: string]: number } = {
  'owner': 2,
  'business-owner': 1,
  'damaged-property': 1,
  'insurance': 1
};

// If these are present in arrayValues, ignore
enum arraySubvalueIgnore {
  'business-owner-county'=1,
  'owner-county'
}

// These values are moved to the "extra" list
enum extraList {
  'indictment-string'=1,
  'suspended-string',
  'member-string',
  'delinquent-string',
  'lawsuits-string',
  'bankruptcy-string',
  'convicted-string',
  'federal-loan-string',
};

// These values are ignored
enum ignoreList {
  'insurance-coverage-no'=1,
  'insurance-coverage-yes',
  'mailing-address-not-damaged',
  'trade-name-no',
  'trade-name-yes',
  'mailing-address',
  'multiple-damaged-properties-no',
  'multiple-damaged-properties-yes',
  'mailing-address-owned',
  'mailing-address-leased',
};
