import insuranceFields, { insuranceSchema } from '../insuranceFields';
import { ownerFields, businessOwnerFields, ownerSchema } from '../ownerFields';
import { Question } from '../../components/QuestionTypes';
import * as Yup from 'yup';
import { addressSchema } from '../addressFields';

function atLeastOneOf(id: string, list: string[], message?: string) {
  return Yup.boolean().test(id, message || "At least one option must be selected", function() {
    const p = this.parent;
    const result = Object.keys(p).some((f => {
      return !!p[f] && list.includes(f)
    }));
    return result;
  });
}

function radioSet(prefix: string) {
  const id = prefix + '-id';
  const list = [prefix + '-yes', prefix + '-no'];
  const radioSchemas = list.reduce((obj, v) => ({ ...obj, [v]: Yup.boolean().required('Required')}), {});
  return Yup.object().shape({
    ...radioSchemas,
    [id]: atLeastOneOf(id, list),
    [prefix + '-string']: Yup.string().when(list[0], {
      is: true,
      then: Yup.string().required('Required'),
      otherwise: Yup.string()
    }),
  });
}

function getConditionalTextBox(id: string, when: string) {
  return Yup.string().when(when, {
    is: true,
    then: Yup.string().required('Required'),
    otherwise: Yup.string(),
  });
}

const tooltipList = {paddingTop: '5px', paddingBottom: '5px'}

// Schema: export[FORM WIZARD PAGE NUMBER][QUESTION NUMBER][ANSWER OPTIONS FOR CHECKBOX/RADIO]
const questions: Array<Question[]> = [
  [{
    type: 'checkbox',
    question: 'This application is for:',
    id: 'businesstype',
    validation: Yup.object().shape({
      'nonprofit': Yup.boolean(),
      'vet_org': Yup.boolean(),
      'tribal': Yup.boolean(),
      'ind_cont': Yup.boolean(),
      'self_employed': Yup.boolean(),
      businesstype: atLeastOneOf("businesstype", ['nonprofit', 'vet_org', 'tribal', 'ind_cont', 'self_employed']),
    }),
    fields: [
      {
        id: 'nonprofit',
        displayName: 'Non-Profit'
      },
      {
        id: 'vet_org',
        displayName: 'Vet Org',
      },
      {
        id: 'tribal',
        displayName: 'Tribal',
      },
      {
        id: 'ind_cont',
        displayName: 'Independent Contractor',
      },
      {
        id: 'self_employed',
        displayName: 'Self-Employed',
      },
    ],
  }],
  [{
    type: 'text',
    question: 'What is the business\'s legal name?',
    id: 'business-legal-name',
    validation: Yup.object().shape({
      'business-legal-name': Yup.string().required('Required')
    }),
  }],
  [{
    type: 'radio',
    id: 'alternate-business-name',
    question: 'Do you conduct business under a different name than the legal one provided in question 2?',
    tooltip: ' Fill this out if your customers might know you by another name that you use for advertising. For example, "McDonald\'s" may be the trade name, but the legal name is "McDonald\'s Corporation."',
    validation: Yup.object().shape({
      'trade-name-yes': Yup.boolean(),
      'trade-name-no': Yup.boolean(),
      'trade-name': Yup.string().when('trade-name-yes', {
        is: true,
        then: Yup.string().required('Required'),
        otherwise: Yup.string().notRequired()
      }),
      'alternate-business-name': atLeastOneOf("alternate-business-name", ['trade-name-yes', 'trade-name-no'], "One must be selected"),
    }),
    fields: [
      {
        id: 'trade-name-yes',
        showCondition: () => true,
        displayName: 'Yes',
      },
      {
        id: 'trade-name-no',
        showCondition: () => true,
        onChangeHandler: (setFieldValue) => {
          setFieldValue('trade-name', undefined);
        },
        displayName: 'No',
      },
    ],
  }, {
    type: 'text',
    question: 'What trade name do you use for your business?',
    id: 'trade-name',
    showCondition: (values) => (values['trade-name-yes'] === true),
  }],
  [{
    type: 'address',
    question: 'Please enter the business address:',
    id: 'business-address',
    validation: addressSchema('business-address', 'business-address', false),
  }],
  [{
    type: 'text',
    question: 'What\'s your business phone number? (including area code)',
    id: 'business-phone',
    validation: Yup.object().shape({
      'business-phone': Yup.string().min(10, 'Please enter a valid phone number').required('Required')
    }),
  }],
  [{
    type: 'text',
    question: 'What\'s your business federal E.I.N., TIN, SSN?',
    tooltip: 'This is also known as your tax identification number and has the format (XX-XXXXXXX), or for individuals, your social security number.',
    id: 'federal-ein',
    validation: Yup.object().shape({ 'federal-ein': Yup.string().required('Required') }),
  }],
  [{
    type: 'text',
    question: 'Name of primary contact person for this application:',
    tooltip: 'Please provide the name of the person we should contact regarding updates and follow-up to this application.',
    id: 'primary-contact',
    validation: Yup.object().shape({
      'primary-contact': Yup.string().required('Required')
    }),
  }],
  [{
    type: 'text',
    question: 'Email address for the primary contact person:',
    tooltip: 'Please provide the name of the person we should contact regarding updates and follow-up to this application.',
    id: 'primary-email',
    validation: Yup.object().shape({
      'primary-email': Yup.string().email("Please provide a valid email").required('Required')
    }),
  }],
  [{
    type: 'currency',
    question: 'What\'s the average monthly payroll?',
    tooltip: (
      <div>
        What counts as payroll?
        <ul>
          <li style={tooltipList}>Salaries, wages, commissions, or tips (capped at $100,000 on an annualized basis for each employee)</li>
          <li style={tooltipList}>Employee benefits including costs for vacation, parental, family, medical, or sick leave</li>
          <li style={tooltipList}>Allowance for separation or dismissal, payments required for the provisions of group health care benefits including insurance premiums, and payment of any retirement benefit</li>
          <li style={tooltipList}>State and local taxes assessed on compensation</li>
          <li style={tooltipList}>For sole proprietor or independent contractors: wages, commisions, income, or net earnings from self-employment, capped at $100,000 on an annualized basis for each employee.</li>
        </ul>
      </div>
    ),
    id: 'average-monthly-payroll',
    validation: Yup.object().shape({
      'average-monthly-payroll': Yup.number().typeError("Must be a number").required('Required')
    }),
  }],
  [{
    type: 'currency',
    question: 'Loan Amount',
    id: 'loan-amount',
    readonly: true,
    computedValue: form => {
      return parseInt((parseFloat(form['average-monthly-payroll'] || 0) * 2.5 * 100).toString(), 10) / 100
    }
  }],
  [{
    type: 'text',
    question: 'How many jobs are included in this payroll?',
    id: 'number-jobs',
    validation: Yup.object().shape({
      'number-jobs': Yup.number().typeError("Must be a number").required('Required')
    }),
  }],
  [{
    type: 'checkbox',
    question: 'How will you use this loan?',
    tooltip: 'Please select all that apply.',
    id: 'loan-use',
    validation: Yup.object().shape({
      'loan-use-payroll': Yup.boolean(),
      'loan-use-rent': Yup.boolean(),
      'loan-use-utilities': Yup.boolean(),
      'loan-use-other': Yup.boolean(),
      "loan-use": atLeastOneOf("loan-use", ['loan-use-payroll', 'loan-use-rent', 'loan-use-utilities', 'loan-use-other']),
      'loan-use-other-string': Yup.string().when('loan-use-other', {
        is: true,
        then: Yup.string().required('Required'),
        otherwise: Yup.string().notRequired()
      }),
    }),
    fields: [
      {
        id: 'loan-use-payroll',
        displayName: 'Payroll'
      },
      {
        id: 'loan-use-rent',
        displayName: 'Rent / Mortgage Interest',
      },
      {
        id: 'loan-use-utilities',
        displayName: 'Utilities',
      },
      {
        id: 'loan-use-other',
        displayName: 'Other',
        onChangeHandler: (setFieldValue) => {
          setFieldValue('loan-use-other-string', undefined);
        },
      }
    ],
  }, {
    type: 'text',
    question: 'Explain:',
    id: 'loan-use-other-string',
    showCondition: (values) => (values['loan-use-other'] === true),
  }],

  [
    {
      type: 'growingtable',
      question:
        'Who are the owners of the business? Complete for each owner with more than 20% ownership stakes.',
      columns: [
        'Legal Name',
        'Title / Office',
        '% Owned',
        'TIN / EIN / SSN of owner',
        'Mailing Address',
        'City',
        'State',
        'Zip',
        'Email'
      ],
      columnIds: [
        'owner-legal-name',
        'owner-title',
        'owner-percentage',
        'owner-tin-ein-ssn',
        'owner-mailing-address-street',
        'owner-city',
        'owner-state',
        'owner-zip',
        'owner-email'
      ],
      id: 'owners',
      gridSize: new Array(9).fill(1),
      columnTypes: [
        'string',
        'string',
        'string',
        'string',
        'string',
        'string',
        'string',
        'string',
        'string'
      ],
      rowTitle: 'owner'
    }
  ],
  [{
    type: 'radio',
    id: 'suspended',
    question: 'Is the Business or any owner presently suspended, debarred, proposed for debarment, declared ineligible, voluntarily excluded from participation in this transaction by any Federal department or agency, or presently involved in any bankruptcy?',
    validation: Yup.object().shape({
      'suspended-yes': Yup.boolean().oneOf([false], "According to guidelines, the loan will NOT be approved if \"Yes\" is selected."),
      'suspended-no': Yup.boolean(),
      'suspended': atLeastOneOf("suspended", ['suspended-yes', 'suspended-no'], "One must be selected"),
    }),
    fields: [
      {
        id: 'suspended-yes',
        displayName: 'Yes',
      },
      {
        id: 'suspended-no',
        displayName: 'No'
      },
    ],
  }],
  [{
    type: 'radio',
    id: 'default',
    question: 'Has the Business, any of its owners, or any business owned or controlled by any of them, ever obtained a direct or guaranteed loan from SBA or any other Federal agency that is currently delinquent or has defaulted in the last 7 years and caused a loss to the government?',
    validation: Yup.object().shape({
      'default-yes': Yup.boolean().oneOf([false], "According to guidelines, the loan will NOT be approved if \"Yes\" is selected."),
      'default-no': Yup.boolean(),
      'default': atLeastOneOf("default", ['default-yes', 'default-no'], "One must be selected"),
    }),
    fields: [
      {
        id: 'default-yes',
        displayName: 'Yes',
      },
      {
        id: 'default-no',
        displayName: 'No'
      },
    ],
  }],
  [{
    type: 'radio',
    id: 'indictment',
    question: 'Is any owner presently subject to an indictment, criminal information, arraignment, or other means by which formal criminal charges are brought in any jurisdiction, or presently incarcerated, on probation or parole?',
    validation: Yup.object().shape({
      'indictment-yes': Yup.boolean().oneOf([false], "According to guidelines, the loan will NOT be approved if \"Yes\" is selected."),
      'indictment-no': Yup.boolean(),
      'indictment': atLeastOneOf("indictment", ['indictment-yes', 'indictment-no'], "One must be selected"),
    }),
    fields: [
      {
        id: 'indictment-yes',
        displayName: 'Yes',
      },
      {
        id: 'indictment-no',
        displayName: 'No'
      },
    ],
  }],
  [{
    type: 'radio',
    id: 'felony',
    question: 'Within the last 7 years, for any felony or misdemeanor for a crime against a minor, has any owner: 1) been convicted; 2) pleaded guilty; 3) pleaded nolo contendere; 4) been placed on pretrial diversion; or 5) been placed on any form of parole or probation (including probation before judgment)?',
    validation: Yup.object().shape({
      'felony-yes': Yup.boolean().oneOf([false], "According to guidelines, the loan will NOT be approved if \"Yes\" is selected."),
      'felony-no': Yup.boolean(),
      'felony': atLeastOneOf("felony", ['felony-yes', 'felony-no'], "One must be selected"),
    }),
    fields: [
      {
        id: 'felony-yes',
        displayName: 'Yes',
      },
      {
        id: 'felony-no',
        displayName: 'No'
      },
    ],
  }],
  [{
    type: 'radio',
    id: 'all-citizens',
    question: 'Are all owners U.S. citizens or have lawful permanent resident status?',
    validation: Yup.object().shape({
      'all-citizens-yes': Yup.boolean(),
      'all-citizens-no': Yup.boolean().oneOf([false], "According to guidelines, the loan will NOT be approved if \"No\" is selected."),
      'all-citizens': atLeastOneOf("all-citizens", ['all-citizens-yes', 'all-citizens-no'], "One must be selected"),
    }),
    fields: [
      {
        id: 'all-citizens-yes',
        displayName: 'Yes',
      },
      {
        id: 'all-citizens-no',
        displayName: 'No'
      },
    ],
  }, {
    type: 'checkbox',
    question: 'The owners in your business are/have:',
    tooltip: 'Please select all that apply.',
    showCondition: values => values["all-citizens-yes"] === true,
    id: 'citizens',
    validation: Yup.object().shape({
      'citizen': Yup.boolean(),
      'lawful-resident': Yup.boolean(),
      "citizens": atLeastOneOf("citizens", ['citizen', 'lawful-resident'])
    }),
    fields: [
      {
        id: 'citizen',
        displayName: 'U.S. Citizens'
      },
      {
        id: 'lawful-resident',
        displayName: 'Lawful Permanent Resident Status',
      }
    ],
  }],
  [{
    type: 'radio',
    id: 'affiliates',
    question: 'Is the Business or any owner an owner of any other business or have common management with any other business?',
    validation: Yup.object().shape({
      'affiliates-yes': Yup.boolean(),
      'affiliates-no': Yup.boolean(),
      'affiliates': atLeastOneOf("affiliates", ['affiliates-yes', 'affiliates-no'], "One must be selected"),
    }),
    fields: [
      {
        id: 'affiliates-yes',
        displayName: 'Yes',
      },
      {
        id: 'affiliates-no',
        displayName: 'No'
      },
    ],
  }, {
    type: 'textarea',
    question: 'Describe all Affiliates and describe the relationship:',
    id: 'affiliates-string',
    showCondition: (values) => (values['affiliates-yes'] === true),
  }],
  [{
    type: 'radio',
    id: 'sba-2020',
    question: 'Has the Business received an SBA Economic Injury Disaster Loan between January 31, 2020 and April 3, 2020?',
    validation: Yup.object().shape({
      'sba-2020-yes': Yup.boolean(),
      'sba-2020-no': Yup.boolean(),
      'sba-2020': atLeastOneOf("sba-2020", ['sba-2020-yes', 'sba-2020-no'], "One must be selected"),
    }),
    fields: [
      {
        id: 'sba-2020-yes',
        displayName: 'Yes',
      },
      {
        id: 'sba-2020-no',
        displayName: 'No'
      },
    ],
  }, {
    type: 'textarea',
    question: 'Please describe details:',
    id: 'sba-2020-string',
    showCondition: (values) => (values['sba-2020-yes'] === true),
  }],
];

export const TOTAL_PAGES = questions.length;

export default questions;
