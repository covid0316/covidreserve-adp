import { Question } from "../components/QuestionTypes";
import * as Yup from 'yup';

export const addressFieldsNoLease = (prefix: string) => [
  {
    type: 'text',
    formno: [5],
    question: 'Address: Number, Street, and / or Post Office Box',
    id: prefix + '-street',
  },
  {
    type: 'text',
    formno: [5],
    question: 'City',
    id: prefix + '-city',
  },
  {
    type: 'text',
    formno: [5],
    question: 'State',
    id: prefix + '-state',
  },
  {
    type: 'text',
    formno: [5],
    question: 'Zip',
    id: prefix + '-zip',
  },
  {
    type: 'text',
    formno: [5],
    question: 'County',
    id: prefix + '-county',
  },
] as Question[];

export const addressFields = (prefix) => {
  const arr = addressFieldsNoLease(prefix)
  const leased: Question = {
    type: 'radio',
    formno: [5],
    question: 'Is this property leased or owned?',
    fields: [
      {
        id: prefix + '-leased',
        displayName: 'Leased',
      },
      {
        id: prefix + '-owned',
        displayName: 'Owned',
      },
    ],
  };
  arr.push(leased);
  return arr;
}

export const addressSchema = (id: string, prefix: string, withLease: boolean) => {
  const leasedSchema = withLease ? {
    ['leased']: Yup.boolean().required('Required'),
    ['owned']: Yup.boolean().required('Required'),
  } : {};
  return Yup.object().shape({
    [prefix + '-street']: Yup.string().required('Address required'),
    [prefix + '-city']: Yup.string().required('City required'),
    [prefix + '-state']: Yup.string().required('State required'),
    [prefix + '-zip']: Yup.string().required('Zipcode required'),
    [prefix + '-county']: Yup.string().required('County required'),
    [id]: withLease ? Yup.boolean().test(id, "Please selected whether the property is leased or owned", function() {
      const p = this.parent;
      return Object.keys(p).some((f => !!p[f] && ['leased', 'owned'].includes(f)));
    }) : Yup.boolean(),
    ...leasedSchema
  });
}
