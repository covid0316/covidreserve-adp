import { RequestHandler, NextFunction, ParamsDictionary, Request, Response } from "express-serve-static-core";
import { TaskEither } from 'fp-ts/lib/TaskEither';
import Trouter from 'trouter';
import {Either} from 'fp-ts/lib/Either';
import * as E from 'fp-ts/lib/Either';
import { Db } from 'mongodb';

function onerror(err: any, req: Request, res: Response): any {
  res.statusCode = err.code || err.status || 500;
  res.end(err.message || res.statusCode.toString());
}

type ConnectRequestHandler = (req: Request, res: Response) => any
type ConnectErrorHandler = (err: any, req: Request, res: Response, next?: NextFunction) => any

/**
 * Quick and dirty rewrite of the next-connect module with static typing for
 * input and output types on request handlers.
 */
class Connect {
  private readonly router: Trouter<any> = new Trouter();
  constructor(
    private readonly onError: ConnectErrorHandler = onerror,
    private readonly onNoMatch: ConnectRequestHandler = onerror.bind(null, { code: 404, message: 'not found' }),
  ) { }

  apply(): RequestHandler {
    return (req: Request, res: Response) => new Promise((resolve) => this.handle(req, res, resolve));
  };

  handle(req: Request, res: Response, done: NextFunction): any {
    let i = 0;
    const { handlers } = this.find(req.method as Trouter.HTTPMethod, req.url);
    const next = async (err?: any) => {
      const handler = handlers[i];
      if (!handler) {
        if (done) done();
        else if (err) this.onError(err, req, res);
        else if (
          !err
          && (res.writableEnded === false || res.finished === false)
        ) {
          setImmediate(this.onNoMatch, req, res);
        }
        return;
      }
      try {
        if (!err) {
          i += 1;
          handler(req, res, next);
        } else this.onError(err, req, res, next);
      } catch (error) {
        next(error);
      }
    }
    next();
  };

  // middleware
  private mount(fn: RequestHandler | Connect): RequestHandler | Connect {
    return ((fn instanceof Connect || (fn as any).routes)
      ? (req, res, next) => setImmediate((fn as any).handle.bind(fn), req, res, next)
      : fn);
  }

  useForPattern<I, O>(pattern: string | RegExp, ...handlers: RequestHandler<ParamsDictionary, O, I>[]): Connect {
    this.router.use.call(this, pattern, ...handlers.map(this.mount));
    return this;
  }

  use<I, O>(...handlers: RequestHandler<ParamsDictionary, O, I>[]): Connect {
    this.router.use('*', handlers.map(this.mount));
    return this;
  }

  find(method: Trouter.HTTPMethod, url: string): Trouter.FindResult<any> {
    return this.router.find(method, url);
  }


  getForPattern<I, O>(pattern: string | RegExp, ...handlers: RequestHandler<ParamsDictionary, O, I>[]): Connect {
    this.router.add('GET', pattern, handlers);
    return this;
  }

  get<I, O>(...handlers: RequestHandler<ParamsDictionary, O, I>[]): Connect {
    this.router.add('GET', '*', handlers);
    return this;
  }

  headForPattern<I, O>(pattern: string | RegExp, ...handlers: RequestHandler<ParamsDictionary, O, I>[]): Connect {
    this.router.add('HEAD', pattern, handlers);
    return this;
  }

  head<I, O>(...handlers: RequestHandler<ParamsDictionary, O, I>[]): Connect {
    this.router.add('HEAD', '*', handlers);
    return this;
  }

  patchForPattern<I, O>(pattern: string | RegExp, ...handlers: RequestHandler<ParamsDictionary, O, I>[]): Connect {
    this.router.add('PATCH', pattern, handlers);
    return this;
  }

  patch<I, O>(...handlers: RequestHandler<ParamsDictionary, O, I>[]): Connect {
    this.router.add('PATCH', '*', handlers);
    return this;
  }

  optionsForPattern<I, O>(pattern: string | RegExp, ...handlers: RequestHandler<ParamsDictionary, O, I>[]): Connect {
    this.router.add('OPTIONS', pattern, handlers);
    return this;
  }

  options<I, O>(...handlers: RequestHandler<ParamsDictionary, O, I>[]): Connect {
    this.router.add('OPTIONS', '*', handlers);
    return this;
  }

  connectForPattern<I, O>(pattern: string | RegExp, ...handlers: RequestHandler<ParamsDictionary, O, I>[]): Connect {
    this.router.add('CONNECT', pattern, handlers);
    return this;
  }

  connect<I, O>(...handlers: RequestHandler<ParamsDictionary, O, I>[]): Connect {
    this.router.add('CONNECT', '*', handlers);
    return this;
  }

  deleteForPattern<I, O>(pattern: string | RegExp, ...handlers: RequestHandler<ParamsDictionary, O, I>[]): Connect {
    this.router.add('DELETE', pattern, handlers);
    return this;
  }

  delete<I, O>(...handlers: RequestHandler<ParamsDictionary, O, I>[]): Connect {
    this.router.add('DELETE', '*', handlers);
    return this;
  }

  traceForPattern<I, O>(pattern: string | RegExp, ...handlers: RequestHandler<ParamsDictionary, O, I>[]): Connect {
    this.router.add('TRACE', pattern, handlers);
    return this;
  }

  trace<I, O>(...handlers: RequestHandler<ParamsDictionary, O, I>[]): Connect {
    this.router.add('TRACE', '*', handlers);
    return this;
  }

  postForPattern<I, O>(pattern: string | RegExp, ...handlers: RequestHandler<ParamsDictionary, O, I>[]): Connect {
    this.router.add('POST', pattern, handlers);
    return this;
  }

  post<I, O>(...handlers: RequestHandler<ParamsDictionary, O, I>[]): Connect {
    this.router.add('POST', '*', handlers);
    return this;
  }

  putForPattern<I, O>(pattern: string | RegExp, ...handlers: RequestHandler<ParamsDictionary, O, I>[]): Connect {
    this.router.add('PUT', pattern, handlers);
    return this;
  }

  put<I, O>(...handlers: RequestHandler<ParamsDictionary, O, I>[]): Connect {
    this.router.add('PUT', '*', handlers);
    return this;
  }
}

type FpRequest<I, O> = Request<ParamsDictionary, O, I> & { user?: any, db?: Db };
type FpRequestHandler<I, O> = (req: FpRequest<I, O>, res: Response<O>, next?: NextFunction) => TaskEither<any, any>;
class FpConnect {
  private readonly underlying: Connect;
  constructor(
    private readonly onError: ConnectErrorHandler = onerror,
    private readonly onNoMatch: ConnectRequestHandler = onerror.bind(null, { code: 404, message: 'not found' }),
  ) {
    this.underlying = new Connect(onerror, onNoMatch);
  }

  apply(): RequestHandler {
    return this.underlying.apply();
  };

  handle(req: Request, res: Response, done: NextFunction): any {
    return this.underlying.handle(req, res, done);
  };

  private toFpHandler<I, O>(rh: FpRequestHandler<I, O>): RequestHandler<ParamsDictionary, O, I> {
    return (res, req, next) => { return rh(res, req, next)(); };
  }

  useForPattern<I, O>(pattern: string | RegExp, ...handlers: FpRequestHandler<I, O>[]): FpConnect {
    this.underlying.useForPattern(pattern, ...handlers.map(this.toFpHandler));
    return this;
  }

  useMiddleware(...handlers: any): FpConnect {
    this.underlying.use(...handlers);
    return this;
  }

  use<I, O>(...handlers: FpRequestHandler<I, O>[]): FpConnect {
    this.underlying.use(...handlers.map(this.toFpHandler));
    return this
  }

  find(method: Trouter.HTTPMethod, url: string): Trouter.FindResult<any> {
    return this.underlying.find(method, url);
  }


  getForPattern<I, O>(pattern: string | RegExp, ...handlers: FpRequestHandler<I, O>[]): FpConnect {
    this.underlying.getForPattern(pattern, ...handlers.map(this.toFpHandler));
    return this;
  }

  get<I, O>(...handlers: FpRequestHandler<I, O>[]): FpConnect {
    this.underlying.get(...handlers.map(this.toFpHandler));
    return this;
  }

  headForPattern<I, O>(pattern: string | RegExp, ...handlers: FpRequestHandler<I, O>[]): FpConnect {
    this.underlying.headForPattern(pattern, ...handlers.map(this.toFpHandler));
    return this;
  }

  head<I, O>(...handlers: FpRequestHandler<I, O>[]): FpConnect {
    this.underlying.head(...handlers.map(this.toFpHandler));
    return this;
  }

  patchForPattern<I, O>(pattern: string | RegExp, ...handlers: FpRequestHandler<I, O>[]): FpConnect {
    this.underlying.patchForPattern(pattern, ...handlers.map(this.toFpHandler));
    return this;
  }

  patch<I, O>(...handlers: FpRequestHandler<I, O>[]): FpConnect {
    this.underlying.patch(...handlers.map(this.toFpHandler));
    return this;
  }

  optionsForPattern<I, O>(pattern: string | RegExp, ...handlers: FpRequestHandler<I, O>[]): FpConnect {
    this.underlying.optionsForPattern(pattern, ...handlers.map(this.toFpHandler));
    return this;
  }

  options<I, O>(...handlers: FpRequestHandler<I, O>[]): FpConnect {
    this.underlying.options(...handlers.map(this.toFpHandler));
    return this;
  }

  connectForPattern<I, O>(pattern: string | RegExp, ...handlers: FpRequestHandler<I, O>[]): FpConnect {
    this.underlying.connectForPattern(pattern, ...handlers.map(this.toFpHandler));
    return this;
  }

  connect<I, O>(...handlers: FpRequestHandler<I, O>[]): FpConnect {
    this.underlying.connect(...handlers.map(this.toFpHandler));
    return this;
  }

  deleteForPattern<I, O>(pattern: string | RegExp, ...handlers: FpRequestHandler<I, O>[]): FpConnect {
    this.underlying.deleteForPattern(pattern, ...handlers.map(this.toFpHandler));
    return this;
  }

  delete<I, O>(...handlers: FpRequestHandler<I, O>[]): FpConnect {
    this.underlying.delete(...handlers.map(this.toFpHandler));
    return this;
  }

  traceForPattern<I, O>(pattern: string | RegExp, ...handlers: FpRequestHandler<I, O>[]): FpConnect {
    this.underlying.traceForPattern(pattern, ...handlers.map(this.toFpHandler));
    return this;
  }

  trace<I, O>(...handlers: FpRequestHandler<I, O>[]): FpConnect {
    this.underlying.trace(...handlers.map(this.toFpHandler));
    return this;
  }

  postForPattern<I, O>(pattern: string | RegExp, ...handlers: FpRequestHandler<I, O>[]): FpConnect {
    this.underlying.postForPattern(pattern, ...handlers.map(this.toFpHandler));
    return this;
  }

  post<I, O>(...handlers: FpRequestHandler<I, O>[]): FpConnect {
    this.underlying.post(...handlers.map(this.toFpHandler));
    return this;
  }

  putForPattern<I, O>(pattern: string | RegExp, ...handlers: FpRequestHandler<I, O>[]): FpConnect {
    this.underlying.putForPattern(pattern, ...handlers.map(this.toFpHandler));
    return this;
  }

  put<I, O>(...handlers: FpRequestHandler<I, O>[]): FpConnect {
    this.underlying.put(...handlers.map(this.toFpHandler));
    return this;
  }
}

type NotAuthorized = {
  notAuthorized: true,
  message: string
};
function authenticate<I, O>(req: FpRequest<I, O>): Either<NotAuthorized, undefined> {
  return req.user ? E.right(undefined) : E.left({
    notAuthorized: true,
    message: "Not authorized"
  });
}

export { FpConnect, Connect, authenticate }