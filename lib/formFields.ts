import insuranceFields, { insuranceSchema } from './insuranceFields';
import { ownerFields, businessOwnerFields, ownerSchema } from './ownerFields';
import { Question } from '../components/QuestionTypes';
import * as Yup from 'yup';
import { addressSchema } from './addressFields';

function atLeastOneOf(id: string, list: string[]) {
  return Yup.boolean().test(id, "At least one option should be selected", function() {
    const p = this.parent;
    const result = Object.keys(p).some((f => {
      return !!p[f] && list.includes(f)
    }));
    return result;
  });
}

function radioSet(prefix: string) {
  const id = prefix + '-id';
  const list = [prefix + '-yes', prefix + '-no'];
  const radioSchemas = list.reduce((obj, v) => ({ ...obj, [v]: Yup.boolean().required('Required')}), {});
  return Yup.object().shape({
    ...radioSchemas,
    [id]: atLeastOneOf(id, list),
    [prefix + '-string']: Yup.string().when(list[0], {
      is: true,
      then: Yup.string().required('Required'),
      otherwise: Yup.string()
    }),
  });
}

function getConditionalTextBox(id: string, when: string) {
  return Yup.string().when(when, {
    is: true,
    then: Yup.string().required('Required'),
    otherwise: Yup.string(),
  });
}

// Schema: export[FORM WIZARD PAGE NUMBER][QUESTION NUMBER][ANSWER OPTIONS FOR CHECKBOX/RADIO]
const questions: Array<Question[]> = [
  [{
    type: 'checkbox',
    formno: [5], // which form(s) does this question belong to?
    question: 'What damage are you applying for?',
    id: 'damagetype',
    validation: Yup.object().shape({
      'real-property': Yup.boolean().required('Required'),
      'physical-damage': Yup.boolean().required('Required'),
      'business-contents': Yup.boolean().required('Required'),
      'economic-injury': Yup.boolean().required('Required'),
      damagetype: atLeastOneOf("damagetype", ['real-property', 'physical-damage', 'business-contents', 'economic-injury']),
    }),
    tooltip:
      'Most businesses impacted by covid will be applying for Economic Injury (EIDL)',
    fields: [
      {
        id: 'physical-damage',
        showCondition: () => true,
        displayName: 'Physical Damage',
        onChangeHandler: (setFieldValue, e) => {
          setFieldValue('physical-damage', e.target.checked);
          if (e.target.checked === false) {
            setFieldValue('real-property', false);
            setFieldValue('business-contents', false);
          }
        },
      },
      {
        id: 'real-property',
        showCondition: (field) => (field || {})['physical-damage'] === true,
        displayName: 'Real Property',
      },
      {
        id: 'business-contents',
        showCondition: (field) => (field || {})['physical-damage'] === true,
        displayName: 'Business Contents',
      },
      {
        id: 'economic-injury',
        showCondition: () => true,
        displayName: 'Economic Injury (EIDL)',
      },
    ],
  }],
  [{
    type: 'radio',
    formno: [5],
    question: 'What is the organization type of your business?',
    tooltip: 'Note: we do not currently support the sole proprietor application',
    id: 'organization-type',
    other: 'organization-type-other',
    validation: Yup.object().shape({
      'partnership': Yup.boolean().required('Required'),
      'limited-partnership': Yup.boolean().required('Required'),
      'limited-liability-entity': Yup.boolean().required('Required'),
      'corporation': Yup.boolean().required('Required'),
      'nonprofit-organization': Yup.boolean().required('Required'),
      'trust': Yup.boolean().required('Required'),
      'organization-type-other': Yup.boolean().required('Required'),
      'organization-type-other-string': Yup.string().when('organization-type-other', {
        is: true,
        then: Yup.string().min(5).required('Required'),
        otherwise: Yup.string(),
      }),
      'organization-type': atLeastOneOf('organization-type', ['partnership', 'limited-partnership', 'limited-liability-entity', 'corporation', 'nonprofit-organization', 'trust', 'organization-type-other'])
    }),
    fields: [
      {
        id: 'partnership',
        showCondition: () => true,
        displayName: 'Partnership',
      }, {
        id: 'limited-partnership',
        showCondition: () => true,
        displayName: 'Limited Partnership',
      }, {
        id: 'limited-liability-entity',
        showCondition: () => true,
        displayName: 'Limited Liability Entity',
      }, {
        id: 'corporation',
        showCondition: () => true,
        displayName: 'Corporation',
      }, {
        id: 'nonprofit-organization',
        showCondition: () => true,
        displayName: 'Nonprofit Organization',
      }, {
        id: 'trust',
        showCondition: () => true,
        displayName: 'Trust',
      },
    ],
  }],
[  {
    type: 'text',
    formno: [5],
    question: 'What is the legal name of your business?',
    tooltip: 'This is the name you incorporated your business as',
    id: 'business-legal-name',
    validation: Yup.object().shape({
      'business-legal-name': Yup.string().required('Required')
    }),
  },],
  [{
    type: 'text',
    formno: [5],
    question: 'What\'s your business federal E.I.N. (if applicable)',
    tooltip: 'This is also known as your tax identification number and has the format (XX-XXXXXXX).',
    id: 'federal-ein',
    validation: Yup.object().shape({ 'federal-ein': Yup.string().length(10, "Must be ${length} characters").required('Required') }),
  }],
  [{
    type: 'radio',
    formno: [5],
    id: 'alternate-business-name',
    question: 'Do you conduct business under a different name than the legal one provided previously?',
    tooltip: ' Fill this out if your customers might know you by another name that you use for advertising. For example, “McDonald\'s” may be the trade name, but the legal name is “McDonald\'s Corporation.”',
    validation: Yup.object().shape({
      'trade-name-yes': Yup.boolean().required('Required'),
      'trade-name-no': Yup.boolean().required('Required'),
      'trade-name': Yup.string().when('trade-name-yes', {
        is: true,
        then: Yup.string().required('Required'),
        otherwise: Yup.string().notRequired()
      }),
    }),
    fields: [
      {
        id: 'trade-name-yes',
        showCondition: () => true,
        displayName: 'Yes',
      },
      {
        id: 'trade-name-no',
        showCondition: () => true,
        onChangeHandler: (setFieldValue) => {
          setFieldValue('trade-name', undefined);
        },
        displayName: 'No',
      },
    ],
  }, {
    type: 'text',
    formno: [5],
    question: 'What trade name do you use for your business?',
    id: 'trade-name',
    showCondition: (values) => (values['trade-name-yes'] === true),
  }],
  [{
    type: 'text',
    formno: [5],
    question: 'What\'s your business phone number? (including area code)',
    id: 'business-phone',
    validation: Yup.object().shape({
      'business-phone': Yup.string().min(10, 'Please enter a valid phone number').required('Required')
    }),
  }],
  [{
    type: 'radio',
    formno: [5], // which form(s) does this question belong to?
    question: 'What mailing address would you like to use?',
    other: 'mailing-other',
    id: 'mailing-type',
    validation: Yup.object().shape({
      'mailing-business': Yup.boolean().required('Required'),
      'mailing-home': Yup.boolean().required('Required'),
      'mailing-temp': Yup.boolean().required('Required'),
      'mailing-other': Yup.boolean().required('Required'),
      'mailing-type': atLeastOneOf('mailing-type', ['mailing-business', 'mailing-home', 'mailing-temp', 'mailing-other']),
      'mailing-other-string': Yup.string().when('mailing-other', {
        is: true,
        then: Yup.string().required('Required'),
        otherwise: Yup.string().notRequired()
      }),
    }).required('Required'),
    fields: [
      {
        id: 'mailing-business',
        displayName: 'Business',
      },
      {
        id: 'mailing-home',
        displayName: 'Home',
      },
      {
        id: 'mailing-temp',
        displayName: 'Temporary',
      },
    ],
  },
  {
    type: 'address',
    formno: [5],
    question: 'Please enter the address name below:',
    id: 'mailing-address',
    validation: addressSchema('mailing-address', 'mailing-address', false),
  },
  {
    type: 'radio',
    formno: [5],
    question: 'Has this property address been damaged?',
    tooltip: 'This can be economic or property damage',
    id: 'mailing-address-damage-id',
    validation: Yup.object().shape({
      'mailing-address-is-damaged': Yup.boolean().required('Required'),
      'mailing-address-not-damaged': Yup.boolean().required('Required'),
      'mailing-address-damage-id': atLeastOneOf('mailing-address-damage-id', ['mailing-address-is-damaged', 'mailing-address-not-damaged']),
    }),
    fields: [
      {
        id: 'mailing-address-is-damaged',
        displayName: 'Yes',
      },
      {
        id: 'mailing-address-not-damaged',
        displayName: 'No',
        onChangeHandler: (setFieldValue) => {
          setFieldValue('mailing-address-leased', false);
          setFieldValue('mailing-address-owned', false);
        },
      },
    ],
  },
  {
    type: 'radio',
    formno: [5],
    question: 'Is this property leased or owned?',
    id: 'mailing-address-leased-id',
    validation: Yup.object().shape({
      'mailing-address-leased': Yup.boolean(),
      'mailing-address-owned': Yup.boolean(),
      'mailing-address-leased-id': Yup.boolean().when('mailing-address-is-damaged', {
        is: true,
        then: atLeastOneOf('mailing-address-leased-id', ['mailing-address-leased', 'mailing-address-owned']),
        otherwise: Yup.boolean(),
      }),
    }),
    showCondition: (values) => values['mailing-address-is-damaged'] === true,
    fields: [
      {
        id: 'mailing-address-leased',
        displayName: 'Leased',
      },
      {
        id: 'mailing-address-owned',
        displayName: 'Owned',
      },
    ],
  }],
  [{
    type: 'radio',
    formno: [5],
    question: 'Do you have additional property addresses that have been damaged?',
    tooltip: 'This can be economic or property damage',
    id: 'multiple-damaged-properties-id',
    validation: Yup.object().shape({
      'multiple-damaged-properties-yes': Yup.boolean().required('Required'),
      'multiple-damaged-properties-no': Yup.boolean().required('Required'),
      'multiple-damaged-properties-id': atLeastOneOf('multiple-damaged-properties-id', ['multiple-damaged-properties-yes', 'multiple-damaged-properties-no'])
    }),
    // showCondition: (values) => values['mailing-address-is-damaged'] === true,
    fields: [
      {
        id: 'multiple-damaged-properties-yes',
        displayName: 'Yes',
      },
      {
        id: 'multiple-damaged-properties-no',
        displayName: 'No',
      },
    ],
  },
  {
    type: 'multi',
    id: 'damaged-property',
    initialCount: 1,
    validation: Yup.object().shape({
      'damaged-property': Yup.array().when('multiple-damaged-properties-yes', {
        is: true,
        then: Yup.array().of(addressSchema('damaged-property', 'damaged-property', true)).min(1, 'You must enter at least 1 damaged property').required('Required'),
        otherwise: Yup.array()
      })
    }),
    fieldFunction: function (num: number) {
      return [
      {
        id: 'damaged-property',
        type: 'address',
        question: 'Please enter the address below:'
      },
      {
        type: 'radio',
        id: 'damaged-property',
        formno: [5],
        question: 'Is this property leased or owned?',
        fields: [
          {
            id: 'leased',
            displayName: 'Leased',
          },
          {
            id: 'owned',
            displayName: 'Owned',
          },
        ],
    }] as Question[]},
    // fieldFunction: addressFields.bind(this, 'damaged-property'),
    formno: [5],
    question: null,
    addLabel: 'Add another address',
    showCondition: (values) => (values['multiple-damaged-properties-yes'] === true || values['mailing-address-not-damaged'] === true),
  }],
  [{
    type: 'none',
    formno: [5],
    question: 'Who is the person to contact for Loss Verification Inspection?',
    tooltip: 'This is the person whom SBA should contact regarding any on-site inspections, telephone interviews, etc. to verify the extent of damages.',
  },
  {
    type: 'text',
    formno: [5],
    question: 'Name:',
    validation: Yup.object().shape({
      'loss-verification-inspection-name': Yup.string().required('Required'),
      'loss-verification-inspection-tel': Yup.string().required('Required')
    }),
    id: 'loss-verification-inspection-name'
  }, {
    type: 'text',
    formno: [5],
    question: 'Telephone Number',
    id: 'loss-verification-inspection-tel'
  }],
  [{
    type: 'none',
    formno: [5],
    question: 'Who is the person to contact for information necessary to process the application?',
  },
  {
    type: 'text',
    formno: [5],
    question: 'Name:',
    validation: Yup.object().shape({
      'application-process-poc-name': Yup.string().required('Required'),
      'application-process-poc-tel': Yup.string().required('Required')
    }),
    id: 'application-process-poc-name'
  }, {
    type: 'text',
    formno: [5],
    question: 'Telephone Number',
    id: 'application-process-poc-tel'
  }],
  [{
    type: 'checkbox',
    formno: [5], // which form(s) does this question belong to?
    question: 'Please provide ways to contact you:',
    id: 'alternate-way-to-contact',
    validation: Yup.object().shape({
      'alternate-way-to-contact-phone': Yup.boolean().required('Required'),
      'alternate-way-to-contact-email': Yup.boolean().required('Required'),
      'alternate-way-to-contact-fax': Yup.boolean().required('Required'),
      'alternate-way-to-contact-other': Yup.mixed(),
      'alternate-way-to-contact': atLeastOneOf('alternate-way-to-contact',
        ['alternate-way-to-contact-phone', 'alternate-way-to-contact-email', 'alternate-way-to-contact-fax', 'alternate-way-to-contact-other']
      ),
      'alternate-way-to-contact-phone-string': Yup.string().when('alternate-way-to-contact-phone', {
        is: true,
        then: Yup.string().required('Required'),
        otherwise: Yup.string()
      }),
      'alternate-way-to-contact-email-string': Yup.string().when('alternate-way-to-contact-email', {
        is: true,
        then: Yup.string().required('Required'),
        otherwise: Yup.string()
      }),
      'alternate-way-to-contact-fax-string': Yup.string().when('alternate-way-to-contact-fax', {
        is: true,
        then: Yup.string().required('Required'),
        otherwise: Yup.string()
      }),
      'alternate-way-to-contact-other-string': Yup.string().when('alternate-way-to-contact-other', {
        is: (val) => !!val, // truthy
        then: Yup.string().required('Required'),
        otherwise: Yup.string(),
      }),
    }),
    other: 'alternate-way-to-contact-other',
    fields: [
      {
        id: 'alternate-way-to-contact-phone',
        displayName: 'Cell Phone',
      },
      {
        id: 'alternate-way-to-contact-email',
        displayName: 'Email',
      },
      {
        id: 'alternate-way-to-contact-fax',
        displayName: 'Fax',
      },
    ],
  },
  {
    type: 'text',
    formno: [5],
    id: 'alternate-way-to-contact-phone-string',
    question: 'Enter Cell Phone',
    showCondition: (values) => values['alternate-way-to-contact-phone']
  },
  {
    type: 'text',
    formno: [5],
    id: 'alternate-way-to-contact-email-string',
    question: 'Enter Email',
    showCondition: (values) => values['alternate-way-to-contact-email']
  },
  {
    type: 'text',
    formno: [5],
    id: 'alternate-way-to-contact-fax-string',
    question: 'Enter Fax',
    showCondition: (values) => values['alternate-way-to-contact-fax']
  },
  {
    type: 'text',
    formno: [5],
    id: 'alternate-way-to-contact-other-string',
    question: 'Other',
    showCondition: (values) => values['alternate-way-to-contact-other']
  }],
  [{
    type: 'text',
    formno: [5],
    id: 'business-activity',
    validation: Yup.object().shape({
      'business-activity': Yup.string().required('Required'),
    }),
    question: 'What is your primary business activity?',
    tooltip: '(e.g. Advertising, Restaurants / Food, Florist)'
  }],
  [{
    type: 'text',
    formno: [5],
    validation: Yup.object().shape({
      'employees-pre-disaster': Yup.number().required('Required'),
    }),
    id: 'employees-pre-disaster',
    question: 'How many employees did you have (pre-disaster)?',
  }],
  [{
    type: 'text', // @todo: use datepicker
    formno: [5],
    id: 'established-date',
    validation: Yup.object().shape({
      'established-date': Yup.string().required('Required'),
    }),
    question: 'What date was your business established?',
    tooltip: 'Enter the establishment date of your business. (MM-DD-YYYY)'
  }],
  [{
    type: 'text', // @todo: use datepicker
    formno: [5],
    id: 'management-start-date',
    validation: Yup.object().shape({
      'management-start-date': Yup.string().required('Required'),
    }),
    question: 'Your current management has been in place since:',
    tooltip: 'Enter the start date of the current management. (MM-DD-YYYY)'
  }],
  [{
    type: 'checkbox',
    formno: [5], // which form(s) does this question belong to?
    question: 'Please enter the estimated loss amounts (if unknown, enter a question mark):',
    id: 'estimated-loss',
    validation: Yup.object().shape({
      'estimated-loss-real-estate': Yup.boolean().required('Required'),
      'estimated-loss-inventory': Yup.boolean().required('Required'),
      'estimated-loss-m-e': Yup.boolean().required('Required'),
      'estimated-loss-leasehold': Yup.boolean().required('Required'),
      'estimated-loss-real-estate-string': getConditionalTextBox('estimated-loss-real-estate-string', 'estimated-loss-real-estate'),
      'estimated-loss-inventory-string': getConditionalTextBox('estimated-loss-inventory-string', 'estimated-loss-invetory'),
      'estimated-loss-m-e-string': getConditionalTextBox('estimated-loss-m-e-string', 'estimated-loss-m-e'),
      'estimated-loss-leasehold-string': getConditionalTextBox('estimated-loss-leasehold-string', 'estimated-loss-leasehold'),
    }),
    tooltip: 'Best provide your best estimate.',
    fields: [
      {
        id: 'estimated-loss-real-estate',
        displayName: 'Real Estate',
      },
      {
        id: 'estimated-loss-inventory',
        displayName: 'Inventory',
      },
      {
        id: 'estimated-loss-m-e',
        displayName: 'Machinery & Equipment',
      },
      {
        id: 'estimated-loss-leasehold',
        displayName: 'Leasehold Improvements',
      },
    ],
  },
  {
    type: 'text',
    formno: [5],
    id: 'estimated-loss-real-estate-string',
    question: 'Estimate Real Estate Loss:',
    showCondition: (values) => values['estimated-loss-real-estate']
  },
  {
    type: 'text',
    formno: [5],
    id: 'estimated-loss-inventory-string',
    question: 'Estimate Invetory Loss:',
    showCondition: (values) => values['estimated-loss-inventory']
  },
  {
    type: 'text',
    formno: [5],
    id: 'estimated-loss-m-e-string',
    question: 'Estimate Machinery & Equipment Loss:',
    showCondition: (values) => values['estimated-loss-m-e']
  },
  {
    type: 'text',
    formno: [5],
    id: 'estimated-loss-leasehold-string',
    question: 'Estimate Leasehold Loss:',
    showCondition: (values) => values['estimated-loss-leasehold']
  }],
  [{
    type: 'radio',
    formno: [5],
    question: 'Do you have insurance coverage on your business?',
    id: 'insurance-id',
    validation: Yup.object().shape({
      'insurance-coverage-yes': Yup.boolean().required('Required'),
      'insurance-coverage-no': Yup.boolean().required('Required'),
      'insurance-id': atLeastOneOf('insurance-id', ['insurance-coverage-yes', 'insurance-coverage-no']),
    }),
    fields: [
      {
        id: 'insurance-coverage-yes',
        displayName: 'Yes',
      },
      {
        id: 'insurance-coverage-no',
        displayName: 'No',
        onChangeHandler: (setFieldValue) => { setFieldValue('insurance', []) }
      },
    ],
  },
  {
    type: 'multi',
    id: 'insurance',
    initialCount: 1,
    validation: Yup.object().shape({ 'insurance': Yup.array().when('insurance-coverage-yes', {
      is: true,
      then: Yup.array().of(insuranceSchema('insurance')).min(1, 'You must add at least ${min} insurance policy').required('Required'),
      otherwise: Yup.array()
    }) }),
    fieldFunction: insuranceFields.bind(this, 'insurance'),
    showCondition: (values) => !!values['insurance-coverage-yes'],
    formno: [5],
    addLabel: 'Add another insurance',
    question: 'Please provide some information about your insurance coverage:',
  }],
  [{
    type: 'none',
    formno: [5],
    question: 'Who are the owners of the business? Complete for each: 1) proprietor, or 2) limited partner who owns 20% or more interest and each general partner, or 3) stockholder or entity owning 20% or more voting stock.',
  },
  {
    type: 'multi',
    id: 'owner',
    initialCount: 0,
    formno: [5],
    question: 'Individual Owners',
    addLabel: 'Add another owner',
    validation: ownerSchema('owner', ownerFields('owner')),
    fieldFunction: ownerFields.bind(this, 'owner'),
  },
  {
    type: 'multi',
    id: 'business-owner',
    initialCount: 0,
    formno: [5],
    question: 'Business Entity Owners',
    addLabel: 'Add another business entity owner',
    validation: ownerSchema('business-owner', businessOwnerFields('business-owner')),
    fieldFunction: businessOwnerFields.bind(this, 'business-owner'),
  }],
  [{
    type: 'none',
    formno: [5],
    question: `Answer 'Yes' or 'No' for the following questions. Please provide additional details for any questions that have a 'Yes' response`
  },
  {
    type: 'radio',
    formno: [5],
    question: 'Has the business or a listed owner ever been involved in a bankruptcy or insolvency proceeding?',
    id: 'bankruptcy-id',
    validation: radioSet('bankruptcy'),
    fields: [
      {
        id: 'bankruptcy-yes',
        displayName: 'Yes',
      },
      {
        id: 'bankruptcy-no',
        displayName: 'No',
      },
    ],
  },
  {
    type: 'text',
    formno: [5],
    question: 'Please provide dates and details',
    id: 'bankruptcy-string',
    showCondition: (values) => values['bankruptcy-yes']
  },
  {
    type: 'radio',
    formno: [5],
    validation: radioSet('lawsuits'),
    id: 'lawsuits-id',
    question: 'Does the business or a listed owner have any outstanding judgements, tax liens, or pending lawsuits against them?',
    fields: [
      {
        id: 'lawsuits-yes',
        displayName: 'Yes',
      },
      {
        id: 'lawsuits-no',
        displayName: 'No',
      },
    ],
  },
  {
    type: 'text',
    formno: [5],
    question: 'Please provide dates and details',
    id: 'lawsuits-string',
    showCondition: (values) => values['lawsuits-yes']
  }],
  [{
    type: 'radio',
    formno: [5],
    id: 'delinquent-id',
    validation: radioSet('delinquent'),
    question: 'Is the business or a listed owner delinquent on any Federal taxes, direct or guaranteed Federal loans (SBA, FHA, VA, student, etc.), Federal contracts, Federal grants, or any child support payments?',
    fields: [
      {
        id: 'delinquent-yes',
        displayName: 'Yes',
      },
      {
        id: 'delinquent-no',
        displayName: 'No',
      },
    ],
  },
  {
    type: 'text',
    formno: [5],
    question: 'Please provide dates and details',
    id: 'delinquent-string',
    showCondition: (values) => values['delinquent-yes']
  }],
  [{
    type: 'radio',
    formno: [5],
    id: 'member-id',
    validation: radioSet('member'),
    question: `Does any owner, owner's spouse, or household member work for SBA or serve as a member of SBA's SCORE, ACE, or Advisory Council?`,
    fields: [
      {
        id: 'member-yes',
        displayName: 'Yes',
      },
      {
        id: 'member-no',
        displayName: 'No',
      },
    ],
  },
  {
    type: 'text',
    formno: [5],
    question: 'Please provide dates and details',
    id: 'member-string',
    showCondition: (values) => values['member-yes']
  }],
  [{
    type: 'radio',
    formno: [5],
    id: 'suspended-id',
    validation: radioSet('suspended'),
    question: 'Is the applicant or any listed owner currently suspended or debarred from contracting with the Federal government or receiving Federal grants or loans?',
    fields: [
      {
        id: 'suspended-yes',
        displayName: 'Yes',
      },
      {
        id: 'suspended-no',
        displayName: 'No',
      },
    ],
  },
  {
    type: 'text',
    formno: [5],
    question: 'Please provide dates and details',
    id: 'suspended-string',
    showCondition: (values) => values['suspended-yes']
  }],
  [{
    type: 'radio',
    formno: [5],
    id: 'indictment-id',
    validation: radioSet('indictment'),
    question: `Are any you or any of the joint applicants you listed as owners: a) presently subject to an indictment, criminal information, arraignment, or other means by which formal criminal charges are brought in any juristiction; b) been arrested in the past six months for any criminal offense c) for any criminal offense - other than a minor vehicle violation - have you ever: 1) been convicted, 2) plead guilty, 3) plead nolo contendere, 4) been placed on pretrial diversion, or 5) been placed on any form of parole or probation (including probation before judgement)?`,
    fields: [
      {
        id: 'indictment-yes',
        displayName: 'Yes',
      },
      {
        id: 'indictment-no',
        displayName: 'No',
      },
    ],
  },
  {
    type: 'text',
    formno: [5],
    question: 'Please provide name(s)',
    id: 'indictment-name',
    showCondition: (values) => values['indictment-yes']
  },
  {
    type: 'text',
    formno: [5],
    question: 'Please provide dates and details',
    id: 'indictment-string',
    showCondition: (values) => values['indictment-yes']
  }],
  [{
    type: 'radio',
    formno: [5],
    id: 'convicted-id',
    validation: radioSet('convicted'),
    question: `In the past year, has the business or a listed owner been convicted of a criminal offense committed during and in
    connection with a riot or civil disorder or other declared disaster, or ever been engaged in the production or distribution of any
    product or service that has been determined to be obscene by a court of competent jurisdiction?`,
    fields: [
      {
        id: 'convicted-yes',
        displayName: 'Yes',
      },
      {
        id: 'convicted-no',
        displayName: 'No',
      },
    ],
  },
  {
    type: 'text',
    formno: [5],
    question: 'Please provide dates and details',
    id: 'convicted-string',
    showCondition: (values) => values['convicted-yes']
  }],
  [{
    type: 'radio',
    formno: [5],
    id: 'federal-loan-id',
    validation: radioSet('federal-loan'),
    question: `Has the business or a listed owner ever had or guaranteed a Federal loan or a Federally guaranteed loan?`,
    fields: [
      {
        id: 'federal-loan-yes',
        displayName: 'Yes',
      },
      {
        id: 'federal-loan-no',
        displayName: 'No',
      },
    ],
  },
  {
    type: 'text',
    formno: [5],
    question: 'Please provide dates and details',
    id: 'federal-loan-string',
    showCondition: (values) => values['federal-loan-yes']
  }],
  [{
    type: 'checkbox',
    formno: [5],
    validation: Yup.object().shape({
      'additional-funds': Yup.boolean().required('Required')
    }),
    question: `For Physical Damage Loans only: If your application is approved, you may be eligible for additional funds to cover the cost of mitigating measures (real property improvements or devices to minimize or protect against future damage from the same type of diaster event). It is not necessary for you to submit the descirption and cost estimates with the application. SBA must approve the mitigating measures before any loan increase. Please check here if you're interested in having SBA consider this increase. `,
    tooltip: 'If you had physical damage to your property, checking this box may let the SBA increase the size of your loan so that you can cover the cost of making property or device improvement from preventing a similar event from occuring in the future.',
    fields: [
      {
        id: 'additional-funds',
        displayName: `I'm interested`,
      },
    ],
  }]
];

export const TOTAL_PAGES = questions.length;

export default questions;
