import { Question } from "../components/QuestionTypes";
import * as Yup from 'yup';

const insuranceFields = (prefix: string) =>
  [
    {
      type: 'text',
      formno: [5],
      question: 'Coverage Type',
      id: prefix + '-coverage-type'
    },
    {
      type: 'text',
      formno: [5],
      question: 'Name of Insurance Company and Agent',
      id: prefix + '-coverage-name-and-agent'
    },
    {
      type: 'text',
      formno: [5],
      question: 'Phone Number of Insurance Agent',
      id: prefix + '-agent-phone'
    },
    {
      type: 'text',
      formno: [5],
      question: 'Policy Number',
      id: prefix + '-policy-number'
    }
  ] as Question[];
export default insuranceFields;

export const insuranceSchema = (prefix: string) => {
  return Yup.object().shape({
    [prefix + '-coverage-type']: Yup.string().required('Required'),
    [prefix + '-coverage-name-and-agent']: Yup.string().required('Required'),
    [prefix + '-agent-phone']: Yup.string().required('Required'),
    [prefix + '-agent-phone']: Yup.string().required('Required'),
    [prefix + '-policy-number']: Yup.string().required('Required')
  });
}
