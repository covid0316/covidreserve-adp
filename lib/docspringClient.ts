
import Docspring from 'docspring';
import { TaskEither } from 'fp-ts/lib/TaskEither';
import { fromPromise } from './util';

type GeneratePdfResponse = {
  url: string
};

type DataRequest = {
  email: string,
  name?: string,
  fields?: string[],
  auth_type: 'password' | 'oauth' | 'email_link' | 'phone_number' | 'ldap' | 'saml',
  auth_session_started_at: string,
  auth_second_factor_type?: 'none' | 'phone_number' | 'totp' | 'mobile_push' | 'security_key' | 'fingerprint',
  auth_provider?: string,
  auth_session_id_hash?: string,
  auth_user_id_hash?: string,
  auth_username_hash?: string,
  auth_phone_number_hash?: string
};

// This is the type that generatePDF accepts, it's probably
// incomplete though.
type SubmissionData = {
  editable?: boolean,
  data: any, // should match schema of the template
  metadata?: any, // passthrough fields for webhook
  field_overrides?: any,
  wait?: boolean,
  data_requests?: Array<DataRequest>
};

export type DataRequestToken = {
  request_id: string,
  id: string,
  secret: string,
  expires_at: string,
  data_request_url: string
};

type DataRequestId = {
  id: string
};

class DocspringClient {
  private readonly client: any;
  constructor(
    apiTokenId: string,
    apiTokenSecret: string
  ) {
    const config = new Docspring.Configuration();
    config.apiTokenId = apiTokenId;
    config.apiTokenSecret = apiTokenSecret;
    this.client = new Docspring.Client(config);
  }

  // TODO: session start time and hashed credentials for better auditing
  generateDataRequest(templateId: string, email: string, submissionData: any, userFields?: string[]): Promise<DataRequestId> {
    return new Promise((resolve, reject) =>
      this.client.generatePDF(
        templateId,
        { data: submissionData, data_requests: [{
          email,
          auth_type: 'password',
          auth_session_started_at: new Date().toISOString(),
          fields: userFields
        }]} as SubmissionData,
        (error: any, response: any) => {
          if (error) reject(error);
          else {
            resolve({
              id: response.submission.data_requests[0].id
            })
          }
        }
      )
    );
  }

  generateGenericDataRequest(dataRequests: any[], templateId: string, email: string, submissionData: any): Promise<DataRequestId[]> {
    return new Promise((resolve, reject) =>
      this.client.generatePDF(
        templateId,
        { data: submissionData, data_requests: dataRequests } as SubmissionData,
        (error: any, response: any) => {
          if (error) reject(error);
          else {
            resolve(response.submission.data_requests.map(request => ({
              ids: request.id
            })));
          }
        }
      )
    );
  }

  getSubmission(submissionId: string): Promise<GeneratePdfResponse> {
    return new Promise((resolve, reject) => 
      this.client.getSubmission(
        submissionId,
        { includeDate: false },
        (error: any, response: any) => {
          if (error) reject(error);
          else {
            resolve({ url: response.download_url });
          }
        }));
  }

  generatePdf(templateId: string, submissionData: any): Promise<GeneratePdfResponse> {
    return new Promise((resolve, reject) =>
      this.client.generatePDF(
        templateId,
        { data: submissionData } as SubmissionData,
        (error: any, response: any) => {
          if (error) reject(error);
          else {
            resolve({
              url: response.submission.download_url
            });
          }
        }));
  }

  createDataRequestToken(id: string): Promise<DataRequestToken> {
    return new Promise((resolve, reject) => {
      this.client.createDataRequestToken(
        id,
        (error: any, response: { token: DataRequestToken }) => {
          if (error) reject(error);
          else {
            resolve({...response.token, request_id: id});
          }
        });
    });
  }
}

class FpDocspringClient {
  constructor(
    private readonly underlying: DocspringClient
  ) {}

  generatePdf<E>(templateId: string, submissionData: any): TaskEither<E, GeneratePdfResponse> {
    return fromPromise(this.underlying.generatePdf(templateId,  submissionData));
  }

  generateDataRequest<E>(templateId: string, email: string, submissionData: any, userFields?: string[]): TaskEither<E, DataRequestId> {
    return fromPromise(this.underlying.generateDataRequest(templateId, email, submissionData, userFields));
  }

  createDataRequestToken<E>(id: string): TaskEither<E, DataRequestToken> {
    return fromPromise(this.underlying.createDataRequestToken(id));
  }

  getSubmission<E>(submissionId: string): TaskEither<E, GeneratePdfResponse> {
    return fromPromise(this.underlying.getSubmission(submissionId));
  }

  generateGenericDataRequest<E>(dataRequests: any[], templateId: string, email: string, submissionData: any): TaskEither<E, DataRequestId[]> {
    return fromPromise(this.underlying.generateGenericDataRequest(dataRequests, templateId, email, submissionData));
  }
}

export { FpDocspringClient, DocspringClient }
